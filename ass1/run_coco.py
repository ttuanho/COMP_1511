#!/usr/bin/env python3
import codecs
import argparse
import tempfile
import glob
import atexit
import re
import shutil
import os
import sys
import subprocess

ACTION_PLAYER_NAME = 0
MAX_NAME_LEN = 32


def main():
    global args

    args = parse_args()

    players = get_players(args)

    results = run_lots(args, players)

    print_results(args, players, results)

COMMAND = "1511 coco_referee --quiet --print_summary"

def print_results(args, players, results):
    num_games = len(results)

    if num_games == 0:
        print("Unable to show statistics, as no games succeeded")
        return

    for player in players:
        player['1'] = 0
        player['2'] = 0
        player['3'] = 0
        player['4'] = 0
        for result in results:
            for standing in result:
                m = re.search("\s+" + player['name'] + '\s+', standing)
                if m is not None:

                # if player['name'] + ' ' in standing:
                    pos = standing.strip()[0]
                    if not str(pos).isdigit():
                        print("\n---------------------------------\n")
                        print("Error: something weird is going on")
                        print("Please contact AndrewB with the following information:")
                        print(f"result: {result}")
                        print(f"standing: {standing}")
                        print("\n---------------------------------\n")

                        continue

                    player[str(pos)] += 1

    for player in players:
        s = f"Ranks for {player['name']}:"

        print("")
        print(s)
        print('-' * len(s))
        for pos in range(1,5):
            pos = str(pos)
            #print_score(player['name'], pos, player[str(pos)], num_games)

ranks = {
    '1': '1st',
    '2': '2nd',
    '3': '3rd',
    '4': '4th'
}
        

def print_score(name, rank, count, num_games):
    rank = ranks[rank]
    percent = int(count / num_games * 100)
    print(f"Number of times {name} came {rank}: {count} ({percent} %)")

def run_lots(args, players):
    results = []
    binaries = [player['binary'] for player in players]

    command = COMMAND.split(" ") + binaries

    if not args.show_debug_output:
        command.append("--tournament")

    for x in range(args.num_games):
        if not args.quiet:
            print("")

        p = subprocess.run(command, stdout=subprocess.PIPE)
        stdout = codecs.decode(p.stdout, 'ascii', errors='replace')

        output = stdout.strip().split("\n")
        result = [x for x in output[-4:] if x]

        should_print = False

        if game_succeeded(args, players, output):
            # results.append(output)
            results.append(result)
            should_print = True
        else:
            print("An error occured")
            if args.show_errors:
                should_print = True
            else:
                print("To see more information, run with --show-errors")

        if args.quiet:
            should_print = False

        if should_print:
            print("\n".join(output))

    return results

def game_succeeded(args, players, output):

    # last four lines should look like
    # rank\tuser\tpoints x 4
    result = [x for x in output[-4:] if x]

    success_dodgy = dodgy_success_check(result)
    success_check = ranks_success_check(players, result)


    if success_dodgy != success_check:
        print("Error: unable to detect whether game successfully finished")
        print("Please contact AndrewB about this")

        worked = "did"
        if (not success_check):
            worked = "didn't"

        print(f"Assuming that the game {worked} succeed")

    return success_check



def dodgy_success_check(result):
    # really dodgy hack that will *probably* work:
    # check if the first line of the result is "1\t"

    if result[0][0] == "1":
        return True

    return False


def ranks_success_check(players, result):

    success = True

    # potentially less dodgy hack: check that all known player names
    # have a rank?
    for player in players:
        found_player = False
        for standing in result:
            if player['name'] in standing and standing[0].isdigit():
                found_player = True

        if not found_player:
            success = False

    return success



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("num_games", help="number of games to play",
                        nargs="?", default=100, type=int)

    parser.add_argument("source_files", help="coco programs to play",
            nargs="*", default=["coco.c"])

    parser.add_argument("--quiet", action="store_true",
            help="only print final summary")

    parser.add_argument("--show-errors", action="store_true",
            help="show errors if a player makes an invalid move")

    parser.add_argument("--compiler", help="which compiler to use",
            default="dcc")

    parser.add_argument("--optimise", action="store_true",
        help="use optimisation to make the binaries run faster")

    parser.add_argument("--show-debug-output", action="store_true",
            help="show debug output from player(s)")

    parser.add_argument("--debug-script", action="store_true",
            help="print debug output for this script")

    return parser.parse_args()


# compile the players + sanity check the names
def get_players(args):
    binaries = compile_players(args)

    players = []
    for (binary, source) in binaries:
        players.append({
            'binary': binary,
            'source': source,
            'name': get_player_name(binary)
        })

    names = [p['name'] for p in players]

    print("\nPlayers:")
    for player in players:
        print(f"{player['name']} ({player['source']})")

    if len(set(names)) != len(names):
        print("\nError: player names are not unique!")
        print("Make sure that each version of your code has a unique " +
                "player name.")
        sys.exit(1)

    return players


# heavily modified from coco_referee
def compile_players(args):
    print("Compiling player(s) first (so that we don't have to keep " + \
            "compiling them for each attempt)")

    player_dirs = []
    binaries = []

    temp_dir = tempfile.mkdtemp()
    atexit.register(cleanup, temp_dir=temp_dir)

    for (player_number,path) in enumerate(args.source_files):
        player_dir = os.path.join(temp_dir, str(player_number))
        os.mkdir(player_dir)

        if not (os.path.isfile(path) and os.path.splitext(path)[1] in ['.c']):
            print(os.path.isfile(path))
            print(f"Error opening file? {path}")
            print("If you think this should work, contact AndrewB")
            sys.exit(1)

        try:
            shutil.copy(path, player_dir)
        except IOError:
            print(f"Error copying file: {path}", file=sys.stderr)
            sys.exit(1)

        player_dirs.append(player_dir)

    for player_dir in player_dirs:
        binaries.append(compile_program(args, dir=player_dir))

    return binaries

# taken nearly verbatim from coco_referee
def compile_program(args, dir=None, binary='coco'):
    if dir:
        os.chdir(dir)

    source_files = glob.glob('*.c')
    compiler = [args.compiler, '-o', binary, '-lm']
    try:
        command = compiler + source_files
        if args.optimise:
            command += ["-O2"]
        print(' '.join(command))
        subprocess.check_call(command)
    except subprocess.CalledProcessError:
        sys.exit(1)

    return (os.path.realpath(binary), ''.join(source_files))


def get_player_name(binary):
    player_name_input = str(ACTION_PLAYER_NAME) + "\n"

    p = subprocess.run(
        binary, stdout=subprocess.PIPE, input=player_name_input.encode())

    stdout = codecs.decode(p.stdout, 'ascii', errors='replace').strip()

    name = "Unknown player"
    if stdout is not None:
        name = re.sub(r"[^a-zA-Z0-9' _-]", '', stdout)[0:MAX_NAME_LEN]

    return name

# verbatim stolen from referee
def cleanup(temp_dir=None):
    if temp_dir and re.search('^/tmp/', temp_dir):
        shutil.rmtree(temp_dir)


# TODO: use this maybe?f
# probs just detect if the same name, and advise to change
def sanitize_player_names(players):
    names = set()
    for player in players:
        name = player['name']
        prefix = re.sub(r"[^a-zA-Z0-9' _-]", '', name)[0:MAX_SUPPLIED_PLAYER_NAME_CHARS]
        i = 0
        name = prefix
        while name in names:
            name = prefix + str(i)
            i += 1
        player['name'] = name
        names.add(name)



"""
def run_binary(player, input=''):
    (stdout, stderr, exit_status) = \
        subprocess.run(binary, stdout=subprocess.PIPE, input=input)

    stdout = codecs.decode(stdout, 'ascii', errors='replace')

    return stdout


def run_dual(binary, arguments=[], input=''):
    output = []
    binaries = [binary]

    # don't have to care about this
    if isinstance(input, str):
        input = input.encode('ascii')

    # replace this with whatever
    (stdout, stderr, exit_status) = subprocess.run(binary,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE, input=input)
    # does this matter?
    stdout = codecs.decode(stdout, 'ascii', errors='replace')
    stderr = codecs.decode(stderr, 'ascii', errors='replace')

    if stderr and exit_status != 0:
        return (stdout, stderr, exit_status, b)
    output.append((stdout, stderr, exit_status, b))

    return output[0] if output else ('', '', 1, binary)


def run_with_resource_limits(*args, **kwargs):
    p = subprocess.run(*args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kwargs)
    return (p.stdout, p.stderr, p.returncode)
"""


# compile it

# get the name from each of the player files

# run the referee many times

# calculate the stats from the output


# print the results

if __name__ == "__main__":
    main()


