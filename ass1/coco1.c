// Assignment 1 19T1 COMP1511 Coco
//
// This program was written by TUAN HO  (z5261243)
// on 19/3/2019
//
//

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>


#define ACTION_PLAYER_NAME    0
#define ACTION_DISCARD        1
#define ACTION_PLAY_CARD      2

#define N_CARDS              40
#define N_CARDS_INITIAL_HAND 10
#define N_PLAYERS             4
#define N_CARDS_DISCARDED     3

#define CARD_MIN             10
#define CARD_MAX             49

#define DOGHOUGLAS           42

void print_player_name(void);
void choose_discards(void);
void choose_card_to_play(void);

int is_cocomposite(int a, int b);
int is_prime(int num);
int is_array_element_prime(int num[37], int i);
int cocomposite_Index(int num);

int main(void) {

    int which_action = 0;
    scanf("%d", &which_action);
    if (which_action == ACTION_PLAYER_NAME) {
        print_player_name();
    } else if (which_action == ACTION_DISCARD) {
        choose_discards();
    } else if (which_action == ACTION_PLAY_CARD) {
        choose_card_to_play();
    }
    printf("\n");
    return 0;
}

//Print the plyer's name in the game
void print_player_name(void) {
    printf("Andrew Taylor ");
}

//The strategy is to discard 3 numbers which
void choose_discards() {
    int i;
    
    //Input the cards on hand initially
    int cardOnHand[11];
    for (i = 1; i <= 10; i++){
        scanf("%d",&cardOnHand[i]);
    }
    //int N_discarded = 0;
    printf("%d %d %d", cardOnHand[10], cardOnHand[9], cardOnHand[8]);
    
}


void choose_card_to_play(void) {
    int i;
    int cardOnHand[N_CARDS_INITIAL_HAND];
    
    int N_cards_on_hand, N_of_cards_played_in_this_round, table_position;
    
    //printf("N_cards_on_hand, N_of_cards_played_in_this_round, table_position: \n");
    scanf("%d",&N_cards_on_hand);
    scanf("%d",&N_of_cards_played_in_this_round);
    scanf("%d",&table_position);
    
    //N_of_cards_played_in_this_round += 1;
    
    
    //Input the cards on my hand in this round
    //printf("Input the cards on my hand in this round\n");
    for (i = 0; i <= N_cards_on_hand - 1; i++){
        scanf("%d", &cardOnHand[i]);
    }
    
    //Input the cards played in this round
    //printf("Input the cards played in this round\n");
    int cards_played_in_this_round[4] = {0};
    if (N_of_cards_played_in_this_round == 0){
        cards_played_in_this_round[1] = -1;
        //printf("N_of_cards_played_in_this_round = %d\n",N_of_cards_played_in_this_round);
    } else if (N_of_cards_played_in_this_round != 0){
        for (i = 0; i <= N_of_cards_played_in_this_round - 1; i++){
            scanf("%d", &cards_played_in_this_round[i]);
        }
    }
    
    //Input the cards played in previous round(s)
    //printf("Input the cards played in previous round(s)\n");
    int cards_played_in_previous_rounds[37];
    int N_round_before = N_CARDS_INITIAL_HAND - N_cards_on_hand;
    int N_cards_played_in_previous_rounds = N_round_before * 4;
    //printf("N_cards_played_in_previous_rounds = %d\n",N_cards_played_in_previous_rounds);
    for (i = 0; i <= N_cards_played_in_previous_rounds - 1; i++){
        scanf("%d", &cards_played_in_previous_rounds[i]);
    }
        
    //Input the cards I discarded
    //printf("Input the cards I discarded\n");
    int cards_discarded[3];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++){
        scanf("%d", &cards_discarded[i]);
    }
    
    //Input the cards discarded to me
    //printf("Input the cards discarded to me\n");
    int cards_received[3];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++){
        scanf("%d", &cards_received[i]);
    }
    
    //Play the card
    //printf("Play the card\n");
    if (N_of_cards_played_in_this_round == 0){
        cards_played_in_this_round[1] = -1;
    }
    int is_played_yet = 0; //I havenot played my card yet. Otherwise, return 1.
    if (N_of_cards_played_in_this_round != 0){ // If my turn is not first
        int firstCardInThisRound = cards_played_in_this_round[0];
        if (is_prime(firstCardInThisRound) == 0) {//If the first card played in this round is composite
            for (i = N_cards_on_hand - 1; i >= 0; i--){
                if (is_played_yet == 0 && is_cocomposite(firstCardInThisRound,cardOnHand[i]) == 1){
                    //printf("\nThe first card is %d, play ",firstCardInThisRound);
                    printf("%d",cardOnHand[i]);
                    is_played_yet = 1;
                }
            }
            if (is_played_yet == 0) {
                printf("%d", cardOnHand[N_cards_on_hand - 1]);
            }
        } else if (is_prime(firstCardInThisRound) == 1){ //If the first card played in this round is prime
            for (i = N_cards_on_hand - 1; i >= 0; i--){
                if (is_played_yet == 0 && is_prime(cardOnHand[i]) == 1){
                    printf("%d", cardOnHand[i]);
                    is_played_yet = 1;
                }
            }
            if (is_played_yet == 0) {
                printf("%d", cardOnHand[N_cards_on_hand - 1]);
            }
        }
    } else if (N_of_cards_played_in_this_round == 0){//If my turn is first
        i = N_cards_on_hand - 1;
        while(is_played_yet == 0){
            if (is_prime(cardOnHand[i]) == 0){
                printf("%d",cardOnHand[i]);
                is_played_yet = 1;
            }
            i--;
        }
        
    }
}

// This functions checks if any two numbers are co-composite 
// If a,b are co-composite -> return 1. Otherwise, return 0.
int is_cocomposite(int a, int b){
    int is_it = 0;
    int i, j;
    
    for (i = 2; i < a; i++){
        if (a % i == 0 && b % i ==0){
            is_it = 1;
        }
    }
    for (i = 2; i < b; i++){
        if (a % i == 0 && b % i ==0){
            is_it = 1;
        }
    }
    return is_it;
}


//This function checks if a number is a prime
//If the number is prime -> return 1. Otherwise, return 0.
int is_prime(int num){
    int is_it = 1; //assuming is a prime number
    int i;
    if (num == 0){
        is_it = 0;
    } else {
        for (i = 2; i < num ; i++){
            if (num % i == 0){
                is_it = 0;
            }
        }
    }
    return is_it;
}

void choose_legal_cards(int N_cards_on_hand, int cardOnHand[10], int firstCardInThisRound){
    
}
