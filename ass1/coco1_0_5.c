// Assignment 1 19T1 COMP1511 Coco
//
// This program was written by TUAN HO  (z5261243)
// on 19/3/2019
//
//

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>


#define ACTION_PLAYER_NAME    0
#define ACTION_DISCARD        1
#define ACTION_PLAY_CARD      2

#define N_CARDS              40
#define N_CARDS_INITIAL_HAND 10
#define N_PLAYERS             4
#define N_CARDS_DISCARDED     3

#define CARD_MIN             10
#define CARD_MAX             49

#define DOUGLAS              42

int cardOnHand[10] = {0};

int is_cocomposite(int a, int b);
int is_prime(int num);
int is_array_element_prime(int array[10], int index);
int max_card_in_this_round(int cards_played_in_this_round[4], int N_cards_played_in_this_round);
int is_DOUGLAS_on_hand(int cardOnHand[10], int N_cards_on_hand);
int legal_cards(int cardOnHand[10], int N_cards_on_hand, int firstCardInThisRound, int legal_cards[10]);
int is_DOUGLAS_played_in_this_round(int cards_played_in_this_round[4], int N_cards_played_in_this_round);
void print_player_name(void);
void choose_discards(void);
void choose_card_to_play(void);

int main(void) {

    int which_action = 0;
    scanf("%d", &which_action);
    if (which_action == ACTION_PLAYER_NAME) {
        print_player_name();
    } else if (which_action == ACTION_DISCARD) {
        choose_discards();
    } else if (which_action == ACTION_PLAY_CARD) {
        choose_card_to_play();
    }
    printf("\n");
    return 0;
}

//Print the plyer's name in the game
void print_player_name(void) {
    printf("CocoNerdt v0.0.3");
}

//The strategy is to discard the 2 of 3 highest prime numbers 43, 47, 41 in decreasing order if on hand.
//Then ditch the highest non-primed number in decreasing order until enough 3 cards are discarded
//If none are discarded yet, discard the highest primed numbers if on hand.
//Then discard the highest non-primed numbers until enough 3 cards are discarded.
void choose_discards() {
    int i;
    int discard_counter = 0;
    int cards_to_discard[N_CARDS_DISCARDED] = {0};
    int how_many_primes = 0;
    
    //Input the cards on hand initially and discard 2 of 43, 47 or 41 if on hand
    int cardOnHand[N_CARDS_INITIAL_HAND];
    for (i = 0; i <= N_CARDS_INITIAL_HAND - 1; i++) {
        scanf("%d", &cardOnHand[i]);
        
        if (is_prime(cardOnHand[i]) == 1){
            how_many_primes += 1;
        }
        
    }

    for (i = N_CARDS_INITIAL_HAND - 1; i >= 0; i--) {
        //If having DOUGLAS as the highest card, hold it
        if (cardOnHand[i] == DOUGLAS && discard_counter == 0) {
            continue;
        }
        //Discard all primes > 40
        if (cardOnHand[i] > 40 && is_prime(cardOnHand[i]) == 1 && discard_counter < 2){
            cards_to_discard[discard_counter] = cardOnHand[i];
            discard_counter += 1;
            continue;
        }
        if (discard_counter == 0 || discard_counter == 1) {
            if (how_many_primes < 4){
                cards_to_discard[discard_counter] = cardOnHand[i];
                discard_counter += 1;
                continue;
            } else {
                if (is_prime(cardOnHand[i]) == 1){
                    cards_to_discard[discard_counter] = cardOnHand[i];
                    discard_counter += 1;
                    how_many_primes -= 1;
                    continue;
                }
            }
        }
        if (discard_counter == 2){
            if (is_prime(cardOnHand[i]) == 0){
                cards_to_discard[discard_counter] = cardOnHand[i];
                discard_counter += 1;
            } else {
                continue;
            }
        }
        if (discard_counter == 3) {
            break;
        }
    }
    
    //Output the cards discarded
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++){
        printf("%d ",cards_to_discard[i]);
    }
}


void choose_card_to_play(void) {
    int i;
    int cardOnHand[N_CARDS_INITIAL_HAND] = {0};
    
    int N_cards_on_hand, N_cards_played_in_this_round, table_position;
    
    //printf("N_cards_on_hand, N_cards_played_in_this_round, table_position: \n");
    scanf("%d", &N_cards_on_hand);
    scanf("%d", &N_cards_played_in_this_round);
    scanf("%d", &table_position);
    
    //Input the cards on my hand in this round
    for (i = 0; i <= N_cards_on_hand - 1; i++) {
        scanf("%d", &cardOnHand[i]);
    }
    
    // Input the cards played in this round
    int cards_played_in_this_round[4] = {0};
    if (N_cards_played_in_this_round == 0) {
        cards_played_in_this_round[1] = -1;
        //printf("N_cards_played_in_this_round = %d\n",N_cards_played_in_this_round);
    } else if (N_cards_played_in_this_round != 0) {
        for (i = 0; i <= N_cards_played_in_this_round - 1; i++) {
            scanf("%d", &cards_played_in_this_round[i]);
        }
    }
    
    // Input the cards played in previous round(s)
    int cards_played_in_previous_rounds[37];
    int N_round_before = N_CARDS_INITIAL_HAND - N_cards_on_hand;
    int N_cards_played_in_previous_rounds = N_round_before * N_PLAYERS;
    //printf("N_cards_played_in_previous_rounds = %d\n",N_cards_played_in_previous_rounds);
    for (i = 0; i <= N_cards_played_in_previous_rounds - 1; i++) {
        scanf("%d", &cards_played_in_previous_rounds[i]);
    }
        
    // Input the cards I discarded
    //printf("Input the cards I discarded\n");
    int cards_discarded[3];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++) {
        scanf("%d", &cards_discarded[i]);
    }
    
    // Input the cards discarded to me
    int cards_received[3];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++) {
        scanf("%d", &cards_received[i]);
    }
    
    // Play the card
    int firstCardInThisRound;
    if (N_cards_played_in_this_round == 0) {
        firstCardInThisRound = 0;
    } else {
        firstCardInThisRound = cards_played_in_this_round[0];
    }
    
    
    //I havenot played my card yet. Otherwise, return 1.
    int is_played_yet = 0;
    
    if (N_cards_played_in_this_round != 0) {
        if (is_prime(firstCardInThisRound) == 0) {
            if (is_DOUGLAS_played_in_this_round(cards_played_in_this_round, N_cards_played_in_this_round) == 0){
                for (i = N_cards_on_hand - 1; i >= 0; i--) {
                    if (is_DOUGLAS_on_hand(cardOnHand, N_cards_on_hand) == 1){
                        if (max_card_in_this_round(cards_played_in_this_round, N_cards_played_in_this_round) > DOUGLAS){
                            printf("%d", DOUGLAS);
                            is_played_yet = 1;
                            break;
                        }
                    }
                    if (is_played_yet == 0){
                        if (is_cocomposite(firstCardInThisRound, cardOnHand[i]) == 1) {
                            //printf("\nThe first card is %d, play ",firstCardInThisRound);
                            printf("%d", cardOnHand[i]);
                            is_played_yet = 1;
                            break;
                        }
                    }
                }
                if (is_played_yet == 0) {
                    printf("%d", cardOnHand[N_cards_on_hand - 1]);
                }
            } else {
                for (i = 0; i <= N_cards_on_hand - 1; i++) {
                    if (is_DOUGLAS_on_hand(cardOnHand, N_cards_on_hand) == 1){
                        if (max_card_in_this_round(cards_played_in_this_round, N_cards_played_in_this_round) > 42){
                            printf("%d", DOUGLAS);
                            is_played_yet = 1;
                            break;
                        }
                    }
                    if (is_played_yet == 0){
                        if (is_cocomposite(firstCardInThisRound, cardOnHand[i]) == 1) {
                            //printf("\nThe first card is %d, play ",firstCardInThisRound);
                            printf("%d", cardOnHand[i]);
                            is_played_yet = 1;
                            break;
                        }
                    }
                }
                if (is_played_yet == 0) {
                    printf("%d", cardOnHand[0]);
                }
            }
        } else if (is_prime(firstCardInThisRound) == 1) {
            for (i = N_cards_on_hand - 1; i >= 0; i--) {
                if (is_played_yet == 0 && is_prime(cardOnHand[i]) == 1){
                    printf("%d", cardOnHand[i]);
                    is_played_yet = 1;
                    break;
                }
            }
            if (is_played_yet == 0) {
                printf("%d", cardOnHand[N_cards_on_hand - 1]);
            }
        }
    } else if (N_cards_played_in_this_round == 0) {
        i = 0;
        while (is_played_yet == 0 && i <= N_cards_on_hand - 1) {
            int smallest_card_on_hand = cardOnHand[i];
            if (is_prime(smallest_card_on_hand) == 0) {
                printf("%d",cardOnHand[i]);
                is_played_yet = 1;
            }
            i++;
        }
        if (is_played_yet == 0) {
            printf("%d", cardOnHand[N_cards_on_hand - 1]);
        }
    }
}

// This functions checks if any two numbers are co-composite 
// If a,b are co-composite -> return 1. Otherwise, return 0.
int is_cocomposite(int a, int b) {
    int is_it = 0;
    int i, j;
    if (b > a) {
        for (i = 2; i < a; i++) {
            if (a % i == 0 && b % i ==0) {
                is_it = 1;
            }
        }
    } else if (b < a) {
        for (i = 2; i < b; i++) {
            if (a % i == 0 && b % i ==0) {
                is_it = 1;
            }
        }
    }
    return is_it;
}


//This function checks if a number is a prime
//If the number is prime -> return 1. Otherwise, return 0.
int is_prime(int num) {
    //int is_it = 1; //assuming is a prime number
    int j;
    if (num <= 1) {
        return 0;
    } else if (num % 2 == 0 && num > 2) {
        return 0;
    } else {
        for (j = 3; j < num ; j += 2) {
            if (num % j == 0) {
                return 0;
            }
        }
    }
    return 1;
}

int is_array_element_prime(int cardOnHand[10], int index) {
    int is_it = 1; //assuming is a prime number
    int i;
    int num = cardOnHand[index];
    if (num == 0) {
        is_it = 0;
    } else {
        if (is_prime(num) == 0) {
            is_it = 0;
        }
    }
    return is_it;
}

int max_card_in_this_round(int cards_played_in_this_round[4], int N_cards_played_in_this_round) {
    int i;
    int max = 0;
    for (i = 0; i <= N_cards_played_in_this_round - 1; i++){
        if (cards_played_in_this_round[i] > max){
            max = cards_played_in_this_round[i];
        }
    }
    return max;
}
//Is DOUBLAS on my hand. If yes, return 1.
int is_DOUGLAS_on_hand(int cardOnHand[10], int N_cards_on_hand) {
    int i;
    int is_it = 0;
    for (i = 0; i <= N_cards_on_hand - 1; i++){
        if (cardOnHand[i] == 42){
            is_it = 1;
        }
    }
    return is_it;
}
int is_DOUGLAS_played_in_this_round(int cards_played_in_this_round[4],
                                    int N_cards_played_in_this_round) {
    int i;
    int is_it = 0;
    for (i = 0; i <= N_cards_played_in_this_round; i++){
        if (cards_played_in_this_round[i] == 42){
            is_it = 1;
        }
    }
    return is_it;
}
int is_DOUGLAS_played_before(int cards_played_in_previous_rounds[36],
                             int N_cards_played_in_previous_rounds) {
    int i;
    int is_it = 0;
    for (i = 0; i <= N_cards_played_in_previous_rounds; i++){
        if (cards_played_in_previous_rounds[i] == 42){
            is_it = 1;
        }
    }
    return is_it;
}
//This function chooses legal. If I can play any card, return 0
int legal_cards(int cardOnHand[10], int N_cards_on_hand,
                int firstCardInThisRound, int legal_cards[10]){
    int can_play_any_card = 0;
    int legal_card_counter = 0;
    int i;
    if (is_prime(firstCardInThisRound) == 1){
        for (i = 0; i <= N_cards_on_hand - 1; i++){
            if (is_prime(cardOnHand[i]) == 1){
                legal_cards[legal_card_counter] = cardOnHand[i];
                legal_card_counter += 1;
            }
        }
        if (legal_card_counter == 0){
            can_play_any_card = 1;
        }
    } else {
        
    }
    
    if (legal_card_counter == 0){
        
    }
    return legal_card_counter;
}
