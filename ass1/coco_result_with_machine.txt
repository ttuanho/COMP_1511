Compiling player(s) first (so that we don't have to keep compiling them for each attempt)
dcc -o coco -lm coco.c

Players:
CocoNerdt v003 (coco.c)

1	Andrew	0
1	CocoNerdt v003	0
3	Harrison	5
4	Alex	13

1	Vincent	0
1	Claire	0
3	CocoNerdt v003	3
4	Costa	15

1	Matthew	0
1	CocoNerdt v003	0
3	Andrew	7
4	Harrison	11

1	CocoNerdt v003	0
2	Aydin	4
3	Michael	7
3	Nicholas	7

1	Xue	0
1	CocoNerdt v003	0
3	Michael	1
4	Nicholas	17

1	Reede	0
2	CocoNerdt v003	4
3	Oscar	6
4	Benjamin	8

1	Braedon	1
2	Benjamin	2
3	CocoNerdt v003	4
4	Marc	11

1	Dylan	3
2	CocoNerdt v003	4
2	Elizabeth	4
4	Vincent	7

1	CocoNerdt v003	0
2	Claire	1
3	David	5
4	Marc	12

1	Xue	0
1	CocoNerdt v003	0
3	Peter	3
4	Michael	15

1	CocoNerdt v003	0
1	Peter	0
3	Kane	8
4	Harrison	10

1	CocoNerdt v003	0
1	Aydin	0
3	Nathan	4
4	Nicholas	14

1	Gal	0
2	Benjamin	1
3	CocoNerdt v003	2
4	Dean	15

1	CocoNerdt v003	0
1	Dean	0
3	Alex	8
4	Costa	10

1	Kangying	0
2	Nicholas	1
3	CocoNerdt v003	7
4	David	10

1	Livia	0
2	David	3
3	Xue	4
4	CocoNerdt v003	11

1	Stephen	1
1	CocoNerdt v003	1
3	Mitchell	4
4	Kangying	12

1	Peter	0
2	CocoNerdt v003	3
2	Vincent	3
4	Sabrina	12

1	CocoNerdt v003	0
1	Minh	0
3	Alex	2
4	Nathan	16

1	Dean	0
2	CocoNerdt v003	1
3	Stephen	3
4	Peter	14

1	CocoNerdt v003	0
1	Gal	0
3	Sabrina	7
4	Vincent	11

1	Finbar	0
1	CocoNerdt v003	0
3	Claire	6
4	Oscar	12

1	CocoNerdt v003	0
1	Nathan	0
3	Gal	4
4	Heather	14

1	Kangying	1
2	CocoNerdt v003	4
3	Sabrina	6
4	Peter	7

1	Nicholas	0
1	CocoNerdt v003	0
3	Stephen	5
4	Nathan	13

1	CocoNerdt v003	0
1	Harrison	0
3	Gal	4
4	Eleni	14

1	Michael	1
2	Elizabeth	4
2	CocoNerdt v003	4
4	Connor	9

1	CocoNerdt v003	0
1	Costa	0
3	Matthew	6
4	Claire	12

1	Xue	0
1	CocoNerdt v003	0
3	Matthew	7
4	Vincent	11

1	CocoNerdt v003	0
2	Claire	4
3	Livia	7
3	David	7

1	Eleni	0
1	CocoNerdt v003	0
3	Mitchell	7
4	Benjamin	11

1	CocoNerdt v003	0
2	Peter	1
3	Gal	7
4	Connor	10

1	CocoNerdt v003	0
2	Alex	1
3	Oscar	8
4	David	9

1	Dean	0
1	CocoNerdt v003	0
3	Aydin	5
4	Matthew	13

1	CocoNerdt v003	0
2	Gal	1
3	Michael	4
4	Oscar	13

1	CocoNerdt v003	0
2	Marc	1
3	Costa	6
4	Reede	11

1	Reede	0
2	Harrison	1
3	Stephen	7
4	CocoNerdt v003	10

1	Alex	3
2	Stephen	4
2	Heather	4
4	CocoNerdt v003	7

1	Oscar	0
2	CocoNerdt v003	1
3	Reede	4
4	Nicholas	13

1	CocoNerdt v003	0
2	Finbar	1
3	Marc	6
4	Alex	11

1	Eleni	0
2	Stephen	2
3	CocoNerdt v003	8
3	George	8

1	Nathan	0
2	CocoNerdt v003	2
3	Elizabeth	7
4	Kane	9

1	Kane	1
2	Minh	2
3	CocoNerdt v003	7
4	Jamal	8

1	CocoNerdt v003	0
1	Claire	0
3	Harrison	2
4	Peter	16

1	Nathan	0
1	CocoNerdt v003	0
3	Oscar	8
4	Eleni	10

1	Livia	0
2	Elizabeth	1
3	CocoNerdt v003	4
4	Jamal	13

1	CocoNerdt v003	0
1	Xue	0
3	Nicholas	6
4	Harrison	12

1	Andrew	0
2	Harrison	4
3	CocoNerdt v003	7
3	David	7

1	CocoNerdt v003	0
2	Emily	3
3	Dean	4
4	Connor	11

1	Costa	1
2	CocoNerdt v003	2
3	Minh	4
4	Stephen	11

1	CocoNerdt v003	0
2	Benjamin	1
3	Alex	4
4	Finbar	13

1	Sabrina	3
2	CocoNerdt v003	4
2	Braedon	4
4	Connor	7

1	Kane	0
2	CocoNerdt v003	4
3	Vincent	7
3	Zachary	7

1	Matthew	0
1	CocoNerdt v003	0
3	Marc	4
4	Aydin	14

1	Reede	0
1	CocoNerdt v003	0
3	Eleni	7
4	Xue	11

1	CocoNerdt v003	0
2	Harrison	4
3	Aydin	7
3	Minh	7

1	Elizabeth	1
2	CocoNerdt v003	4
3	Michael	6
4	Gal	7

1	Eleni	0
1	Alex	0
1	CocoNerdt v003	0
4	Kangying	18

1	Aydin	0
1	Alex	0
3	Reede	2
4	CocoNerdt v003	16

1	CocoNerdt v003	0
1	Emily	0
3	Mitchell	7
4	Finbar	11

1	CocoNerdt v003	0
1	Costa	0
3	Kangying	5
4	Oscar	13

1	CocoNerdt v003	0
1	Oscar	0
3	Alex	4
4	Andrew	14

1	Kangying	0
1	Aydin	0
3	CocoNerdt v003	3
4	Vincent	15

1	Reede	0
2	Minh	2
3	CocoNerdt v003	3
4	Marc	13

1	Braedon	1
2	CocoNerdt v003	4
3	Dylan	5
4	Kane	8

1	Vincent	1
2	Dylan	3
3	CocoNerdt v003	4
4	Andrew	10

1	Mitchell	0
1	Dylan	0
3	CocoNerdt v003	7
4	Jamal	11

1	Eleni	0
2	CocoNerdt v003	4
3	Matthew	6
4	Livia	8

1	Xue	1
2	Mitchell	4
3	Peter	6
4	CocoNerdt v003	7

1	Elizabeth	0
1	Heather	0
3	CocoNerdt v003	4
4	Benjamin	14

1	Reede	0
1	CocoNerdt v003	0
3	Jamal	7
4	Stephen	11

1	CocoNerdt v003	0
2	Braedon	4
3	Harrison	5
4	Aydin	9

1	Vincent	0
1	CocoNerdt v003	0
3	Marc	7
4	Eleni	11

1	CocoNerdt v003	0
2	Heather	2
3	Zachary	4
4	Kangying	12

1	CocoNerdt v003	0
2	Marc	4
2	Finbar	4
4	Matthew	10

1	Xue	1
2	CocoNerdt v003	4
3	David	6
4	Livia	7

1	Sabrina	0
1	CocoNerdt v003	0
3	Harrison	6
4	Dean	12

1	CocoNerdt v003	0
2	Eleni	2
3	Vincent	3
4	Heather	13

1	Harrison	0
2	CocoNerdt v003	4
3	Marc	6
4	Benjamin	8

1	CocoNerdt v003	0
1	Dylan	0
3	Claire	8
4	Reede	10

1	CocoNerdt v003	0
1	Matthew	0
3	Zachary	6
4	Dylan	12

1	Oscar	0
2	CocoNerdt v003	4
3	Minh	7
3	Connor	7

1	Braedon	0
2	CocoNerdt v003	4
2	Dylan	4
4	Zachary	10

1	Elizabeth	0
1	CocoNerdt v003	0
3	Peter	8
4	Alex	10

1	Mitchell	3
2	Dylan	4
2	CocoNerdt v003	4
4	Zachary	7

1	CocoNerdt v003	0
2	Stephen	3
3	Vincent	4
4	Dylan	11

1	CocoNerdt v003	0
1	Oscar	0
3	Gal	7
4	Braedon	11

1	Marc	0
1	CocoNerdt v003	0
3	Minh	7
4	Heather	11

1	Dean	0
1	CocoNerdt v003	0
3	Jamal	7
4	Matthew	11

1	Matthew	0
1	Dean	0
3	CocoNerdt v003	5
4	Jamal	13

1	CocoNerdt v003	0
1	Minh	0
1	Jamal	0
4	Braedon	18

1	CocoNerdt v003	0
1	Marc	0
3	Andrew	8
4	Kane	10

1	CocoNerdt v003	0
1	Dylan	0
3	Dean	9
3	Braedon	9

1	Trung	0
2	Peter	1
3	CocoNerdt v003	7
4	Heather	10

1	Sabrina	0
1	CocoNerdt v003	0
3	Benjamin	8
4	Michael	10

1	CocoNerdt v003	0
1	Livia	0
3	Jamal	4
4	Stephen	14

1	Reede	0
2	Benjamin	1
3	CocoNerdt v003	4
4	Mitchell	13

1	CocoNerdt v003	1
2	George	3
3	Kane	4
4	Costa	10

1	CocoNerdt v003	0
1	Aydin	0
3	Vincent	7
4	Michael	11

1	CocoNerdt v003	0
2	Connor	1
2	Claire	1
4	Elizabeth	16

Ranks for CocoNerdt v003:
-------------------------
Number of times CocoNerdt v003 came 1st: 60 (60 %)
Number of times CocoNerdt v003 came 2nd: 19 (19 %)
Number of times CocoNerdt v003 came 3rd: 16 (16 %)
Number of times CocoNerdt v003 came 4th: 5 (5 %)
