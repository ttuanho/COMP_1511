#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct node {
    struct node *next;
    int          data;
};

int intersection_size(struct node *head1, struct node *head2);
struct node *strings_to_list(int len, char *strings[]);

// DO NOT CHANGE THIS MAIN FUNCTION
int main(int argc, char *argv[]) {
    // create two linked lists from command line arguments
    int dash_arg = argc - 1;
    while (dash_arg > 0 && strcmp(argv[dash_arg], "-") != 0) {
        dash_arg = dash_arg - 1;
    }
    struct node *head1 = strings_to_list(dash_arg - 1, &argv[1]);
    struct node *head2 = strings_to_list(argc - dash_arg - 1, &argv[dash_arg + 1]);

    int result = intersection_size(head1, head2);
    printf("%d\n", result);

    return 0;
}

// return the number of values which occur in both linked lists
// no value is repeated in either list
int intersection_size(struct node *head1, struct node *head2) {

    struct node *p1;
    struct node2 {
        struct node2 *next;
        int data;
        int count;
    } *data1, *data2, *headdata1, *headdata2, *p, *t;
    data1 = malloc(sizeof(struct node2));
    data2 = malloc(sizeof(struct node2));
    for (p1 = head1; p1 != NULL; p1 = p1->next) {
        if (p1 == head1) {
            data1->data = p1->data;
            data1->count = 1;
            data1->next = NULL;
            headdata1 = data1;
        } else {
            int newNum = 1;
            for (t = headdata1; t != NULL; t = t->next) {
                if (p->data == t->data) {
                    newNum = 0;
                    t->count++;
                }
            }
            if (newNum == 1) {
                struct node2 *temp = malloc(sizeof(struct node2));
                temp->data = p->data;
                temp->count = 1;
                t->next = temp;
                temp->next = NULL;
            }
        }
    }
    for (p = head2; p != NULL; p = p->next) {
        if (p == head2) {
            data2->data = p->data;
            data2->count = 1;
            data2->next = NULL;
            headdata2 = data2;
        } else {
            int newNum = 1;
            for (t = headdata2; t != NULL; t = t->next) {
                if (p->data == t->data) {
                    newNum = 0;
                    t->count++;
                }
            }
            if (newNum == 1) {
                struct node2 *temp = malloc(sizeof(struct node2));
                temp->data = p->data;
                temp->count = 1;
                t->next = temp;
                temp->next = NULL;
            }
        }
    }
    int intersection = 0;
    for (p = headdata1; p != NULL; p = p->next) {
        for (t = headdata2; t != NULL; t = t->next) {
            if (p->data == t->data) {
                if (p->count <= t->count) {
                    intersection += p->count;
                } else {
                    intersection += t->count;
                }
            }
        }
    }
    return intersection;
}


// DO NOT CHANGE THIS FUNCTION
// create linked list from array of strings
struct node *strings_to_list(int len, char *strings[]) {
    struct node *head = NULL;
    for (int i = len - 1; i >= 0; i = i - 1) {
        struct node *n = malloc(sizeof (struct node));
        assert(n != NULL);
        n->next = head;
        n->data = atoi(strings[i]);
        head = n;
    }
    return head;
}
