// Assignment 1 19T1 COMP1511 Coco
//
// This program was written by TUAN HO  (z5261243)
// on 19/3/2019
//
// IMPORTANT NOTE FOR STYLE:
//      Because naming a function (along with many conditions) an abstract long name or passing too many arguments into a function could be difficult for readability and usability, deep-overnesting is unavoidable.
//      Thus, additional comments are written to articulate the strategy in logical sequences as well as not to cause interruption and confusion for readers
//
// IMPORTANT NOTE FOR CODE:
//   - 'break' and 'continue' are used to skip other logical conditions
//   - Functions are used to avoid overdeep nesting as much as possible (except for reasons above)

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

// All inputs are scanfed in increasing order -> define the smallest index 0 as smallest element of the array
#define SMALLEST_IN_ARRAY     0
#define INITIAL               0


#define ACTION_PLAYER_NAME    0
#define ACTION_DISCARD        1
#define ACTION_PLAY_CARD      2

#define N_CARDS              40
#define N_CARDS_INITIAL_HAND 10
#define N_PLAYERS             4
#define N_OPPONENT            3
#define N_PREV_ROUND_TOTAL    9

#define N_CARDS_DISCARDED     3
#define MAX_PRIME_TO_HOLD     4
#define N_CARDS_RECEIVED      3
#define CARDS_PREV_ROUND_MAX 36
#define PRIME_THRES          40

#define POSITION_MAX          3
#define POSITION_START        0
#define POSITION_END          3

#define CARD_MIN             10
#define CARD_MAX             49
#define DOUGLAS              42

#define YES                   1
#define NO                    0

// Functions for checking and finding certain numbers
int is_cocomposite(int a, int b);
int is_prime(int num);
int cardMax(int round_cards[4], int N_round_cards);

// Functions for strategy
int is_DOUGLAS_on_hand(int cardOnHand[N_CARDS_INITIAL_HAND], int N_cards_on_hand);
int chooseLegalCards(int cardOnHand[N_CARDS_INITIAL_HAND],
                     int N_cards_on_hand,
                     int firstRoundCard,
                     int legal_cards[N_CARDS_INITIAL_HAND]);
int is_DOUGLAS_played_in_this_round(int round_cards[4],
                                    int N_round_cards);
int is_DOUGLAS_played_yet(int prevRoundCards[N_PREV_ROUND_TOTAL][N_PLAYERS],
                          int N_round_before);
int areAllLegalsPrime(int legal_cards[N_CARDS_INITIAL_HAND], int N_legal_cards);
void playDOUGLAS(int highestRoundCard, int firstRoundCard, int is_played_yet);

// Functions to calculate position shifted
int nextPosition(int position);
int prevPosition(int position);

// Action functions
void print_player_name(void);
void choose_discards(void);
void choose_card_to_play(void);

int main(void) {

    int which_action = 0;
    scanf("%d", &which_action);
    if (which_action == ACTION_PLAYER_NAME) {
        print_player_name();
    } else if (which_action == ACTION_DISCARD) {
        choose_discards();
    } else if (which_action == ACTION_PLAY_CARD) {
        choose_card_to_play();
    }
    printf("\n");
    return 0;
}

// Print the plyer's name in the game
void print_player_name(void) {
    printf("CocoNerdt v0.0.8");
}

// The strategy is
// If I have less than 4 primes on hand:
//      -Discard the 2 of 3 highest prime numbers 43, 47, 41 in decreasing order if on hand.
//      -No more than 3 primes are held or a number is higher than highest prime should not be held
//      -Then get rid the highest non-primed number in decreasing order until enough 3 cards are discarded
//      -If none are discarded yet, discard the highest primed numbers if on hand.
//      -Then discard the highest non-primed numbers until enough 3 cards are discarded.
// If I have 4 primes or more on hand -> Discard the highest composites
// The DOUGLAS must be kept every time on hand
void choose_discards() {
    int i;
    int discard_counter = 0;
    int cards_to_discard[N_CARDS_DISCARDED] = {0};
    int N_primes = 0;
    int primeIndex[N_CARDS_INITIAL_HAND];
    
    //Input the cards on hand initially and discard 2 of 43, 47 or 41 if on hand
    int cardOnHand[N_CARDS_INITIAL_HAND];
    for (i = 0; i <= N_CARDS_INITIAL_HAND - 1; i++) {
        scanf("%d", &cardOnHand[i]);
        
        if (is_prime(cardOnHand[i]) == 1) {
            primeIndex[N_primes] = 1;
            N_primes += 1;
        }
        
    }
    int primeIteration = N_primes;
    for (i = N_CARDS_INITIAL_HAND - 1; i >= 0; i--) {
        // If having DOUGLAS, hold it
        if (cardOnHand[i] == DOUGLAS) {
            continue;
        }
        if (N_primes < MAX_PRIME_TO_HOLD) {
            // If I initially hold 3 primes or less
            // Discard all primes > 40 in priority
            if (cardOnHand[i] > PRIME_THRES && is_prime(cardOnHand[i]) == 1
                && discard_counter < 2) {
                cards_to_discard[discard_counter] = cardOnHand[i];
                discard_counter += 1;
                continue;
            }
            // If still holding 3 primes, discard them until having only 2
            if (discard_counter == 0 || discard_counter == 1) {
                if (primeIteration < MAX_PRIME_TO_HOLD - 1
                    || cardOnHand[primeIndex[N_primes-1] < cardOnHand[i]]) {
                    cards_to_discard[discard_counter] = cardOnHand[i];
                    discard_counter += 1;
                    continue;
                } else if (is_prime(cardOnHand[i]) == 1) {
                    cards_to_discard[discard_counter] = cardOnHand[i];
                    discard_counter += 1;
                    primeIteration -= 1;
                    continue;
                }
            }
            // At this point, it doesn't matter what card to discard
            if (discard_counter == 2) {
                cards_to_discard[discard_counter] = cardOnHand[i];
                discard_counter += 1;
                break;
            }
        } else {
            // If I have 4 primes or more on hand -> Discard the highest composites
            if (is_prime(cardOnHand[i]) == 0) {
                cards_to_discard[discard_counter] = cardOnHand[i];
                discard_counter += 1;
            }
            if (discard_counter == 3) {
                break;
            }
        }
    }
    
    //Output the cards discarded
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++) {
        printf("%d ",cards_to_discard[i]);
    }
}


void choose_card_to_play(void) {
    int i, j;
    
    int cardOnHand[N_CARDS_INITIAL_HAND] = {0};
    int N_cards_on_hand, N_round_cards, myTablePosition;
    
    scanf("%d", &N_cards_on_hand);
    scanf("%d", &N_round_cards);
    scanf("%d", &myTablePosition);
    
    //Input the cards on my hand in this round
    for (i = 0; i <= N_cards_on_hand - 1; i++) {
        scanf("%d", &cardOnHand[i]);
    }

    // Input the cards played in this round
    int round_cards[N_PLAYERS] = {0};
    if (N_round_cards != 0) {
        for (i = 0; i <= N_round_cards - 1; i++) {
            scanf("%d", &round_cards[i]);
        }
        
    }
    
    int firstRoundCard;
    if (N_round_cards == 0) {
        firstRoundCard = 0;
    } else {
        firstRoundCard = round_cards[SMALLEST_IN_ARRAY];
    }
    
    // Input the cards played in previous round(s)
    int prevRoundCards[CARDS_PREV_ROUND_MAX][N_PLAYERS];
    int N_round_before = N_CARDS_INITIAL_HAND - N_cards_on_hand;
    //int N_prevRoundCards = N_round_before * N_PLAYERS;
    for (i = 0; i <= N_round_before - 1; i++) {
        for (j = 0; j <= N_PLAYERS - 1; j++) {
            scanf("%d", &prevRoundCards[i][j]);
        }
    }
    
    // Input the cards I discarded
    int cards_discarded[N_CARDS_DISCARDED];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++) {
        scanf("%d", &cards_discarded[i]);
    }
    
    // Input the cards discarded to me
    int cards_received[N_CARDS_RECEIVED];
    for (i = 0; i <= N_CARDS_DISCARDED - 1; i++) {
        scanf("%d", &cards_received[i]);
    }
    
    // Strategy to play the card:
    
    // The variable below states I havenot played my card yet. Otherwise, return 1.
    // This variable is to ensure a legal card is played after going through many strategical sequences or logical restriction.
    int is_played_yet = NO;
    
    int legal_cards[N_CARDS_INITIAL_HAND];
    
    // If I do not play first in the round
    if (N_round_cards != 0) {
        int highestRoundCard = cardMax(round_cards, N_round_cards);
        int N_legal_cards = chooseLegalCards(cardOnHand, N_cards_on_hand, firstRoundCard, legal_cards);
        
        for (i = N_legal_cards - 1; i >= 0; i--) {
            // Play DOUGLAS when there exists a num played that is
            //          cocomposite with the 1st num but DOUGLAS is not
            //          cocomposite with the 1st num but > DOUGLAS
            //      OR  I can play any card
            if (is_DOUGLAS_on_hand(legal_cards, N_legal_cards) == YES
                && is_cocomposite(highestRoundCard, firstRoundCard) == YES
                && firstRoundCard != highestRoundCard
                && highestRoundCard > DOUGLAS
                && is_cocomposite(DOUGLAS, firstRoundCard) == YES) {
        
                printf("%d", DOUGLAS);
                is_played_yet = YES;
                break;
            }
            // If I can play any card, have DOUGLAS, and the fist card is not composite -> play DOUGLAS
            if (is_DOUGLAS_on_hand(legal_cards, N_legal_cards) == YES
                && N_legal_cards == N_cards_on_hand) {
                printf("%d", DOUGLAS);
                is_played_yet = YES;
                break;
            }
            // If the first card is prime
            if (is_prime(firstRoundCard) == YES) {
                // And if there's a prime played > my highest prime
                // Then play the highest prime
                if (is_prime(cardMax(round_cards, N_round_cards)) == YES
                    && cardMax(round_cards, N_round_cards) > cardMax(legal_cards, N_legal_cards)) {
                    printf("%d", cardMax(legal_cards, N_legal_cards));
                    is_played_yet = YES;
                    break;
                } else {
                    // Otherwise, choose the smallest (legal) prime on hand
                    printf("%d", legal_cards[SMALLEST_IN_ARRAY]);
                    is_played_yet = YES;
                    break;
                }
            } else {
                if (is_DOUGLAS_on_hand(cardOnHand, N_cards_on_hand) == NO) {
                    // And if the first round card is not prime and all my legal cards are prime
                    // Then Play the highest prime
                    if (areAllLegalsPrime(legal_cards, N_legal_cards) == YES) {
                        printf("%d", cardMax(legal_cards, N_legal_cards));
                        is_played_yet = YES;
                        break;
                    }
                    // If DOUGLAS (not on my hand) is played in this round
                    if (is_DOUGLAS_played_in_this_round(round_cards, N_round_cards) == YES) {
                        // and there's a winning number > my highest legal card
                        // Then Play my highest legal card
                        if (highestRoundCard != firstRoundCard
                            && is_cocomposite(highestRoundCard, firstRoundCard) == YES
                            && legal_cards[i] < highestRoundCard) {
                            printf("%d", legal_cards[i]);
                            is_played_yet = YES;
                            break;
                        } else {
                            continue;
                        }
                    } else {
                        if (N_round_before != 0) {
                            // If DOUGLAS (not on my hand) is played in previous round, play highest legal/ cocomposite cards asap
                            if (is_DOUGLAS_played_yet(prevRoundCards, N_round_before) == YES) {
                                printf("%d", legal_cards[N_legal_cards - 1]);
                                is_played_yet = YES;
                                break;
                            } else {
                                // If DOUGLAS is not on my hand & there's cocomposite higher than my highest legal card
                                // Play my highest legal/ cocomposite card
                                if (cardMax(legal_cards, N_legal_cards) < highestRoundCard
                                    && is_cocomposite(highestRoundCard, firstRoundCard) == YES
                                    && highestRoundCard != firstRoundCard) {
                                    printf("%d", cardMax(legal_cards, N_legal_cards));
                                    is_played_yet = YES;
                                    break;
                                } else {
                                    // Play my highest legal if it is my last turn and DOUGLAS (not on my hand) was not played in the round
                                    if (N_round_cards == 3
                                           && is_DOUGLAS_played_in_this_round(round_cards, N_round_cards) == NO) {
                                        printf("%d", cardMax(legal_cards, N_legal_cards));
                                        is_played_yet = YES;
                                        break;
                                    } else {
                                        printf("%d", legal_cards[SMALLEST_IN_ARRAY]);
                                        is_played_yet = YES;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // If DOUGLAS is on my hand
                    playDOUGLAS(highestRoundCard, firstRoundCard, is_played_yet);
                }
            }
        }
        // If DOUGLAS on my hand and first card is not prime, play the highest legal card.
        if (is_played_yet == NO) {
            if (is_DOUGLAS_on_hand(cardOnHand, N_cards_on_hand) == YES
                && cardMax(legal_cards, N_legal_cards) != DOUGLAS
                && is_prime(firstRoundCard) == NO) {
                printf("%d", cardMax(legal_cards, N_legal_cards));
            } else {
                printf("%d", legal_cards[SMALLEST_IN_ARRAY]);
                is_played_yet = YES;
            }
        }
        
    } else if (N_round_cards == 0) {
        // If my turn is first in the round
        // Play the lowest composite number
        i = SMALLEST_IN_ARRAY;
        while (is_played_yet == NO && i <= N_cards_on_hand - 1) {
            int smallest_card_on_hand = cardOnHand[i];
            if (is_prime(smallest_card_on_hand) == NO) {
                printf("%d",cardOnHand[i]);
                is_played_yet = YES;
            }
            i++;
        }
        // Otherwise, play my smallest legal card
        if (is_played_yet == NO) {
            printf("%d", cardOnHand[SMALLEST_IN_ARRAY]);
        }
    }
}

// This functions checks if any two numbers are co-composite 
// If a,b are co-composite -> return 1. Otherwise, return 0.
int is_cocomposite(int num1, int num2) {
    int is_it = NO;
    int i;
    if (num2 > num1) {
        for (i = 2; i < num1; i++) {
            if (num1 % i == 0 && num2 % i ==0) {
                is_it = YES;
            }
        }
    } else if (num2 < num1) {
        for (i = 2; i < num2; i++) {
            if (num1 % i == 0 && num2 % i ==0) {
                is_it = YES;
            }
        }
    } else {
        is_it = YES;
    }
    return is_it;
}

//This function checks if a number is a prime
//If the number is prime -> return 1. Otherwise, return 0.
int is_prime(int num) {
    int j;
    if (num <= 1) {
        return NO;
    } else if (num % 2 == 0 && num > 2) {
        return NO;
    } else {
        for (j = 3; j < num; j += 2) {
            if (num % j == 0) {
                return NO;
            }
        }
    }
    return YES;
}
// This function is to check if all my legal cards are primes
int areAllLegalsPrime(int legal_cards[N_CARDS_INITIAL_HAND], int N_legal_cards) {
    int i;
    int is_it = YES;
    for (i = 0; i <= N_legal_cards - 1; i++) {
        if (is_prime(legal_cards[i]) == 0) {
            is_it = NO;
        }
    }
    return is_it;
}
// Find the highest cards in this round
int cardMax(int round_cards[N_PLAYERS], int N_round_cards) {
    int i;
    int max = 0;
    for (i = 0; i <= N_round_cards - 1; i++) {
        if (round_cards[i] > max) {
            max = round_cards[i];
        }
    }
    return max;
}
// Find the highest in ONE previous round
int prevRoundCardMax(int prevRoundCards[N_PREV_ROUND_TOTAL][N_PLAYERS],
                     int round) {
    int i;
    int max = 0;
    for (i = 0; i <= N_PLAYERS - 1; i++) {
        if (prevRoundCards[round][i] > max) {
            max = prevRoundCards[round][i];
        }
    }
    return max;
}
// This function is to check if DOUGLAS on my hand
// If yes, return 1. Otherwise, return 0.
int is_DOUGLAS_on_hand(int cardOnHand[N_CARDS_INITIAL_HAND], int N_cards_on_hand) {
    int i;
    int is_it = NO;
    for (i = 0; i <= N_cards_on_hand - 1; i++) {
        if (cardOnHand[i] == DOUGLAS) {
            is_it = YES;
        }
    }
    return is_it;
}
// This function is to check if  DOUGLAS played in this round yet
// If yes, return 1. Otherwise, return 0.
int is_DOUGLAS_played_in_this_round(int round_cards[4],
                                    int N_round_cards) {
    int i;
    int is_it = NO;
    for (i = 0; i <= N_round_cards; i++) {
        if (round_cards[i] == DOUGLAS) {
            is_it = YES;
        }
    }
    return is_it;
}
// The function is to check is DOOUGLAS played in the rounds before
int is_DOUGLAS_played_yet(int prevRoundCards[N_PREV_ROUND_TOTAL][N_PLAYERS],
                          int N_round_before) {
    int i, j;
    int is_it = NO;
    for (i = 0; i <= N_round_before - 1; i++) {
        for (j = 0; j <= N_PLAYERS - 1; j++) {
            if (prevRoundCards[i][j] == DOUGLAS) {
                is_it = YES;
            }
        }
    }
    return is_it;
}
// This function is to choose legal cards.
// It chooses legal cards as well as retunrs the number of legal cards
int chooseLegalCards(int cardOnHand[N_CARDS_INITIAL_HAND],
                     int N_cards_on_hand,
                     int firstRoundCard,
                     int legal_cards[N_CARDS_INITIAL_HAND]) {
    int legal_card_counter = 0;
    int i;
    if (is_prime(firstRoundCard) == YES) {
        for (i = 0; i <= N_cards_on_hand - 1; i++) {
            if (is_prime(cardOnHand[i]) == YES) {
                legal_cards[legal_card_counter] = cardOnHand[i];
                legal_card_counter += 1;
            }
        }
    } else {
        for (i = 0; i <= N_cards_on_hand - 1; i++) {
            if (is_cocomposite(firstRoundCard, cardOnHand[i]) == YES) {
                legal_cards[legal_card_counter] = cardOnHand[i];
                legal_card_counter += 1;
            }
        }
    }
    
    if (legal_card_counter == 0) {
        legal_card_counter = N_cards_on_hand;
        for (i = 0; i <= N_cards_on_hand - 1; i++) {
            legal_cards[i] = cardOnHand[i];
        }
    }
    return legal_card_counter;
}
// Strategy to play DOUGLAS
void playDOUGLAS(int highestRoundCard, int firstRoundCard, int is_played_yet) {
    if (is_played_yet == NO) {
        // If there exists a cocomposite cards > DOUGLAS, then play DOUGLAS
        if (is_cocomposite(highestRoundCard, firstRoundCard) == YES
            && highestRoundCard > DOUGLAS
            && is_cocomposite(DOUGLAS, highestRoundCard) == YES) {
            printf("%d ", DOUGLAS);
            is_played_yet = YES;
        } else if (highestRoundCard != firstRoundCard
                   && is_cocomposite(highestRoundCard, firstRoundCard) == YES
                   && is_cocomposite(DOUGLAS, firstRoundCard) == NO) {
            // If there's a card cocomposite with the first round card that DOUGLAS is not cocomposite with
            printf("%d", DOUGLAS);
            is_played_yet = YES;
        }
    }
}
                                
// The function is to calculate what the next position of a position is in a cirlce
int nextPosition(int position) {
    int nextPosition = position;
    if (position == POSITION_END) {
        nextPosition = POSITION_START;
    } else {
        nextPosition += 1;
    }
    return nextPosition;
}

// The function is to calculate what the previous position of a position is in a cirlce
int prevPosition(int position) {
    int prevPosition = position;
    if (position == 0) {
        prevPosition = POSITION_END;
    } else {
        prevPosition -= 1;
    }
    return prevPosition;
}
