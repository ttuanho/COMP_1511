#include <stdio.h>
#include <stdlib.h>

struct node {
    struct node *next;
    int         data;
};

int main(int argc, char *argv[]) {
    struct node *head;
    struct node *last;

    head = malloc(sizeof (struct node));
    head->data = 1;
    last = head;

    int i = 2;
    while (i < 10) {
        last->next = malloc(sizeof (struct node));
        last = last->next;
        last->data = i;
        i = i + 1;
    }

    last->next = NULL;

    // prints 1 2 3 4 5 6 7 8 9
    for (struct node *p = head; p != NULL; p = p->next) {
        printf("%d ", p->data);
    }
    printf("\n");

    return 0;
}