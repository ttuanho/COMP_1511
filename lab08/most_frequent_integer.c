#include <stdio.h>
#include <stdlib.h>

struct node {
    struct node *next;
    int         data;
    int count;
};

int main(int argc, char *argv[]) {
    struct node *head;
    struct node *last;

    head = malloc(sizeof (struct node));
    int first;
    scanf("%d", &first);
    head->data = first;
    head->count = 1;
    last = head;
    last->next=NULL;


    int i = 1;
    while (i < 100002) {
        int n;
        if (scanf("%d",&n) == -1) {
            break;
        } else  {
            int added = 0;
            if (1) {
                for (struct node *p = head; p!= NULL; p = p->next) {
                    //printf("The list stores \n");
                    if (p->data == n) {
                        p->count++;
                        //printf("num = %d, count =%d\n ", p->data, p->count);   
                        added = 1;
                        break;
                    }
                }
                //printf("End of loop\n");
            }
            //printf("Added is %d\n", added);
            if (added == 0 && i != 1) {
                //printf("Created a additional node\n");
                last->next = malloc(sizeof (struct node));
                last = last->next;
                last->data = n;
                last->count = 1;
                last->next = NULL;
            }
        }
        
        i = i + 1;
    }

    last->next = NULL;
    struct node *maxNode;
    int max = -100;
    int datamax = -100;
    for (struct node *p = head; p != NULL; p = p->next) {
        //printf("Summary: num = %d, count = %d\n ", p->data, p->count);
        if ((p->count > max) || (p->count>=max && p->data > datamax)) {
            maxNode = p;
            max = p->count;
            datamax = p->data;
            //printf("\n%d %d\n", maxNode->data, maxNode->count);
        }
    }
    printf("%d\n",maxNode->data);

    return 0;
}