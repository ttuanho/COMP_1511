#include <stdio.h>
#include <stdlib.h>

// return the number of even values in an array.
int count_even(int length, int array[]) {
    // PUT YOUR CODE HERE (you must change the next line!)
    int i;
    int count = 0;
    for (i = 0; i <= length - 1; i++) {
        if (array[i] % 2 == 0) {
            count += 1;
        }
    }
    return count;
}
int remove_duplicates(int length, int source[length], int destination[length]) {
    int check[1000000] = {0};
    int i;
    int count = 0;
    for (i = 0; i <= length - 1; i++) {
        if (check[source[i]] == 0) {
            destination[count] = source[i];
            check[source[i]] = 1;
            count ++;
        }
    }
    return count;
}
// This is a simple main function which could be used
// to test your count_even function.
// It will not be marked.
// Only your count_even function will be marked.

# define TEST_ARRAY_SIZE 8

int main(void) {
    int test_array[TEST_ARRAY_SIZE] = {16, 7, 8, 12, 13, 19, 21, 12};

    int result = count_even(TEST_ARRAY_SIZE, test_array);

    printf("%d\n", result);
    return 0;
}