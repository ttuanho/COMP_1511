// Convert cms to feet using ints
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Week 3, March 2019

#include <stdio.h>

#define INCHES_IN_A_FOOT 12
#define CMS_IN_AN_INCH 2.54

int main(void) {
    printf("Enter your height in centimetres: ");

    int height_in_cms;
    scanf("%d", &height_in_cms);
    
    double height_in_feet = height_in_cms / (INCHES_IN_A_FOOT * CMS_IN_AN_INCH);
    printf("Your height in feet is %lf\n", height_in_feet);   

    return 0;
}
