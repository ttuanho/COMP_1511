// Count to 42
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Week 3, March 2019

#include <stdio.h>

int main(void) {
    int x = 1;

    while (x <= 42) {
        printf("%d\n", x);
        // x = x + 1;
        x++;
    }
    
    return 0;
}
