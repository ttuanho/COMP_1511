// Print numbers divisible by 7 between two given numbers
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Week 3, March 2019

#include <stdio.h>

int main(void) {
    int start, end;
    printf("please enter start number: ");
    scanf("%d", &start);
    
    printf("please enter end number: ");
    scanf("%d", &end);
    
    int count = start;
    while (count <= end) {
        if (count % 7 == 0) {
            // count is divisible by 7
            printf("%d\n", count);
        }
        count = count + 1;
    }
    
    return 0;
}
