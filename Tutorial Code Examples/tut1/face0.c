// This program prints a face
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Feb 2019

#include <stdio.h>

int main(void) {
    printf("~ ~\n");
    printf("0 0\n");
    printf(" o\n");
    printf(" -\n");

    return 0;
}
