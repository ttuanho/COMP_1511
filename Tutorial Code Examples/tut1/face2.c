// Let's add a nose!
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Feb 2019

#include <stdio.h>

int main(void) {
    printf("~ ~\n");
    printf("0 0\n");
    printf(" \"\n");
    printf("\\-/\n");

    return 0;
}
