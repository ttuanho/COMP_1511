// Computes a rectangle area
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Feb 2019

#include <stdio.h>

int main(void) {
    double height, width;
    printf("Please enter rectangle height: ");
    scanf("%lf", &height);

    printf("Please enter rectangle width: ");
    scanf("%lf", &width);
    
    double area = height * width;
    printf("Area = %lf\n", area);

    return 0;
}
