// Tell us the significance of a grade (pass vs. fail)
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Feb 2019

#include <stdio.h>

int main(void) {
    printf("Please enter your mark: ");

    int mark;
    scanf("%d", &mark);

    if (mark < 0 || mark > 100) {
        printf("ERROR\n");
    } else if (mark >= 50) {
        printf("PASS\n");
    } else {
        printf("FAIL\n");
    }

    return 0;
}
