// Implement some functions to do with arrays
// March 28, 2019
// Dylan Blecher - blecher.dylan@gmail.com

#include <stdio.h>
#define N_TILES 5

void print_tiles(int n_tiles, int tiles[N_TILES]);
int copy_positive(int numbers[N_TILES], int positive_numbers[N_TILES]);
int read_tiles(int tiles[N_TILES]);

int main(void) {
    int tiles[N_TILES];
    int n_success = read_tiles(tiles);
    printf("n_success = %d\n", n_success);
    int positive_tiles[N_TILES] = {0};
    int num_pos = copy_positive(tiles, positive_tiles);
    print_tiles(num_pos, positive_tiles);
    return 0;
}

void print_tiles(int n_tiles, int tiles[N_TILES]) {
    int index = 0;
    while (index < n_tiles) {
        printf("%d ", tiles[index]);
        index = index + 1;
    }
    printf("\n");
}

int copy_positive(int numbers[N_TILES], int positive_numbers[N_TILES]) {
    int index = 0;
    int count_pos = 0;
    int index_pos = 0;
    while (index < N_TILES) {
        if (numbers[index] > 0) {
            positive_numbers[index_pos] = numbers[index];
            count_pos = count_pos + 1;
            index_pos = index_pos + 1;
        }
        index = index + 1;
    }

    return count_pos;
}

int read_tiles(int tiles[N_TILES]) {
    int index = 0;
    int successful_scans = 0;
    while (index < N_TILES) {
        if (scanf("%d", &tiles[index]) == 1) {
            // scanf succeeded
            successful_scans++;
        }
        index = index + 1;
    }
    return successful_scans;
}
