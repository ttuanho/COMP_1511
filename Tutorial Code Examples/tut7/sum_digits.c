// Count how many digits we find in STDIN, and sum them.
// April 2, 2019
// Dylan Blecher - blecher.dylan@gmail.com


#include <stdio.h>

int main(void) {
    char ch = getchar();
    int digit_count = 0;
    int digit_sum = 0;
    while (ch != EOF) {
        if (ch >= '0' && ch <= '9') {
            digit_count = digit_count + 1;
            digit_sum = digit_sum + (ch - '0');
        }
        ch = getchar();
    }
    printf("We found %d digits which summed to %d\n", digit_count, digit_sum);
    return 0;
}
