// Print digits that were given as command line arguments
// April 2, 2019
// Dylan Blecher - blecher.dylan@gmail.com

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int word_counter = 0;
    // go through each word
    while (word_counter < argc) {
        printf("\n wordCounter = %d\n", word_counter);
        // go through each character
        int char_counter = 0;
        while (argv[word_counter][char_counter] != '\0') {
            char ch = argv[word_counter][char_counter];
            putchar(ch);
            printf("%c", ch);
            char_counter = char_counter + 1;
        }
        printf(" ");
        word_counter = word_counter + 1;
    }
    printf("\n");
    printf("%d", atoi(argv[1]));
     
    return 0;
}

/*
Here is how to convert from a command line argument (string) to an int:
    ./a.out 42
    int converted_number = atoi(argv[1]);   // argv[1] is the string "42"
    printf("%d\n", converted_number);       // will print 42 (the int)
*/
