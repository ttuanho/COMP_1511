// Print the contents of an array
// Dylan Blecher
// March 21, 2019

#include <stdio.h>
#define SIZE 7

void print_array(int n, int array[n]);

int main(void) {
    int numbers[SIZE] = {3, 7, 4, 9, 11, 13, 19};
    print_array(SIZE, numbers);

    return 0;
}

void print_array(int n, int array[n]) {
    int counter = 0;
    while (counter < n) {
        printf("array[%d] = %d\n", counter, array[counter]);
        counter++;
    }
}
