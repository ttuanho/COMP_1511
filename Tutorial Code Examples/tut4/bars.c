// Print a pattern of bars.
// Author: Dylan Blecher (d.blecher@unsw.edu.au)
// Date created: Week 4, March 2019

#include <stdio.h>

int main(void) {
    int number;
    int row, column;

    // Obtain input
    printf("Enter size: ");
    scanf("%d", &number);

    row = 0;
    while (row < number) {
        column = 0;
        while (column < number) {
            if (column % 2 == 1) {        
                printf("*");  
            } else {
                printf("-");
            }  
            column = column + 1;
        }
        printf("\n");
        row = row + 1;
    }

    return 0;
}
