#include <stdio.h>

int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        && (ch >= 'A' && ch <= 'Z')) {
            return 1;
        } else {
            return 0;
        }
}
int isNum(int ch) {
    if (ch >= '0' && ch <= '9') {
        return 1;
    } else {
        return 0;
    }
}
int main(int argc, char *argv[]) {

    int ch = getchar();
    while (ch != EOF) {
        if (isAlphabet(ch)) {
            printf(".");
        } else if (isNum(ch)) {
            putchar(ch);
        } else if (ch == ' ') {
            putchar(ch);
        } else if (ch == '\n') {
            printf("\n");
        } else {
            printf(".");
        }
        ch = getchar();
    }
    return 0;
}