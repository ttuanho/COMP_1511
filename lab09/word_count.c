#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRINGS 2000
#define MAX_CHAR 100
int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        || (ch >= 'A' && ch <= 'Z')) {
        return 1;
    } else {
        return 0;
    }
}
int isNum(int ch) {
    if (ch >= '0' && ch <= '9') {
        return 1;
    } else {
        return 0;
    }
}
int conver_to_lowercase(int ch) {
    int nch = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'A' && ch <= 'Z') {
            nch = ch + 32;
        }
    } 
    return nch;
}
void printWord(int word[MAX_STRINGS][MAX_CHAR], int char_lenght, int index) {
    int i;
    for (i = 0; i <char_lenght; i++) {
        printf("%c", word[index][i]);
    }
}
int main(int argc, char *argv[]) {
    int word[MAX_STRINGS][MAX_CHAR];
    int char_in_string = 0;
    int N_char_in_string[MAX_STRINGS];
    int wordCount = 0;
    int lineCount = 0;
    int charCount = 0;
    int enterWord = 0;

    int ch = getchar();
    while (ch != EOF) {
        if (ch == '\n') {
            lineCount++;
        }
        if (enterWord == 0 && isAlphabet(ch)) {
            enterWord = 1;
        }
        
        if (isAlphabet(ch) && enterWord) {
            word[wordCount][char_in_string] = ch;
            char_in_string++;
        } 
        /*if (enterWord == 1 && char_in_string != 0 && ch == '\'' && isAlphabet(word[wordCount][char_in_string-1])) {
            word[wordCount][char_in_string] = ch;
            char_in_string++;
        } else */if (!isAlphabet(ch) && enterWord == 1) {
            N_char_in_string[wordCount] = char_in_string;
            char_in_string = 0;
            enterWord = 0;
            wordCount++;
        }

        charCount++;
        ch = getchar();
    }
    int i, j;
    int t;
    int totalWordCount = wordCount;

    /*printf("word[i][j]\n");
    for (i = 0; i < wordCount; i++) {
        for (j = 0; j <N_char_in_string[i]; j++) {
            printf("%c", word[i][j]);
        }
    }
    printf("\n");*/
    int N_duplicate = 0;;
    for (i = 0; i < wordCount; i++) {
        for (t = i + 1; t < wordCount; t++) {
            int sameString = 1;
            if (N_char_in_string[i] == N_char_in_string[t]) {
                for (j = 0; j <N_char_in_string[i]; j++) {
                    if (conver_to_lowercase( word[i][j]) != conver_to_lowercase( word[t][j])) {
                        sameString = 0;
                        break;
                    }
                }
            } else {
                sameString = 0;
            }
            if (sameString == 1) {
                N_duplicate++;
                /*printf("Marked duplicate word ");
                printWord(word, N_char_in_string[i], i);
                printf(" i = %d, t = %d", i, t);
                printf("\n");*/
            }
        }
    }
    totalWordCount -= N_duplicate;
    printf("%d lines %d words %d characters\n", lineCount, totalWordCount, charCount);
    return 0;
}