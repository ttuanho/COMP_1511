#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        && (ch >= 'A' && ch <= 'Z')) {
            return 1;
        } else {
            return 0;
        }
}
int isNum(int ch) {
    if (ch >= '0' && ch <= '9') {
        return 1;
    } else {
        return 0;
    }
}
int main(int argc, char *argv[]) {

    int ch = getchar();
    while (ch != EOF) {
        if (isNum(ch)) {
            printf("%c", argv[1][ch -'0']);
        } else {
            putchar(ch);
        }
        ch = getchar();
    }
    return 0;
}