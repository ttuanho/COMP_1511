//vector_best_permutation
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
//@UNSW
//zID:    z5621243
//Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com

#include <stdio.h>
#include <math.h>

int valueIndex(int array[100], int size, int position_in_array){
    int i,j;
    int index = 0;
    //int min = array[1];
    for (i = 1; i <= size; i++){
        if (array[position_in_array] >= array[i]){
            index += 1;
        }
    }
    
    return index;
}
int correspondingId(int array[100], int size, int givenIndex){
    int i;
    int save = 1;
    for (i = 1; i <=  size; i++){
        if (valueIndex(array,size,i) == givenIndex){
            save = i;
        }
    }
    return save;
}
int main(void){
    int vector_length,v1[100],v2[100],v3[100],v4[100];
    double result;
    
    //Enter vector length
    printf("Enter vector length: ");
    scanf("%d",&vector_length);
    int i,j,count;
    
    count = vector_length*(vector_length-1)/2;
    
    //Enter vector 1
    printf("Enter vector1: ");
    for (i=1;i<= vector_length; i++){
        scanf("%d",&v1[i]);
    }
    v1[0]=0;
    v1[i+1]=0;
    //Print out valueIndex of elements of vector 1
    /*for (i=1;i<= vector_length; i++){
        printf("element = %d, valueIndex = %d\n",v1[i],valueIndex(v1,vector_length,i));
    }*/
    
    //Enter vector 2
    printf("Enter vector2: ");
    for (i=1;i<= vector_length; i++){
        scanf("%d",&v2[i]);
    }
    v2[0]=0;
    v2[i+1]=0;
    
    //Print out optiomal permutation
    printf("Optimal permutation: ");
    for (i=1;i<= vector_length; i++){
        printf("%d ",correspondingId(v1,vector_length,valueIndex(v2,vector_length,i))-1);
    }
    printf("\n");

    //Print corresponding v1
    /*printf("Corresponding v1: ");
    for (i=1;i<= vector_length; i++){
        printf("%d ",v1[correspondingId(v1,vector_length,valueIndex(v2,vector_length,i))]);
    }
    printf("\n");*/
    
    //Calculate the distance
    result = 0;
    for (i=1;i<= vector_length; i++){
        //printf("i= %d, v2[i] = %d, v1[i] = %d, index2 = %d, index1 = %d => corres.Id of v1 = %d, corres. v1 = %d\n",i,v2[i],v1[i],valueIndex(v2,vector_length,i),valueIndex(v1,vector_length,i),correspondingId(v1,vector_length,valueIndex(v2,vector_length,i)+1),v1[correspondingId(v1,vector_length,valueIndex(v2,vector_length,i))]);
        result += pow(v2[i]-v1[correspondingId(v1,vector_length,valueIndex(v2,vector_length,i))],2);
    }
    result = sqrt(result);
    
    printf("Euclidean distance = %lf\n",result);
    
    return 0;
}
