
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
//@UNSW
//zID:    z5621243
//Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com


#include <stdio.h>
#include <math.h>

int is_output(int num, int not_result[100], int array_size){
    
    int i;
    int count = 0;
    int is = 1;
    for (i=0;i<=array_size;i++){
        if (not_result[i] == num){
            count+=1;
        }
    }
    if (count != 0) {
        is = 0;
    }
    return is;
}

int main(void){
    
    int num[1000] = {0};
    num[0] = 0;
    int odd [20] = {0};
    int even[20] = {0};
    int count_odd = 0;
    int count_even = 0;
    int i = 0;
    int count = 0;
    
    for (i = 0; i<=40; i++){
        if (scanf("%d",&num[i]) != EOF){
            count +=1;
        }
        else {
            break;
        }
    }
    int not_result[100];
    int count_result = 0;
    int check [100] = {2};
    int j;
    for (i=1;i <= count-1; i++){
        for (j=0;j<=i-1;j++){
        //printf("num[i] = %d, num[j] = %d, check[num[j]]= %d,check[num[j]]=%d\n",num[i],num[j],check[num[i]],check[num[j]]);
            if (num[i] % num[j] ==0 ){
                not_result[count_result] = num[i];
                count_result+=1;
            }
            if (num[j] % num[i] ==0 ){
                not_result[count_result] = num[j];
                count_result+=1;
            }
        }
    }
    count_result-=1;
    printf("Indivisible numbers: ");
    for (i=0;i <= count-1; i++){
        if (is_output(num[i],not_result,count_result)==1){
            printf("%d ",num[i]);
        }
    }
    printf("\n");
    return 0;
}

