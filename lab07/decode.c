
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
//@UNSW
//zID:    z5621243
//Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int is_alphabet(int ch){
    int return_value = 0;
    if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
        return_value = 1;
    } else{
        return_value = 0 ;
    }
    return return_value;
}
int is_capital(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            is_it = 1;
        }
    }
    return is_it ;
}
int is_lowewcase(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 97 && ch <= 122){
            is_it = 1;
        }
    }
    return is_it ;
}
int swap_case(int ch){
    int return_value = ch;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            ch += 32;
        } else if (ch >= 97 && ch <= 122){
            ch -= 32;
        }
    } else {
        return_value = ch;
    }
    return_value = ch;
    return return_value;
}
int caesar_encrypt(int ch, int shift_num){
    int return_value = 0;
    if (is_alphabet(ch) == 1){
        if (shift_num > 0 ){
            return_value = ch + shift_num % 26;
        } else if (shift_num < 0) {
            return_value = ch + shift_num % 26 ;
        }
    } else {
        return_value = ch;
    }
    if (shift_num > 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90-1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65-1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122-1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97-1;
            }
        }
    }
    if (shift_num < 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90+1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65+1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122+1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97+1;
            }
        }
    }
    return return_value;
}
int convert_to_lowercase(int ch){
    if (is_capital(ch) == 1){
        ch += 32;
    }
    return ch;
}

int decrypt_sub(int character[256], int key[26], int cipher){
    int i;
    int count_key = 0;
    int return_value = cipher;
    if (is_alphabet(cipher) == 1){
        for (i = 97; i <= 122; i++){
            character[i] = key[count_key];
            if (cipher == key[count_key]){
                return_value = i;
            }
            count_key += 1;
        }
        count_key = 0;
        for (i = 65; i <= 90; i++){
            character[i] = key[count_key]-32;
            if (cipher == key[count_key]-32){
                return_value = i;
            }
            count_key += 1;
        }
    }
    
    return return_value;
}

void input_key(int key[26]){
    int i;
    //printf("Input key: ");
    for (i = 0; i <= 25; i++){
        key[i] = getchar();
    }
    //printf("Finishing input key. \n");
}
int encrypt_sub(int character[256], int key[26], int ch){
    int value = ch;
    int count_key = 0;
    int i;
    if (is_alphabet(ch) == 1){
        for (i = 97; i <= 122; i++){
            character[i] = key[count_key];
            count_key += 1;
        }
        count_key = 0;
        for (i = 65; i <= 90; i++){
            character[i] = key[count_key]-32;
            count_key += 1;
        }
        value = character[ch];
    } 
    return value;
}
/*int return_value_encrypt_sub(int ch, int key[26]){
    
}*/


int main(int argc, char * argv[]){
    int character_freq[256] = {0};
    int character[256] = {0};
    int key[27];
    
    //int shift = 5;
    int word_counter = 0;
    // go through each word
    while (word_counter < argc) {
        int char_counter = 0;
        while (argv[word_counter][char_counter] != '\0') {
            char ch = argv[word_counter][char_counter];
            key[char_counter] = argv[word_counter][char_counter];
            char_counter = char_counter + 1;
        }
        word_counter = word_counter + 1;
    }
    //key = atoi(argv[1]);
    
    //input_key(key);
    int ch = getchar();
    putchar(decrypt_sub(character,key,ch));
    
    while (ch != EOF) {
        ch = getchar();
        if (ch!= EOF) {
            putchar(decrypt_sub(character,key,ch));
        }
        
    }
    
    return 0;
}
