// copy all of the values in source1 which are also found in source2 into destination
// return the number of elements copied into destination

int common_elements(int length, int source1[length], int source2[length], int destination[length]) {
    int i, j;
    int counter = 0;
    int check1[1000] = {0};
    int check2[1000] = {0};
    for (i = 0; i < 1000; i++) {
        check1[i] = 0;
        check2[i] = 0;
    }
    for ( i = 0; i <= length - 1; i++) {
        int good = 0;
        for (j = 0; j <= length - 1; j++) {
            if (source1[i] == source2[j] ) {
                good = 1;
                break;
            }
        }
        if (good == 1) {
            destination[counter] = source1[i];
            counter += 1;
        }
    }

    return counter;
}

// You may optionally add a main function to test your common_elements function.
// It will not be marked.
// Only your common_elements function will be marked.
