
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
// @UNSW
// zID:    z5621243
// Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int is_alphabet(int ch){
    int return_value = 0;
    if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
        return_value = 1;
    } else{
        return_value = 0 ;
    }
    return return_value;
}
int is_capital(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            is_it = 1;
        }
    }
    return is_it ;
}
int convert_to_lowercase(int ch){
    if (is_capital(ch) == 1){
        ch += 32;
    }
    return ch;
}
int main(int argc, char *argv[]) {
    printf("Enter a string: ");
    char str[1000];
    str[0] = 0;
    int i = 0;
    while (scanf("%c", &str[i]) != EOF && str[i] != '\n') {
        str[i] = convert_to_lowercase(str[i]);
        if (is_alphabet(str[i]) == 1) {
            i++;
        }
        
    }
    i--;
    int r = i;
    i=0;
    int is = 1;
    while (i <= r) {
        if (str[i] != str[r-i]) {
            is = 0;
            break;
        }
        //printf("%c, %c\n", str[i], str[r-i]);
        i++;
    }
    if (is == 1) {
        printf("String is a palindrome\n");
    } else {
        printf("String is not a palindrome\n");
    }
    return 0;
}
