
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
//@UNSW
//zID:    z5621243
//Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com
int is_alphabet(int ch){
    int return_value = 0;
    if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
        return_value = 1;
    } else{
        return_value = 0 ;
    }
    return return_value;
}
int is_capital(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            is_it = 1;
        }
    }
    return is_it ;
}
int is_lowewcase(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 97 && ch <= 122){
            is_it = 1;
        }
    }
    return is_it ;
}
int swap_case(int ch){
    int return_value = ch;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            ch += 32;
        } else if (ch >= 97 && ch <= 122){
            ch -= 32;
        }
    } else {
        return_value = ch;
    }
    return_value = ch;
    return return_value;
}
int caesar_encrypt(int ch, int shift_num){
    int return_value = 0;
    if (is_alphabet(ch) == 1){
        if (shift_num > 0 ){
            return_value = ch + shift_num % 26;
        } else if (shift_num < 0) {
            return_value = ch + shift_num % 26 ;
        }
    } else {
        return_value = ch;
    }
    if (shift_num > 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90-1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65-1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122-1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97-1;
            }
        }
    }
    if (shift_num < 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90+1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65+1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122+1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97+1;
            }
        }
    }
    return return_value;
}
int convert_to_lowercase(int ch){
    if (is_capital(ch) == 1){
        ch += 32;
    }
    return ch;
}





#include <stdio.h>
#include <math.h>
int main(void){
    int character_freq[256] = {0};
    int ch = 0;
    int shift = 5;
    int count_character = 0;
    //scanf("%d",&shift);
    while (ch != EOF){
        ch = getchar();
        ch = convert_to_lowercase(ch);
        if (is_alphabet(ch) == 1){
            character_freq[ch] += 1;
            count_character += 1;
        }
        
    }
    int i;
    for (i = 97; i <= 122; i++){
        printf("'");
        putchar(i);
        printf("' %lf %d\n",(double)character_freq[i]/count_character,character_freq[i]);
         
    }
    
    return 0;
}
