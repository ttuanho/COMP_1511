
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
//@UNSW
//zID:    z5621243
//Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>


#define N_WORDS_MAX         1000
#define N_CHAR_MAX_IN_WORD    20

int is_alphabet(int ch){
    int return_value = 0;
    if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
        return_value = 1;
    } else{
        return_value = 0 ;
    }
    return return_value;
}
int is_capital(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            is_it = 1;
        }
    }
    return is_it ;
}
int is_lowewcase(int ch){
    int is_it = 0;
    if (is_alphabet(ch) == 1){
        if (ch >= 97 && ch <= 122){
            is_it = 1;
        }
    }
    return is_it ;
}
int swap_case(int ch){
    int return_value = ch;
    if (is_alphabet(ch) == 1){
        if (ch >= 65 && ch <= 90){
            ch += 32;
        } else if (ch >= 97 && ch <= 122){
            ch -= 32;
        }
    } else {
        return_value = ch;
    }
    return_value = ch;
    return return_value;
}
int caesar_encrypt(int ch, int shift_num){
    int return_value = ch;
    if (is_alphabet(ch) == 1){
        if (shift_num > 0 ){
            return_value = ch + shift_num % 26;
        } else if (shift_num < 0) {
            return_value = ch + shift_num % 26 ;
        }
    } else {
        return_value = ch;
    }
    if (shift_num > 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90-1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65-1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122-1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97-1;
            }
        }
    }
    if (shift_num < 0){
        if (is_capital(ch) == 1){
            if (return_value < 65){
                return_value = return_value - 65 + 90+1;
            } else if (return_value > 90){
                return_value = return_value - 90 + 65+1;
            }
        }
        if (is_lowewcase(ch) == 1){
            if (return_value < 97){
                return_value = return_value - 97 + 122+1;
            } else if (return_value > 122){
                return_value = return_value - 122 + 97+1;
            }
        }
    }
    if (shift_num == 0) {
        return_value = ch;
    }
    return return_value;
}

int convert_to_lowercase(int ch){
    if (is_capital(ch) == 1){
        ch += 32;
    }
    return ch;
}


void input_key(int key[26]){
    int i;
    //printf("Input key: ");
    for (i = 0; i <= 25; i++){
        key[i] = getchar();
    }
    //printf("Finishing input key. \n");
}
int encrypt_sub(int character[256], int key[26], int ch){
    int i;
    int count_key = 0;
    if (is_alphabet(ch) == 1){
        for (i = 97; i <= 122; i++){
            character[i] = key[count_key];
            count_key += 1;
        }
        count_key = 0;
        for (i = 65; i <= 90; i++){
            character[i] = key[count_key]-32;
            count_key += 1;
        }
    } else {
        character[ch] = ch;
    }
    return character[ch];
}
int decrypt_sub(int character[256], int key[26], int cipher){
    int i;
    int count_key = 0;
    int return_value = cipher;
    if (is_alphabet(cipher) == 1){
        for (i = 97; i <= 122; i++){
            character[i] = key[count_key];
            if (cipher == key[count_key]){
                return_value = i;
            }
            count_key += 1;
        }
        count_key = 0;
        for (i = 65; i <= 90; i++){
            character[i] = key[count_key]-32;
            if (cipher == key[count_key]-32){
                return_value = i;
            }
            count_key += 1;
        }
    }
    
    return return_value;
}
int input_cipher(int cipher[N_WORDS_MAX]){
    int i = 0;
    //printf("Input Cipher: \n");
    cipher[i] = getchar();
    while (cipher[i] != EOF) {
        if (i == 0){
            i += 1;
        }
        /*printf("%d : ",cipher[i]);
        putchar(cipher[i]);
        printf(", ");*/
        cipher[i] = getchar();
        //cipher[i] = atoi(cipher[i]);
        if (cipher[i] == EOF) {
            i -= 1;
        }
        i += 1;
    }
    //printf("\ni = %d\n",i - 1);
    return i - 1;
}
//classic swap
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
void sort(int character_freq[256], int rank[256]){
    int i,j,min_index;
    int order = 1;
    for (i = 97; i <= 122; i++){
        rank[i] = order;
        order += 1;
    }
    int buffer;
    for (i = 97; i <= 122; i++){
        min_index = i;
        for (j = 97; j <= 122; j++){
            if ((character_freq[j] < character_freq[i]) && (rank[j] > rank[min_index])){
                //printf("%d %d",rank[min_index],rank[j]);
                swap(&rank[min_index], &rank[j]);
                /*printf(" swapped ");
                printf("%d %d \n",rank[min_index],rank[j]);*/
            }
        }
    }
    /*printf("\nOutput rankings of characters:\n");
    for (i = 97; i <= 122; i++){
        printf("char = ");
        putchar(i);
        printf(", freq = %d, rank = %d\n", character_freq[i],rank[i]);
    }*/
}
int return_its_rank_from_char(int character_freq[256], int rank[256], int ch){
    int i,j,min_index;
    int order = 1;
    int return_value = ch;
    for (i = 97; i <= 122; i++){
        rank[i] = order;
        order += 1;
    }
    int buffer;
    for (i = 97; i <= 122; i++){
        min_index = i;
        for (j = 97; j <= 122; j++){
            if ((character_freq[j] < character_freq[i]) && (rank[j] > rank[min_index])){
                //printf("%d %d",rank[min_index],rank[j]);
                swap(&rank[min_index], &rank[j]);
                /*printf(" swapped ");
                 printf("%d %d \n",rank[min_index],rank[j]);*/
            }
        }
    }
    for (i = 97; i <= 122; i++){
        if (i == ch){
            return_value = rank[i];
        }
    }
    return return_value;
}
int return_char_from_its_rank(int character_freq[256], int rank[256],int its_rank){
    int i,j,min_index;
    int order = 1;
    int return_value = 0;
    for (i = 97; i <= 122; i++){
        rank[i] = order;
        order += 1;
    }
    int buffer;
    for (i = 97; i <= 122; i++){
        min_index = i;
        for (j = 97; j <= 122; j++){
            if ((character_freq[j] < character_freq[i]) && (rank[j] > rank[min_index])){
                //printf("%d %d",rank[min_index],rank[j]);
                swap(&rank[min_index], &rank[j]);
                /*printf(" swapped ");
                 printf("%d %d \n",rank[min_index],rank[j]);*/
            }
        }
    }
    for (i = 97; i <= 122; i++){
        if (rank[i] == its_rank){
            return_value = i;
        }
    }
    return return_value;
}

int decrypt_caesar(int ch, int character_freq[256], int character_freq_in_cipher[256]){
    int return_value = ch;
    int i,j;
    
    int rank1[256] = {0};
    int rank2[256] = {0};
    sort(character_freq,rank1);
    sort(character_freq_in_cipher,rank2);
    
    int char_rank_in_cipher = return_its_rank_from_char(character_freq_in_cipher,rank2,ch);
    int corresponding_char = return_char_from_its_rank(character_freq,rank1,char_rank_in_cipher);
    return_value = corresponding_char;
    return return_value;
}
int isAllValidWords(char sampleWords[N_WORDS_MAX][N_CHAR_MAX_IN_WORD],
                    int N_char_in_a_word[N_WORDS_MAX],
                    int N_words_total,
                    char textWords[N_WORDS_MAX][N_CHAR_MAX_IN_WORD],
                    int N_char_in_a_text_word[N_WORDS_MAX],
                    int N_text_words_total) {
    int i, j, a, b;
    int is_it[N_WORDS_MAX] = {0};
    int textWordCounter = 0;
    for (a = 0; a <= N_text_words_total - 1; a++) {
        is_it[a] = 0;
        for (i = 0; i <= N_words_total - 1; i++) {
            if (N_char_in_a_word[i] == N_char_in_a_text_word[a]) {
                int not_yet = 1;
                for (j = 0; j <= N_char_in_a_text_word[a] - 1; j++) {
                    if (sampleWords[i][j] != textWords[a][j]) {
                        not_yet = 0;
                        break;
                    }
                }
                if (not_yet == 1) {
                    is_it[a] = 1;
                }
            }
        }
        textWordCounter += 1;
    }
    int return_value = 1;
    for (i = 0; i <= textWordCounter - 1; i++) {
        if (is_it[i] != 1) {
            return_value = 0;
        }
    }
    return return_value;
}


int main(int argc, char *argv[]) {
    int ch = 0;
    int shift = 5;
    int word_counter = 0;
    char filename[100];
    
    int character_freq[256] = {0};
    int count_character = 0;
    char sampleWords[N_WORDS_MAX][N_CHAR_MAX_IN_WORD];
    int N_char_in_a_word[N_WORDS_MAX];
    int wordCounter = 0;
    int charCounter = 0;
    
    // go through a list of files specified on the command line
    // printing their contents in turn to standard output
    
    int argument = 1;
    while (argument < argc) {
        
        // FILE is an opaque (hidden type) defined in stdio.h
        // "r" indicate we are opening file to read contents
        
        FILE *stream = fopen(argv[argument], "r");
        if (stream == NULL) {
            perror(argv[argument]);  // prints why the open failed
            return 1;
        }
        
        // fgetc returns next the next byte (ascii code if its a text file)
        // from a stream
        // it returns the special value EOF if not more bytes can be read from the stream
        
        int c = fgetc(stream);
        if (is_alphabet(ch) == 1){
            character_freq[ch] += 1;
            count_character += 1;
            sampleWords[wordCounter][wordCounter] = ch;
            charCounter += 1;
            N_char_in_a_word[wordCounter] = charCounter;
        } else {
            charCounter = 0;
            wordCounter += 1;
        }
        // return  bytes from the stream (file) one at a time
        
        while (c != EOF) {
            fputc(c, stdout); // write the byte to standard output
            c = fgetc(stream);
            if (c != EOF) {
                if (is_alphabet(ch) == 1){
                    character_freq[ch] += 1;
                    count_character += 1;
                    sampleWords[wordCounter][wordCounter] = ch;
                    charCounter += 1;
                    N_char_in_a_word[wordCounter] = charCounter;
                } else {
                    charCounter = 0;
                    wordCounter += 1;
                }
            }
        }
        
        argument = argument + 1;
    }
    

    //ch = fgetc(stream);
    /*ch = getchar();
    //while (!feof(stream)) {
    while (ch != EOF) {
        if (ch != EOF) {
            if (is_alphabet(ch) == 1){
                character_freq[ch] += 1;
                count_character += 1;
                sampleWords[wordCounter][wordCounter] = ch;
                charCounter += 1;
                N_char_in_a_word[wordCounter] = charCounter;
            } else {
                charCounter = 0;
                wordCounter += 1;
            }
        }
        ch = getchar();
    }*/
    int N_sampleWords = wordCounter;
    
    //character_frrequency analysis
    int i;
    for (i = 97; i <= 122; i++){
        /*printf("'");
        putchar(i);
        printf("' %lf %d\n",(double)character_freq[i]/count_character,character_freq[i]);*/
        
    }
    
    //input cipher
    int cipher[1000];
    int N_cipher_chracter = input_cipher(cipher);
    
    int character_freq_in_cipher[256] = {0};
    int character[256] = {0};
    int key[26];
    
    
    char cipherWords[N_WORDS_MAX][N_CHAR_MAX_IN_WORD];
    int N_charInCipherWord[N_WORDS_MAX] = {0};
    int N_cipherWords = 0;
    //analysing frequecy of cipher characters
    for (i = 0; i <= N_cipher_chracter; i++){
        if (is_alphabet(cipher[i]) == 1){
            /*printf("i=%d, cipher ascii value = %d, char =  ",i,cipher[i]);
            putchar(cipher[i]);
            printf(" | ");*/
            cipher[i] = (int) cipher[i];
            character_freq_in_cipher[convert_to_lowercase(cipher[i])] += 1;
            
            cipherWords[N_cipherWords][N_charInCipherWord[i]] = convert_to_lowercase(cipher[i]);
            //putchar(cipherWords[N_cipherWords][N_charInCipherWord[i]]); printf(" ");
            N_charInCipherWord[i] += 1;
        } else {
            N_cipherWords += 1;
            N_charInCipherWord[i + 1] = 0;
        }
    }
    int rank1[256] = {0};
    int rank2[256] = {0};
    
    //ouput frequecy of cipher characters
    //printf("Frequency of char in cipher: \n");
    for (i = 97; i <= 122; i++){
        
        /*printf("'");
        putchar(i);
        printf("' %lf %d -> ",(double)character_freq_in_cipher[i]/N_cipher_chracter,character_freq_in_cipher[i]);
        putchar(decrypt_caesar(i,character_freq,character_freq_in_cipher));
        printf("\n");*/
        
    }
    //Tesing areas
    //Return its rank from char
    //printf("Testing\n");
    /*for (i = 97; i <= 122; i++){
        printf("In sample text, '");
        putchar(i);
        printf("' has rank of %d\n", return_its_rank_from_char(character_freq,rank1,i));
        printf("In cipher, '");
        putchar(i);
        printf("' has rank of %d ", return_its_rank_from_char(character_freq_in_cipher,rank2,i));
        printf("which correspond with letter '");
        putchar(return_char_from_its_rank(character_freq,rank1, return_its_rank_from_char(character_freq_in_cipher,rank2,i)));
        printf("' in sample text\n");
    }*/
    
    //Output plain text
    for (i = 0; i <= N_cipher_chracter; i++){
        if (cipher[i] != EOF){
            /*printf("%d : ",cipher[i]);
            putchar(cipher[i]);
            printf(" -> ");*/
            if (is_alphabet(cipher[i]) == 1){
                if (is_capital(cipher[i]) == 1){
                    //printf("%d : ",return_char_from_its_rank(character_freq,rank1, return_its_rank_from_char(character_freq_in_cipher,rank2,i))-32);
                    //putchar(return_char_from_its_rank(character_freq,rank1, return_its_rank_from_char(character_freq_in_cipher,rank2,cipher[i]))-32);
                    putchar(decrypt_caesar(convert_to_lowercase(cipher[i]),character_freq,character_freq_in_cipher)-32);
                } else {
                    
                    //putchar(return_char_from_its_rank(character_freq,rank1, return_its_rank_from_char(character_freq_in_cipher,rank2,cipher[i])));
                    putchar(decrypt_caesar(cipher[i],character_freq,character_freq_in_cipher));
                }
            } else {
                //printf("not cipher: ");
                putchar(cipher[i]);
            }
            //printf(", ");
        }
    }
    //Output cipher again
    /*printf("Output cipher again: \n");
    for (i = 0; i <= N_cipher_chracter; i++){
        if (is_alphabet(cipher[i]) == 1){
            printf("i=%d, cipher ascii value = %d, char =  ",i,cipher[i]);
             putchar(cipher[i]);
             printf(" | ");
            cipher[i] = (int) cipher[i];
            //character_freq_in_cipher[convert_to_lowercase(cipher[i])] += 1;
        }
    }*/
    
    //fclose(stream);
}

