#include <stdio.h>
#include <stdlib.h>

struct node {
    struct node *next;
    int         data;
};

int main(int argc, char *argv[]) {
    struct node *head;
    struct node *last;

    last = malloc(sizeof (struct node));
    head = last;

    last->data = 42;
    last->next = malloc(sizeof (struct node));
    last = last->next;
    last->data = 23;
    last->next = malloc(sizeof (struct node));;
    last = last->next;
    last->data = 11;
    last->next = NULL;

    // prints 42 23 11
    for (struct node *p = head; p != NULL; p = p->next) {
        printf("%d ", p->data);
    }
    printf("\n");

    return 0;
}