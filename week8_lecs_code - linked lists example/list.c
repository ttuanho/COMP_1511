
//Coded by
//
//.----------------.  .----------------.  .----------------.  .-----------------.  .----------------.  .----------------.
//| .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. |
//| |  _________   | || | _____  _____ | || |      __      | || | ____  _____  | | | |  ____  ____  | || |     ____     | |
//| | |  _   _  |  | || ||_   _||_   _|| || |     /  \     | || ||_   \|_   _| | | | | |_   ||   _| | || |   .'    `.   | |
//| | |_/ | | \_|  | || |  | |    | |  | || |    / /\ \    | || |  |   \ | |   | | | |   | |__| |   | || |  /  .--.  \  | |
//| |     | |      | || |  | '    ' |  | || |   / ____ \   | || |  | |\ \| |   | | | |   |  __  |   | || |  | |    | |  | |
//| |    _| |_     | || |   \ `--' /   | || | _/ /    \ \_ | || | _| |_\   |_  | | | |  _| |  | |_  | || |  \  `--'  /  | |
//| |   |_____|    | || |    `.__.'    | || ||____|  |____|| || ||_____|\____| | | | | |____||____| | || |   `.____.'   | |
//| |              | || |              | || |              | || |              | | | |              | || |              | |
//| '--------------' || '--------------' || '--------------' || '--------------' | | '--------------' || '--------------' |
//'----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'
//
//
// @UNSW
// zID:    z5621243
// Email:  tuan.ho@student.unsw.edu.au
//        hhoanhtuann@gmail.com

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct node {
    struct node *next;
    int data;
};

int sum_list(struct node *head);

int main(int argc, char *argv[]) {
    printf("\nSize of struct is %lu\n", sizeof(struct node));
    struct node *a =  malloc(sizeof (struct node));
    struct node *b =  malloc(sizeof (struct node));
    struct node *c =  malloc(sizeof (struct node));
    struct node *d =  malloc(sizeof (struct node));
    
    printf("a=%p a->data=%d a->next=%p\n", a, a->data, a->next);
    printf("b=%p b->data=%d b->next=%p\n", b, (*b).data, (*b).next);
    printf("c=%p c->data=%d c->next=%p\n", c, c->data, c->next);
    printf("d=%p d->data=%d d->next=%p\n", d, d->data, d->next);
    
    struct node n;
    struct node *other = &n;
    n.data = 42;
    n.next = NULL;
    printf("\nSize of n = %lu\n", sizeof n);
    printf("Size of a = %lu\n", sizeof a);
    
    return 0;
}
