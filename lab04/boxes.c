//
//Coded by Tuan Ho
// z5261243

#include<stdio.h>

int main(void){
    printf("Enter size: ");
    
    int n;
    scanf("%d",&n);
    
    int i,j;
    
    int lim = n*3+1;
    i=1;
    while(i<=lim){
        j=1;
        while(j<=lim){
            if ( ((j==i)||(j==(lim-i)))&&(i%2==0)  ||  ((i<lim/2)&&(j<lim/2)&&(((i%2==0)&&(j>i+2))||((j%2==0)&&(j>i+2)))) ){
                printf("#");
            }else {
                printf(" ");
            }
            j+=1;
        }
        printf("\n");
        i+=1;
    }
    
    return 0;
}
