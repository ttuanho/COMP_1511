//
//Coded by Tuan Ho
// z5261243

#include<stdio.h>

int main(void){
    printf("Enter size: ");
    
    int n;
    scanf("%d",&n);
    //int diff = n-2;
    int i=1;
    while(i<=n){
        int j=1;
        while (j<=n){
            if (i==j || n+1-i == j){
                printf("*");
            }else {
                printf("-");
            }
            j+=1;
        }
        printf("\n");
        i+=1;
    }
    
    return 0;
}
