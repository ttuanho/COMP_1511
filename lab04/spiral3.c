//Challenge : deicmal_spiral
//Coded by Tuan Ho


#include<stdio.h>

int main(int argc, char const *argv[])
{
	printf("Enter Size: ");
	int n;
	scanf("%d",&n);
	int row,column,check;
	n=n/2;
	//printf("%d\n",n);

	// Calculate series total
	int series_total = n+ (n-1)*(1+(n-1)/2);
	int t,sum,diff=0;
	for (t=0;t<=series_total-1;t++){
		sum+=diff; //printf("sum = %d, ",sum);
		if (diff == n+1){
			diff = 0;
		}
		diff+=1; //printf("diff = %d | ",diff);

	}
    sum+=2;
    //printf("sum = %d\n", sum);
	int condition;

	row = -n-1;
	while(row<=n+1){
	    //printf("\nrow = %d:\n",row );
	    if(row != 0){
	    	column =-n;
	    	while(column<=n){
		           check=(row!=column)||row>0;
		           condition = n+((column*column<row*row)*row+!(column*column<row*row)*(check+column))&1;
		           //printf("col = %d, check = %d, con = %d ;\n ",column,check,condition);
		           if (condition==1){
		           		printf("*");
		           		//printf("%d",(sum+ row*( (-1)*(column<0)+1*(column>0) )   + column*((-1)*(row%2)*(row>0)+1*(row<0) ) )%10 );
		           		//printf("%d",(sum+ row + column)%10 );
		           }
		           else {
		           		printf("-");
		           }
		           column+=1;
	         }
	      	printf("\n");
	     }
	    row += 1;
 	}


	return 0;
}