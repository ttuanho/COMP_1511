//Coded by Tuan Ho
#include<stdio.h>

int main(void){
    printf("Enter number: ");
    int i=1,n,sum=0;
    scanf("%d",&n);
    printf("The factors of %d are:\n",n);
    while(i<=n){
        if (n%i == 0){
            sum+=i;
            printf("%d\n",i);
        }
        i+=1;
    }
    printf("Sum of factors = %d\n",sum);
    if (sum/2 == n){
        printf("%d is a perfect number\n",n);
    } else{
        printf("%d is not a perfect number\n",n);
    }
    
    return 0;
}
