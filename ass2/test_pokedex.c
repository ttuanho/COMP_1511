// Assignment 2 19T1 COMP1511: Pokedex
// test_pokedex.c
//
// This program was written by  TUAN HO (z5261243)
// on 27/4/19
//
// Version 1.0.0: Assignment released.
// Version 1.0.1: Added pointer check for the provided tests.


#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "pokedex.h"


#define MAX_CHAR_IN_STR         20
#define MAX_POKEMON          32766
#define MIN_STR                  5
#define N_POKEMON_TYPE          18
#define N_APHABET               26

// Define the strlen of "--> #XXX: "
#define PRINT_POKEMON_INDENT    10
// Define the strlen of "pokemon"
#define POKEMON_LEN              7
#define FIRST_LEN                5
#define SECOND_LEN               6
#define THIRD_LEN                5
#define FOURTH_LEN               6
#define FIFTH_LEN                5

#define FIRSTPOKEMON_NAME_LEN      FIRST_LEN + POKEMON_LEN
#define SECONDPOKEMON_NAME_LEN     SECOND_LEN + POKEMON_LEN
#define THIRDPOKEMON_NAME_LEN      THIRD_LEN + POKEMON_LEN
#define FOURTHPOKEMON_NAME_LEN     FOURTH_LEN + POKEMON_LEN
#define FIFTHPOKEMON_NAME_LEN      FIFTH_LEN + POKEMON_LEN

#define N_POKEMON_BELOW_100        3
#define RAND_SEED                  10
#define MAX_ID1                    100
#define N_ENCOUNTER                1000
/*
// Adding pokemons commands
a 33 firstPokemon 3 3 rock grass
a 555 secondPokemon 3 3 flying fire
a 22 thirdPokemon 4 4 Fairy poison

a 444 fourthPokemon 5 5 flying grass
a 1 fifthPokemon 7 8 poison 

*/
/*
The order of pokemon by id
1   fifthPokemon
2   thirdPokemon
3   firstPokemon 
4   fourthPokemon
5   secondPokemon
*/
//Sample data on firstPokemon, the Pokemon with pokemon_id 3
#define FIRSTPOKEMON_ID 33 
#define FIRSTPOKEMON_NAME "firstPokemon"
#define FIRSTPOKEMON_HEIGHT 8.3
#define FIRSTPOKEMON_WEIGHT 1.0
#define FIRSTPOKEMON_FIRST_TYPE ROCK_TYPE
#define FIRSTPOKEMON_SECOND_TYPE GRASS_TYPE


//Sample data on secondPokemon, the Pokemon with pokemon_id 5
#define SECONDPOKEMON_ID 555
#define SECONDPOKEMON_NAME "secondPokemon"
#define SECONDPOKEMON_HEIGHT 0.3
#define SECONDPOKEMON_WEIGHT 3.7
#define SECONDPOKEMON_FIRST_TYPE FLYING_TYPE
#define SECONDPOKEMON_SECOND_TYPE FIRE_TYPE


//Sample data on thirdPokemon, the Pokemon with pokemon_id 2
#define THIRDPOKEMON_ID 22 
#define THIRDPOKEMON_NAME "thirdPokemon"
#define THIRDPOKEMON_HEIGHT 0.4
#define THIRDPOKEMON_WEIGHT 1.0
#define THIRDPOKEMON_FIRST_TYPE FAIRY_TYPE
#define THIRDPOKEMON_SECOND_TYPE POISON_TYPE


//Sample data on fourthPokemon, the Pokemon with pokemon_id 4
#define FOURTHPOKEMON_ID 444 
#define FOURTHPOKEMON_NAME "fourthPokemon"
#define FOURTHPOKEMON_HEIGHT 2.0
#define FOURTHPOKEMON_WEIGHT 0.5
#define FOURTHPOKEMON_FIRST_TYPE STEEL_TYPE
#define FOURTHPOKEMON_SECOND_TYPE FLYING_TYPE


//Sample data on fifthPokemon, the Pokemon with pokemon_id 1
#define FIFTHPOKEMON_ID 1 
#define FIFTHPOKEMON_NAME "fifthPokemon"
#define FIFTHPOKEMON_HEIGHT 0.2
#define FIFTHPOKEMON_WEIGHT 0.1
#define FIFTHPOKEMON_FIRST_TYPE GRASS_TYPE
#define FIFTHPOKEMON_SECOND_TYPE NORMAL_TYPE

static void green();
static void red();
static void yellow();
static void normalColor();
static void printPassed();
static void printFailed();
static int isAlphabet(int ch);
static int lowerChar(int ch);
static int upperChar(int ch);
static int randLowercase();
static int randUppercase();
static char *randString(int N_chars);
static void rand_str(char *dest, int length);
static void upperStr(const char *str);
static void lowerStr(const char *str);

static void indPrintf(const char *str);
static void ind2Printf(const char *str);

// Tests for Pokedex functions from pokedex.c.
static void test_new_pokedex(void);
static void test_add_pokemon(void);
static void test_get_found_pokemon(void);
static void test_next_pokemon(void);

// Helper functions for creating/comparing Pokemon.
static Pokemon create_bulbasaur(void);
static Pokemon create_ivysaur(void);
static int is_same_pokemon(Pokemon first, Pokemon second);
static int is_copied_pokemon(Pokemon first, Pokemon second);


// Prototypes of creating nodes here
static Pokemon createFirstPokemon(void);
static Pokemon createSecondPokemon(void);
static Pokemon createThirdPokemon(void);
static Pokemon createFourthPokemon(void);
static Pokemon createFifthPokemon(void);

static void mainTesting(void);
// Prototypes of testing functions here
static void test1(void);
static void test2(void);
static void test3(void);
static void test4(void);
static void test5(void);
static void test6(void);
int main(int argc, char *argv[]) {
    mainTesting();
}

static void mainTesting(void) {
    test1();
    test2();
    test4();
    test5();
    test6();
}


static void test1() {
    printf("=====================================\n");
    yellow();
    printf("Test 1\n\n");
    printf("    Testing new_pokdex function\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Creating pokenode firstPokemon\n");
    Pokemon firstPokemon = createFirstPokemon();
    printf("    ... Creating pokenode secondPokemon\n");
    Pokemon secondPokemon = createSecondPokemon();
    printf("    ... Creating pokenode thirdPokemon\n");
    Pokemon thirdPokemon = createThirdPokemon();
    printf("    ... Creating pokenode fourthPokemon\n");
    Pokemon fourthPokemon = createFourthPokemon();
    printf("    ... Creating pokenode fifthPokemon\n");
    Pokemon fifthPokemon = createFifthPokemon();
    printf("\n");
    printf("    ... Adding firstPokemon to the pokedex\n");
    add_pokemon(pokedex, firstPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding secondPokemon to the pokedex\n");
    add_pokemon(pokedex, secondPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Removing the current pokemon firstPokemon from the pokedex\n");
    remove_pokemon(pokedex);
    printf("       ==> Checking the total number of Pokemons in the pokedex \n");
    assert(count_total_pokemon(pokedex) == 1);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == secondPokemon);

    printf("    ... Adding thirdPokemon to the pokedex\n");
    add_pokemon(pokedex, thirdPokemon);
    printf("    ... Adding fourthPokemon to the pokedex\n");
    add_pokemon(pokedex, fourthPokemon);
    printf("    ... Adding fifthPokemon to the pokedex\n");
    add_pokemon(pokedex, fifthPokemon);

    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == secondPokemon);

    yellow();
    printf("    Testing change_current_pokemon\n");
    normalColor();
    printf("    ... Changing the current node to the pokemon fourthPokemon with id 444 \n");
    change_current_pokemon(pokedex, 444);
    printf("       ==> Checking if the current pokemon is fourthPokemon \n");
    assert(get_current_pokemon(pokedex) == fourthPokemon);

    yellow();
    printf("    Testing remove_pokemon\n");
    normalColor();
    printf("    ... Removing the current pokemon fourthPokemon from the pokedex\n");
    remove_pokemon(pokedex);
    printf("       ==> Checking the total number of Pokemons in the pokedex \n");
    assert(count_total_pokemon(pokedex) == 3);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == fifthPokemon);


    yellow();
    printf("    Testing function prev_pokemon, next_pokemon, find_current_pokemon\n");
    normalColor();
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 1);
    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == thirdPokemon);

    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 2);

    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == secondPokemon);

    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 3);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == thirdPokemon);
    

    yellow();
    printf("    Testing function destroy_pokedex\n");
    normalColor();
    printf("    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);
    printf("\n");
    green();
    printf("Passed Test 1");
    printf("\n");
    normalColor();
    printf("\n");

}
static void test2(void) {
    printf("=====================================\n");
    yellow();
    printf("Test 2\n");
    printf("    Testing new_pokdex function");
    printf("\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Checking the pokedex is not NULL\n");
    assert(pokedex != NULL);

    yellow();
    printf("    Testing function add_pokemon, get_current_pokemon\n");
    normalColor();

    printf("    ... Creating pokenode firstPokemon\n");
    Pokemon firstPokemon = createFirstPokemon();
    printf("    ... Creating pokenode secondPokemon\n");
    Pokemon secondPokemon = createSecondPokemon();
    printf("    ... Creating pokenode thirdPokemon\n");
    Pokemon thirdPokemon = createThirdPokemon();
    printf("    ... Creating pokenode fourthPokemon\n");
    Pokemon fourthPokemon = createFourthPokemon();
    printf("    ... Creating pokenode fifthPokemon\n");
    Pokemon fifthPokemon = createFifthPokemon();
    printf("    ... Adding firstPokemon to the pokedex\n");
    add_pokemon(pokedex, firstPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding secondPokemon to the pokedex\n");
    add_pokemon(pokedex, secondPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding thirdPokemon to the pokedex\n");
    add_pokemon(pokedex, thirdPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fourthPokemon to the pokedex\n");
    add_pokemon(pokedex, fourthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fifthPokemon to the pokedex\n");
    add_pokemon(pokedex, fifthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type with empty pokedex\n");
    normalColor();
    printf("    ... Getting pokemon type water\n");
    Pokedex waterPokemons = get_pokemon_of_type(pokedex, 4);
    printf("        ==> Checking if the new pokedex is empty\n");
    assert(count_total_pokemon(waterPokemons) == 0);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(waterPokemons);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type again with empty pokedex\n");
    normalColor();
    printf("    ... Getting pokemon type poison\n");
    Pokedex poisonPokemons = get_pokemon_of_type(pokedex, POISON_TYPE);
    printf("        ==> Checking if the new pokedex is empty\n");
    assert(count_total_pokemon(poisonPokemons) == 0);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(poisonPokemons);
    printf("\n");
    yellow();
    printf("    Testing function find_current_pokemon, next_pokemon\n");
    normalColor();
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 1);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == secondPokemon);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 2);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == fourthPokemon);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == fifthPokemon);

    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == thirdPokemon);

    yellow();
    printf("    Testing function get_pokemon_of_type again with one pokemon\n");
    normalColor();
    printf("    ... Getting pokemon type poison\n");
    Pokedex poisonPokemons2 = get_pokemon_of_type(pokedex, POISON_TYPE);
    printf("        ==> Checking if the new pokedex have one pokemon\n");
    assert(count_total_pokemon(poisonPokemons2) == 1);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(poisonPokemons2);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type again with many pokemons\n");
    normalColor();
    printf("    ... Getting pokemon type flying\n");
    Pokedex flying = get_pokemon_of_type(pokedex, FLYING_TYPE);
    printf("        ==> Checking the total number of pokemons in the new pokedex\n");
    assert(count_total_pokemon(flying) == 2);
    printf("        ==> Checking the current pokemon of the new pokedex is not the same\n");
    assert(get_current_pokemon(flying) != secondPokemon);
    printf("        ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(flying), fourthPokemon));

    printf("        ... Get pokemon that have \" tH\" in its name from the new pokedex\n ");
    Pokedex tHPokemons = search_pokemon(flying, "tH");
    printf("            ==> Checking the total number of pokemons in the new pokedex\n");
    assert(count_total_pokemon(tHPokemons) == 1);
    printf("            ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(tHPokemons),  fourthPokemon));

    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(tHPokemons);

    yellow();
    printf("    Testing function get_found_pokemon + find_current_pokemon + next_pokemon\n");

    normalColor();
    printf("    ... Get found pokemons\n");
    Pokedex foundPokemons = get_found_pokemon(pokedex);
    printf("        ==> Checking the total number of pokemons in the new pokedex\n");
    //assert(count_total_pokemon(foundPokemons) == 5);
    printf("        ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons),  fifthPokemon));

    yellow();
    printf("        Also, testing get_found_pokemon pokedex has correct order of id\n");
    normalColor();
    /*
    The order of pokemon by id
    1   fifthPokemon
    2   thirdPokemon
    3   firstPokemon 
    4   fourthPokemon
    5   secondPokemon
    */
    printf("        ==> Check the new pokedex created have ascending order of id\n");
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), thirdPokemon));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), firstPokemon));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), fourthPokemon));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), secondPokemon));
    green();
    printf("            Hooray! The found pokedex has the ids in correct order!\n");
    normalColor();
    printf("        ... Destroy the lately created pokedex\n");
    destroy_pokedex(foundPokemons);

    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(flying);

    printf("    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);
    printf("\n");
    green();
    printf("Passed Test2");
    printf("\n");
    normalColor();
    printf("\n");

}

// Testing stage 4 (evolutions) funcs
static void test4(void) {
    printf("======================================================\n");
    yellow();
    printf("Test 4 : Testing stage 4 (evolutions) funcs\n");
    printf("\n");
    normalColor();
    printf("\n");

    // Creating a pokedex
    printf("    Testing new_pokdex function");
    printf("\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Checking the pokedex is not NULL\n");
    assert(pokedex != NULL);

    // If the Pokedex is empty, the function get_next_evolution should print an appropriate
    // error message and exit the program.
    // assert(get_next_evolution(pokedex) == DOES_NOT_EVOLVE);

    yellow();
    printf("    Testing function add_pokemon, get_current_pokemon\n");
    normalColor();

    printf("    ... Creating pokenode firstPokemon\n");
    Pokemon firstPokemon = createFirstPokemon();
    printf("    ... Creating pokenode secondPokemon\n");
    Pokemon secondPokemon = createSecondPokemon();
    printf("    ... Creating pokenode thirdPokemon\n");
    Pokemon thirdPokemon = createThirdPokemon();
    printf("    ... Creating pokenode fourthPokemon\n");
    Pokemon fourthPokemon = createFourthPokemon();
    printf("    ... Creating pokenode fifthPokemon\n");
    Pokemon fifthPokemon = createFifthPokemon();
    printf("    ... Adding firstPokemon to the pokedex\n");
    add_pokemon(pokedex, firstPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding secondPokemon to the pokedex\n");
    add_pokemon(pokedex, secondPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding thirdPokemon to the pokedex\n");
    add_pokemon(pokedex, thirdPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fourthPokemon to the pokedex\n");
    add_pokemon(pokedex, fourthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fifthPokemon to the pokedex\n");
    add_pokemon(pokedex, fifthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("\n");

    // Testing stage 4 funcs
    yellow();
    printf("    Testing function get_next_evolution, add_pokemon_evolution\n");
    normalColor();

    printf("    ==> Checking there's no evolution for the current pokemon\n");
    assert(get_next_evolution(pokedex) == DOES_NOT_EVOLVE);
    printf("    ... Adding evolution from id %d -> %d\n", SECONDPOKEMON_ID, FIFTHPOKEMON_ID);
    add_pokemon_evolution(pokedex, SECONDPOKEMON_ID, FIFTHPOKEMON_ID);
    printf("    ==> Checking there's still no evolution for the current pokemon\n");
    assert(get_next_evolution(pokedex) == DOES_NOT_EVOLVE);
    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ==> Checking the correct evolved pokemon \n");
    assert(get_next_evolution(pokedex) == FIFTHPOKEMON_ID);
    
    printf("\n");

    yellow();
    printf("    Testing overwriting the add_pokemon_evolution func\n");
    normalColor();

    printf("    ... Over-writing the adding evolution from id %d -> %d\n", SECONDPOKEMON_ID, THIRDPOKEMON_ID);
    add_pokemon_evolution(pokedex, SECONDPOKEMON_ID, THIRDPOKEMON_ID);
    printf("    ==> Checking the correct evolved pokemon \n");
    assert(get_next_evolution(pokedex) == THIRDPOKEMON_ID);
    printf("    ... Over-writing the adding evolution from id %d -> %d\n", SECONDPOKEMON_ID, FOURTHPOKEMON_ID);
    add_pokemon_evolution(pokedex, SECONDPOKEMON_ID, FOURTHPOKEMON_ID);
    printf("    ==> Checking the correct evolved pokemon \n");
    assert(get_next_evolution(pokedex) == FOURTHPOKEMON_ID);
    
    // If there is no Pokemon with the ID `from_id` or `to_id`,
    // or if the provided `from_id` and `to_id` are the same,
    // this function should print an appropriate error message and exit the
    // program.
    // printf("    ... Add revolution but with invalid to id\n");
    // add_pokemon_evolution(pokedex, SECONDPOKEMON_ID, SECONDPOKEMON_ID);

    printf("\n    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf("\nPassed Test 4");
    printf("\n");
    normalColor();
    printf("\n");
}

// Testing remove_pokemon + search_pokemon funcs 
static void test5(void) {
    printf("======================================================\n");
    yellow();
    printf("Test 5: Testing if evolved pokemon copied to the new search_pokedex"
            "\n     Testing remove_pokemon + search_pokemon funcs and others \n\n");
    printf("    Testing new_pokdex function");
    printf("\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Checking the pokedex is not NULL\n");
    assert(pokedex != NULL);

    yellow();
    printf("    Testing function add_pokemon, get_current_pokemon againg\n");
    normalColor();

    printf("    ... Creating pokenode firstPokemon\n");
    Pokemon firstPokemon = createFirstPokemon();
    printf("    ... Creating pokenode secondPokemon\n");
    Pokemon secondPokemon = createSecondPokemon();
    printf("    ... Creating pokenode thirdPokemon\n");
    Pokemon thirdPokemon = createThirdPokemon();
    printf("    ... Creating pokenode fourthPokemon\n");
    Pokemon fourthPokemon = createFourthPokemon();
    printf("    ... Creating pokenode fifthPokemon\n");
    Pokemon fifthPokemon = createFifthPokemon();
    printf("    ... Adding firstPokemon to the pokedex\n");
    add_pokemon(pokedex, firstPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding secondPokemon to the pokedex\n");
    add_pokemon(pokedex, secondPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding thirdPokemon to the pokedex\n");
    add_pokemon(pokedex, thirdPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fourthPokemon to the pokedex\n");
    add_pokemon(pokedex, fourthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fifthPokemon to the pokedex\n");
    add_pokemon(pokedex, fifthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("\n");

    // Setting all pokemons to be found
    yellow();
    printf("    Testing change_current_pokemon, find_pokemon\n");
    normalColor();
    printf("\n    ... Setting all pokemons to be found\n");
    find_current_pokemon(pokedex);
    next_pokemon(pokedex);
    find_current_pokemon(pokedex);
    next_pokemon(pokedex);
    find_current_pokemon(pokedex);
    next_pokemon(pokedex);
    find_current_pokemon(pokedex);
    next_pokemon(pokedex);
    find_current_pokemon(pokedex);
    printf("        ==> Checking the number of Pokemons found\n\n");
    assert(count_found_pokemon(pokedex) == 5);

    // Add some random evolutions to expected new pokedex
    printf("    ... Adding evolution from id %d -> %d\n", FOURTHPOKEMON_ID, FIFTHPOKEMON_ID);
    add_pokemon_evolution(pokedex, FOURTHPOKEMON_ID, FIFTHPOKEMON_ID);
    printf("    ... Change the current pokemon to the one with id %d\n", FOURTHPOKEMON_ID);
    change_current_pokemon(pokedex, FOURTHPOKEMON_ID);
    printf("        ==> Checking the correct evolved pokemon \n");
    assert(get_next_evolution(pokedex) == FIFTHPOKEMON_ID);


    // Check the evolved pokemon is not in search_found pokedex----
    yellow();
    printf("    Testing if evolved pokemon copied to the new search_pokedex\n");
    normalColor();
    printf("    ... Get pokemon that have \" F\" in its name from the new pokedex\n ");
    Pokedex FPokemons = search_pokemon(pokedex, "F");
    printf("            ==> Checking the total number of pokemons in the new pokedex\n");
    assert(count_total_pokemon(FPokemons) == 3);
    printf("            ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(FPokemons),  firstPokemon));
    printf("        ... Change the current pokemon to the one with id %d\n", FOURTHPOKEMON_ID);
    change_current_pokemon(FPokemons, FOURTHPOKEMON_ID);
    printf("            ==> Checking that the current Pokemon is \n");    
    assert(get_current_pokemon(pokedex) ==  fourthPokemon);
    printf("            ==> Checking the current pokemon doesn't have evolved pokemon\n");
    assert(get_next_evolution(FPokemons) == DOES_NOT_EVOLVE);

    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(FPokemons);
    //---------------------------------

    printf("\n    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf("\nPassed Test 5");
    printf("\n");
    normalColor();
    printf("\n");
}
static void test6(void) {
    printf("======================================================\n");
    yellow();
    printf("Test 6\n");
    printf("    Testing go_exploring, remove_pokemon function");
    printf("\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Checking the pokedex is not NULL\n");
    assert(pokedex != NULL);

    yellow();
    printf("    Testing function add_pokemon, get_current_pokemon\n");
    normalColor();

    printf("    ... Creating pokenode firstPokemon\n");
    Pokemon firstPokemon = createFirstPokemon();
    printf("    ... Creating pokenode secondPokemon\n");
    Pokemon secondPokemon = createSecondPokemon();
    printf("    ... Creating pokenode thirdPokemon\n");
    Pokemon thirdPokemon = createThirdPokemon();
    printf("    ... Creating pokenode fourthPokemon\n");
    Pokemon fourthPokemon = createFourthPokemon();
    printf("    ... Creating pokenode fifthPokemon\n");
    Pokemon fifthPokemon = createFifthPokemon();

    printf("    ... Adding firstPokemon to the pokedex\n");
    add_pokemon(pokedex, firstPokemon);
    printf("    ... Adding secondPokemon to the pokedex\n");
    add_pokemon(pokedex, secondPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("    ... Adding thirdPokemon to the pokedex\n");
    add_pokemon(pokedex, thirdPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fourthPokemon to the pokedex\n");
    add_pokemon(pokedex, fourthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);

    printf("    ... Adding fifthPokemon to the pokedex\n");
    add_pokemon(pokedex, fifthPokemon);
    printf("       ==> Checking that the current Pokemon is firstPokemon\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("\n");

    // Testing go_exploring
    yellow();
    printf("    Testing go_exploring function\n");
    normalColor();
    printf("    ... Go exploring with seed = %d, "
           "max id = %d, how_many = %d\n", RAND_SEED, MAX_ID1, N_ENCOUNTER);
    go_exploring(pokedex, RAND_SEED, MAX_ID1, N_ENCOUNTER);
    printf("        ==> Expecting only 3 pokemons id below 100 found. Checking the num of pokemons found\n");
    assert(count_found_pokemon(pokedex) == N_POKEMON_BELOW_100);
    printf("        ... Get found pokedex\n");
    Pokedex found = get_found_pokemon(pokedex);
    printf("            ==> Checking the first pokemon is copy of the Pokemon with smallest id\n");
    assert(is_copied_pokemon(get_current_pokemon(found), fifthPokemon));
    printf("        ... Destroy the lately created pokedex\n");
    destroy_pokedex(found);

    // Testing the remove pokemon func from the bottom of the list
    yellow();
    printf("    Testing the remove_pokemon func from the bottom of the list\n");
    normalColor();
    printf("    ... Move to the last of the pokedex by id %d of the last one\n", FIFTHPOKEMON_ID);
    change_current_pokemon(pokedex, FIFTHPOKEMON_ID);
    printf("        ==> Checking that the current Pokemon is at the last of the pokedex\n");    
    assert(get_current_pokemon(pokedex) == fifthPokemon);
    printf("    ... Remove the current pokemon\n");
    remove_pokemon(pokedex);
    printf("        ==> Check the number of pokemons in the pokemons\n");
    assert(count_total_pokemon(pokedex) == 4);
    printf("       ==> Checking that the current Pokemon is at the last of the pokedex\n");    
    assert(get_current_pokemon(pokedex) == fourthPokemon);
    printf("    ... Remove the current pokemon again!\n");
    remove_pokemon(pokedex);
    printf("        ==> Check the number of pokemons in the pokemons\n");
    assert(count_total_pokemon(pokedex) == 3);
    printf("       ==> Checking that the current Pokemon is at the last of the pokedex\n");    
    assert(get_current_pokemon(pokedex) == thirdPokemon);
    printf("    ... Remove the current pokemon\n");
    remove_pokemon(pokedex);
    printf("        ==> Check the number of pokemons in the pokemons\n");
    assert(count_total_pokemon(pokedex) == 2);
    printf("       ==> Checking that the current Pokemon is at the last of the pokedex\n");    
    assert(get_current_pokemon(pokedex) == secondPokemon);
    printf("    ... Remove the current pokemon\n");
    remove_pokemon(pokedex);
    printf("        ==> Check the number of pokemons in the pokemons\n");
    assert(count_total_pokemon(pokedex) == 1);
    printf("       ==> Checking that the current Pokemon is at the last of the pokedex\n");    
    assert(get_current_pokemon(pokedex) == firstPokemon);
    printf("\n    ... Remove the last and also current pokemon\n");
    remove_pokemon(pokedex);
    printf("        ==> Check the number of pokemons in the pokemons\n");
    assert(count_total_pokemon(pokedex) == 0);


    printf("\n    ... Destroying the empty Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf("\nPassed Test 6");
    printf("\n");
    normalColor();
    printf("\n");
}
////////////////////////////////////////////////////////////////////////
//                     Pokedex Test Functions                         //
////////////////////////////////////////////////////////////////////////


//=======================================================================
// Some of the ways that you could extend these tests would include:
//   = calling the get_found_pokemon function on an empty Pokedex,
//   = calling the get_found_pokemon function on a Pokedex where none of
//     the Pokemon have been found,
//   = checking that the Pokemon in the new Pokedex are in ascending
//     order of pokemon_id (regardless of the order that they appeared
//     in the original Pokedex),
//   = checking that the currently selected Pokemon in the returned
//     Pokedex has been set correctly,
//   = checking that the original Pokedex has not been modified,
//   = ... and more!
//=======================================================================




////////////////////////////////////////////////////////////////////////
//                     Helper Functions                               //
////////////////////////////////////////////////////////////////////////

// Helper function to create firstPokemon for testing purposes.
static Pokemon createFirstPokemon(void) {
    Pokemon pokemon = new_pokemon(
        FIRSTPOKEMON_ID, 
        FIRSTPOKEMON_NAME,
        FIRSTPOKEMON_HEIGHT, 
        FIRSTPOKEMON_WEIGHT,
        FIRSTPOKEMON_FIRST_TYPE,
        FIRSTPOKEMON_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create secondPokemon for testing purposes.
static Pokemon createSecondPokemon(void) {
    Pokemon pokemon = new_pokemon(
        SECONDPOKEMON_ID, 
        SECONDPOKEMON_NAME,
        SECONDPOKEMON_HEIGHT, 
        SECONDPOKEMON_WEIGHT,
        SECONDPOKEMON_FIRST_TYPE,
        SECONDPOKEMON_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create thirdPokemon for testing purposes.
static Pokemon createThirdPokemon(void) {
    Pokemon pokemon = new_pokemon(
        THIRDPOKEMON_ID, 
        THIRDPOKEMON_NAME,
        THIRDPOKEMON_HEIGHT, 
        THIRDPOKEMON_WEIGHT,
        THIRDPOKEMON_FIRST_TYPE,
        THIRDPOKEMON_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create fourthPokemon for testing purposes.
static Pokemon createFourthPokemon(void) {
    Pokemon pokemon = new_pokemon(
        FOURTHPOKEMON_ID, 
        FOURTHPOKEMON_NAME,
        FOURTHPOKEMON_HEIGHT, 
        FOURTHPOKEMON_WEIGHT,
        FOURTHPOKEMON_FIRST_TYPE,
        FOURTHPOKEMON_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create fifthPokemon for testing purposes.
static Pokemon createFifthPokemon(void) {
    Pokemon pokemon = new_pokemon(
        FIFTHPOKEMON_ID, 
        FIFTHPOKEMON_NAME,
        FIFTHPOKEMON_HEIGHT, 
        FIFTHPOKEMON_WEIGHT,
        FIFTHPOKEMON_FIRST_TYPE,
        FIFTHPOKEMON_SECOND_TYPE
    );
    return pokemon;
}


static int is_same_pokemon(Pokemon first, Pokemon second) {
    return first == second;
}

// Helper function to compare whether one Pokemon is a *copy* of
// another, based on whether their attributes match (e.g. pokemon_id,
// height, weight, etc).
// 
// It also checks that the pointers do *not* match == i.e. that the
// pointers aren't both pointing to the same pokemon struct in memory.
// If the pointers both contain the same address, then the second
// Pokemon is not a *copy* of the first Pokemon.
// 
// This function doesn't (yet) check that the Pokemon's names match
// (but perhaps you could add that check yourself...).
static int is_copied_pokemon(Pokemon first, Pokemon second) {
    return (pokemon_id(first) == pokemon_id(second))
    &&  (first != second)
    &&  (pokemon_height(first) == pokemon_height(second))
    &&  (pokemon_weight(first) == pokemon_weight(second))
    &&  (pokemon_first_type(first) == pokemon_first_type(second))
    &&  (pokemon_second_type(first) == pokemon_second_type(second));
}
// ==============   HELPER FUNCTIONS    ================== //

// Print passed sign in color
static void printPassed() {
    green();
    printf("passed\n");
    normalColor();
}

// Print failed sign in color
static void printFailed() {
    red();
    printf("failed\n");
    normalColor();
}

// Change output color to green
static void green() {
    printf("\033[1;32m");
}
// Change output color to red
static void red() {
    printf("\033[1;31m");
}
// Change output color to yellow
static void yellow() {
    printf("\033[1;33m");
}
// Change output color to normal
static void normalColor() {
    printf("\033[0m");
}
// Return if a character is alphabetical
static int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        || (ch >= 'A' && ch <= 'Z')) {
        return 1;
    } else {
        return 0;
    }
}
// Convert a character to lowercase
static int lowerChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'A' && ch <= 'Z') {
            newCh = ch + 32;
        }
    } 
    return newCh;
}
// Convert a character to uppercase
static int upperChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'a' && ch <= 'z') {
            newCh = ch = 32;
        }
    } 
    return newCh;
}
// Return a random Uppercase characters
static int randUppercase() {
    return 'A' + rand() % ('Z'-'A' + 1);
}
// Return a random lowercase characters
static int randLowercase() {
    return 'a' + rand() % ('z' - 'a' + 1);
}

static void rand_str(char *str, int length) {
    char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int firstChar = 1;
    while (length--> 0) {
        int index;
        if (firstChar) {
            index = rand() % N_APHABET + N_APHABET - 1;
            firstChar = 0;
        } else {
            index = rand() % N_APHABET;
        }
        *str++ = charset[index];
    }
    *str = '\0';
}

// Print a string in uppercase
static void upperStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(toupper(str[i]));
        i++;
    }
}
// Print a string in lowercase
static void lowerStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(tolower(str[i]));
        i++;
    }
}
// Print a line with indentation
static void indPrintf(const char *str) {
    printf("    %s", str);
}
// Print a line with second indentation
static void ind2Printf(const char *str) {
    printf("        %s", str);
}
