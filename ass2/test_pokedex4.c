// Assignment 2 19T1 COMP1511: Pokedex
// test_pokedex.c
//
// This program was written by  TUAN HO (z5261243)
// on 27/4/19
//
// Version 1.0.0: Assignment released.
// Version 1.0.1: Added pointer check for the provided tests.

// SKETCH IDEA OF THIS TESTING FILE:
// The idea 

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "pokedex.h"


#define MAX_CHAR_IN_STR         20
#define MAX_POKEMON          32766
#define MIN_STR                  5
#define N_POKEMON_TYPE          18
#define N_APHABET               26

// Sample data on Bulbasaur, the Pokemon with pokemon_id 1.
#define BULBASAUR_ID 1
#define BULBASAUR_NAME "Bulbasaur"
#define BULBASAUR_HEIGHT 0.7
#define BULBASAUR_WEIGHT 6.9
#define BULBASAUR_FIRST_TYPE GRASS_TYPE
#define BULBASAUR_SECOND_TYPE POISON_TYPE

// Sample data on Ivysaur, the Pokemon with pokemon_id 2.
#define IVYSAUR_ID 2
#define IVYSAUR_NAME "Ivysaur"
#define IVYSAUR_HEIGHT 1.0
#define IVYSAUR_WEIGHT 13.0
#define IVYSAUR_FIRST_TYPE GRASS_TYPE
#define IVYSAUR_SECOND_TYPE POISON_TYPE

// Each called function corresponds with a #define number below
// Whenever rand() % the function code == 0, the function is called
// Adjust the numbers for the frequencies of calling the functions
// The lower const number is, the more likely the function occurs
#define GO_EXPLORING                2
#define MOVE_TO_NEXT                3
#define MOVE_PREV                   4
#define CHANGE_CURR_POKEMON         4
#define SET_FOUND                   8
#define ADD_REVOLUTION              5
#define GET_POKEMON_OF_TYPE         5
#define GET_FOUND_POKEMON           5
#define GET_SEARCH_POKEMON          5
#define REMOVE_POKEMON              6


static void green();
static void red();
static void yellow();
static void normalColor();
static void printPassed();
static void printFailed();
static int isAlphabet(int ch);
static int lowerChar(int ch);
static int upperChar(int ch);
static int randLowercase();
static int randUppercase();
static char *randString(int N_chars);
static void rand_str(char *dest, int length);
static void upperStr(const char *str);
static void lowerStr(const char *str);

static void indPrintf(const char *str);
static void ind2Printf(const char *str);

// Tests for Pokedex functions from pokedex.c.
static void test_new_pokedex(void);
static void test_add_pokemon(void);
static void test_get_found_pokemon(void);
static void test_next_pokemon(void);

// Helper functions for creating/comparing Pokemon.
static Pokemon create_bulbasaur(void);
static Pokemon create_ivysaur(void);
static int is_same_pokemon(Pokemon first, Pokemon second);
static int is_copied_pokemon(Pokemon first, Pokemon second);
static void create_pokenodes(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], int N_pokednodes);
static void testing(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], 
                    int pokemonId[MAX_POKEMON],
                    int N_pokednodes);

static void produceTestingCode();

/*
// Adding pokemons commands
a 13770 HfksfuqkcVowad 3 3 rock grass
a 1444 Csyrgt 3 3 flying fire
a 6045 Lvsqgklwgmvoyhx 4 4 Fairy poison
a 2763 Arwauegdkkvacvhief 5 5 flying grass
a 2624 Smyirdnpry 7 8 poison 

*/

//Sample data on HfksfuqkcVowad, the Pokemon with pokemon_id 13770
#define HFKSFUQKcVOWAD_ID 13770 
#define HFKSFUQKcVOWAD_NAME "HfksfuqkcVowad"
#define HFKSFUQKcVOWAD_HEIGHT 8.3
#define HFKSFUQKcVOWAD_WEIGHT 1.0
#define HFKSFUQKcVOWAD_FIRST_TYPE ROCK_TYPE
#define HFKSFUQKcVOWAD_SECOND_TYPE GRASS_TYPE


//Sample data on Csyrgt, the Pokemon with pokemon_id 1444
#define CSYRGT_ID 1444 
#define CSYRGT_NAME "Csyrgt"
#define CSYRGT_HEIGHT 0.3
#define CSYRGT_WEIGHT 3.7
#define CSYRGT_FIRST_TYPE FLYING_TYPE
#define CSYRGT_SECOND_TYPE FIRE_TYPE


//Sample data on Lvsqgklwgmvoyhx, the Pokemon with pokemon_id 6045
#define LVSQGKLWGMVOYHX_ID 6045 
#define LVSQGKLWGMVOYHX_NAME "Lvsqgklwgmvoyhx"
#define LVSQGKLWGMVOYHX_HEIGHT 0.4
#define LVSQGKLWGMVOYHX_WEIGHT 1.0
#define LVSQGKLWGMVOYHX_FIRST_TYPE FAIRY_TYPE
#define LVSQGKLWGMVOYHX_SECOND_TYPE POISON_TYPE


//Sample data on Arwauegdkkvacvhief, the Pokemon with pokemon_id 2763
#define ARWAUEGDKKVACVHIEF_ID 2763 
#define ARWAUEGDKKVACVHIEF_NAME "Arwauegdkkvacvhief"
#define ARWAUEGDKKVACVHIEF_HEIGHT 2.0
#define ARWAUEGDKKVACVHIEF_WEIGHT 0.5
#define ARWAUEGDKKVACVHIEF_FIRST_TYPE STEEL_TYPE
#define ARWAUEGDKKVACVHIEF_SECOND_TYPE FLYING_TYPE


//Sample data on Smyirdnpry, the Pokemon with pokemon_id 2624
#define SMYIRDNPRY_ID 2624 
#define SMYIRDNPRY_NAME "Smyirdnpry"
#define SMYIRDNPRY_HEIGHT 0.2
#define SMYIRDNPRY_WEIGHT 0.1
#define SMYIRDNPRY_FIRST_TYPE GRASS_TYPE
#define SMYIRDNPRY_SECOND_TYPE NORMAL_TYPE

// Prototypes of creating nodes here
static Pokemon create_hfksfuqkcVowad(void);
static Pokemon create_csyrgt(void);
static Pokemon create_lvsqgklwgmvoyhx(void);
static Pokemon create_arwauegdkkvacvhief(void);
static Pokemon create_smyirdnpry(void);

static void mainTesting(void);
// Prototypes of testing functions here
static void test1(void);
static void test2(void);
static void test3(void);
static void test4(void);
static void test5(void);

int main(int argc, char *argv[]) {
    mainTesting();
}

static void mainTesting(void) {
    test1();
    test2();
}



// static void test2() {
//     printf("=====================================\n");
//     yellow();
//     printf("Test2");
//     printf("\n");
//     normalColor();
//     printf("    ... Creating a new Pokedex\n");
//     Pokedex pokedex = new_pokedex();
//     printf("    ... Creating pokenode HfksfuqkcVowad\n");
//     Pokemon hfksfuqkcVowad = create_hfksfuqkcVowad();
//     printf("    ... Creating pokenode Csyrgt\n");
//     Pokemon csyrgt = create_csyrgt();
//     printf("    ... Creating pokenode Lvsqgklwgmvoyhx\n");
//     Pokemon lvsqgklwgmvoyhx = create_lvsqgklwgmvoyhx();
//     printf("    ... Creating pokenode Arwauegdkkvacvhief\n");
//     Pokemon arwauegdkkvacvhief = create_arwauegdkkvacvhief();
//     printf("    ... Creating pokenode Smyirdnpry\n");
//     Pokemon smyirdnpry = create_smyirdnpry();
//     printf("\n");
//     printf("    ... Adding HfksfuqkcVowad to the pokedex\n");
//     add_pokemon(pokedex, hfksfuqkcVowad);
//     printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

//     printf("    ... Adding Csyrgt to the pokedex\n");
//     add_pokemon(pokedex, csyrgt);
//     printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

//     printf("    ... Adding Lvsqgklwgmvoyhx to the pokedex\n");
//     add_pokemon(pokedex, lvsqgklwgmvoyhx);
//     printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

//     printf("    ... Adding Arwauegdkkvacvhief to the pokedex\n");
//     add_pokemon(pokedex, arwauegdkkvacvhief);
//     printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

//     printf("    ... Adding Smyirdnpry to the pokedex\n");
//     add_pokemon(pokedex, smyirdnpry);
//     printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

//     printf("    ... Removing the current pokemon HfksfuqkcVowad from the pokedex\n");
//     remove_pokemon(pokedex);
//     printf("       ==> Checking the total number of Pokemons in the pokedex \n");
//     assert(count_total_pokemon(pokedex) == 4);
//     printf("       ==> Checking if  the corresponding current Pokemon is hfksfuqkcVowad");
//     assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);
    
//     printf("    ... Changing the current node to the pokemon Csyrgt with id 1444 \n");
//     change_current_pokemon(pokedex, 1444);
//     printf("       ==> Checking if the current pokemon is Csyrgt \n");
//     assert(get_current_pokemon(pokedex) == csyrgt);
//     printf("    ... Changing the current node to the pokemon Lvsqgklwgmvoyhx with id 6045 \n");
//     change_current_pokemon(pokedex, 6045);
//     printf("       ==> Checking if the current pokemon is Lvsqgklwgmvoyhx \n");
//     assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);
//     printf("    ... Changing the current node to the pokemon Csyrgt with id 1444 \n");
//     change_current_pokemon(pokedex, 1444);
//     printf("       ==> Checking if the current pokemon is Csyrgt \n");
//     assert(get_current_pokemon(pokedex) == csyrgt);
//     printf("    ... Removing the current pokemon Csyrgt from the pokedex\n");
//     remove_pokemon(pokedex);
//     printf("       ==> Checking the total number of Pokemons in the pokedex \n");
//     assert(count_total_pokemon(pokedex) == 3);
//     printf("       ==> Checking if  the corresponding current Pokemon is csyrgt");
//     assert(get_current_pokemon(pokedex) == csyrgt);
    
//     printf("    ... Changing the current node to the pokemon HfksfuqkcVowad with id 13770 \n");
//     change_current_pokemon(pokedex, 13770);
//     printf("       ==> Checking if the current pokemon is HfksfuqkcVowad \n");
//     assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);
//     printf("    ... Changing the current node to the pokemon Lvsqgklwgmvoyhx with id 6045 \n");
//     change_current_pokemon(pokedex, 6045);
//     printf("       ==> Checking if the current pokemon is Lvsqgklwgmvoyhx \n");
//     assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);
//     printf("    ... Removing the current pokemon Lvsqgklwgmvoyhx from the pokedex\n");
//     remove_pokemon(pokedex);
//     printf("       ==> Checking the total number of Pokemons in the pokedex \n");
//     assert(count_total_pokemon(pokedex) == 2);
//     printf("       ==> Checking if  the corresponding current Pokemon is lvsqgklwgmvoyhx");
//     assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);
    
//     printf("    ... Changing the current node to the pokemon HfksfuqkcVowad with id 13770 \n");
//     change_current_pokemon(pokedex, 13770);
//     printf("       ==> Checking if the current pokemon is HfksfuqkcVowad \n");
//     assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);
//     printf("    ... Removing the current pokemon HfksfuqkcVowad from the pokedex\n");
//     remove_pokemon(pokedex);
//     printf("       ==> Checking the total number of Pokemons in the pokedex \n");
//     assert(count_total_pokemon(pokedex) == 1);
//     printf("       ==> Checking if  the corresponding current Pokemon is hfksfuqkcVowad");
//     assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);
    
//     printf("    ... Changing the current node to the pokemon Arwauegdkkvacvhief with id 2763 \n");
//     change_current_pokemon(pokedex, 2763);
//     printf("       ==> Checking if the current pokemon is Arwauegdkkvacvhief \n");
//     assert(get_current_pokemon(pokedex) == arwauegdkkvacvhief);
//     printf("    ... Removing the current pokemon Arwauegdkkvacvhief from the pokedex\n");
//     remove_pokemon(pokedex);
//     printf("       ==> Checking the total number of Pokemons in the pokedex \n");
//     assert(count_total_pokemon(pokedex) == 0);
//     printf("       ==> Checking if  the corresponding current Pokemon is arwauegdkkvacvhief");
//     assert(get_current_pokemon(pokedex) == arwauegdkkvacvhief);
    
//     printf("    ... Destroying the main Pokedex\n");
//     destroy_pokedex(pokedex);
//     printf("\n");
//     green();
//     printf("Passed Test2");
//     printf("\n");
//     normalColor();
//     printf("\n");

// }

static void test1() {
    printf("=====================================\n");
    yellow();
    printf("Test 1\n\n");
    printf("    Testing new_pokdex function");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Creating pokenode HfksfuqkcVowad\n");
    Pokemon hfksfuqkcVowad = create_hfksfuqkcVowad();
    printf("    ... Creating pokenode Csyrgt\n");
    Pokemon csyrgt = create_csyrgt();
    printf("    ... Creating pokenode Lvsqgklwgmvoyhx\n");
    Pokemon lvsqgklwgmvoyhx = create_lvsqgklwgmvoyhx();
    printf("    ... Creating pokenode Arwauegdkkvacvhief\n");
    Pokemon arwauegdkkvacvhief = create_arwauegdkkvacvhief();
    printf("    ... Creating pokenode Smyirdnpry\n");
    Pokemon smyirdnpry = create_smyirdnpry();
    printf("\n");
    printf("    ... Adding HfksfuqkcVowad to the pokedex\n");
    add_pokemon(pokedex, hfksfuqkcVowad);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Adding Csyrgt to the pokedex\n");
    add_pokemon(pokedex, csyrgt);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Removing the current pokemon HfksfuqkcVowad from the pokedex\n");
    remove_pokemon(pokedex);
    printf("       ==> Checking the total number of Pokemons in the pokedex \n");
    assert(count_total_pokemon(pokedex) == 1);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == csyrgt);

    printf("    ... Adding Lvsqgklwgmvoyhx to the pokedex\n");
    add_pokemon(pokedex, lvsqgklwgmvoyhx);
    printf("    ... Adding Arwauegdkkvacvhief to the pokedex\n");
    add_pokemon(pokedex, arwauegdkkvacvhief);
    printf("    ... Adding Smyirdnpry to the pokedex\n");
    add_pokemon(pokedex, smyirdnpry);

    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == csyrgt);
    printf("    ... Changing the current node to the pokemon Arwauegdkkvacvhief with id 2763 \n");
    change_current_pokemon(pokedex, 2763);
    printf("       ==> Checking if the current pokemon is Arwauegdkkvacvhief \n");
    assert(get_current_pokemon(pokedex) == arwauegdkkvacvhief);
    printf("    ... Removing the current pokemon Arwauegdkkvacvhief from the pokedex\n");
    remove_pokemon(pokedex);
    printf("       ==> Checking the total number of Pokemons in the pokedex \n");
    assert(count_total_pokemon(pokedex) == 3);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == smyirdnpry);

    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 1);

    yellow();
    printf("    Testing function prev_pokemon, next_pokemon, change_current_pokemon\n");
    normalColor();
    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);

    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 2);

    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == csyrgt);

    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 3);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);
    

    yellow();
    printf("    Testing function destroy_pokedex\n");
    normalColor();
    printf("    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);
    printf("\n");
    green();
    printf("Passed Test 1");
    printf("\n");
    normalColor();
    printf("\n");

}
static void test2(void) {
    printf("=====================================\n");
    yellow();
    printf("Test 2\n");
    printf("    Testing new_pokdex function");
    printf("\n");
    normalColor();
    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();
    printf("    ... Checking the pokedex is not NULL\n");
    assert(pokedex != NULL);

    yellow();
    printf("    Testing function add_pokemon, get_current_pokemon\n");
    normalColor();

    printf("    ... Creating pokenode HfksfuqkcVowad\n");
    Pokemon hfksfuqkcVowad = create_hfksfuqkcVowad();
    printf("    ... Creating pokenode Csyrgt\n");
    Pokemon csyrgt = create_csyrgt();
    printf("    ... Creating pokenode Lvsqgklwgmvoyhx\n");
    Pokemon lvsqgklwgmvoyhx = create_lvsqgklwgmvoyhx();
    printf("    ... Creating pokenode Arwauegdkkvacvhief\n");
    Pokemon arwauegdkkvacvhief = create_arwauegdkkvacvhief();
    printf("    ... Creating pokenode Smyirdnpry\n");
    Pokemon smyirdnpry = create_smyirdnpry();
    printf("    ... Adding HfksfuqkcVowad to the pokedex\n");
    add_pokemon(pokedex, hfksfuqkcVowad);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Adding Csyrgt to the pokedex\n");
    add_pokemon(pokedex, csyrgt);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Adding Lvsqgklwgmvoyhx to the pokedex\n");
    add_pokemon(pokedex, lvsqgklwgmvoyhx);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Adding Arwauegdkkvacvhief to the pokedex\n");
    add_pokemon(pokedex, arwauegdkkvacvhief);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);

    printf("    ... Adding Smyirdnpry to the pokedex\n");
    add_pokemon(pokedex, smyirdnpry);
    printf("       ==> Checking that the current Pokemon is HfksfuqkcVowad\n");    
    assert(get_current_pokemon(pokedex) == hfksfuqkcVowad);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type with empty pokedex\n");
    normalColor();
    printf("    ... Getting pokemon type water\n");
    Pokedex waterPokemons = get_pokemon_of_type(pokedex, 4);
    printf("        ==> Checking if the new pokedex is empty\n");
    assert(count_total_pokemon(waterPokemons) == 0);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(waterPokemons);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type again with empty pokedex\n");
    normalColor();
    printf("    ... Getting pokemon type poison\n");
    Pokedex poisonPokemons = get_pokemon_of_type(pokedex, 7);
    printf("        ==> Checking if the new pokedex is empty\n");
    assert(count_total_pokemon(poisonPokemons) == 0);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(poisonPokemons);
    printf("\n");
    yellow();
    printf("    Testing function find_current_pokemon, next_pokemon\n");
    normalColor();
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 1);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == csyrgt);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Check the number of pokemons found\n");
    assert(count_found_pokemon(pokedex) == 2);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == arwauegdkkvacvhief);

    printf("    ... Move the current pokemon to the next one\n");
    next_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == smyirdnpry);

    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("    ... Move the current pokemon to the previous one\n");
    prev_pokemon(pokedex);
    printf("    ... Set the current pokemon to be found\n");
    find_current_pokemon(pokedex);
    printf("       ==> Checking the current Pokemon \n");
    assert(get_current_pokemon(pokedex) == lvsqgklwgmvoyhx);

    yellow();
    printf("    Testing function get_pokemon_of_type again with one pokemon\n");
    normalColor();
    printf("    ... Getting pokemon type poison\n");
    Pokedex poisonPokemons2 = get_pokemon_of_type(pokedex, 7);
    printf("        ==> Checking if the new pokedex is empty\n");
    assert(count_total_pokemon(poisonPokemons2) == 1);
    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(poisonPokemons2);
    printf("\n");

    yellow();
    printf("    Testing function get_pokemon_of_type again with many pokemons\n");
    normalColor();
    printf("    ... Getting pokemon type flying\n");
    Pokedex flying = get_pokemon_of_type(pokedex, 5);
    printf("        ==> Checking the total number of pokemons in the new pokedex\n");
    assert(count_total_pokemon(flying) == 2);
    printf("        ==> Checking the current pokemon of the new pokedex is not the same\n");
    assert(get_current_pokemon(flying) != csyrgt);
    printf("        ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(flying),  csyrgt));

    printf("        ... Get pokemon that have \" cV\" in its name from the new pokedex\n ");
    Pokedex cVPokemons = search_pokemon(flying, "cV");
    printf("            ==> Checking the total number of pokemons in the new pokedex\n");
    assert(count_total_pokemon(cVPokemons) == 1);
    printf("            ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(cVPokemons),  arwauegdkkvacvhief));

    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(cVPokemons);

    yellow();
    printf("    Testing function get_found_pokemon + find_current_pokemon + next_pokemon\n");

    normalColor();
    printf("    ... Get found pokemons\n");
    Pokedex foundPokemons = get_found_pokemon(pokedex);
    printf("        ==> Checking the total number of pokemons in the new pokedex\n");
    //assert(count_total_pokemon(foundPokemons) == 5);
    printf("        ==> Checking if it is a copy of the original node head\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons),  csyrgt));

    yellow();
    printf("        Also, testing get_found_pokemon pokedex has correct order of id\n");
    normalColor();
    printf("        ==> Check the new pokedex created have ascending order of id\n");
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), smyirdnpry));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), arwauegdkkvacvhief));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), lvsqgklwgmvoyhx));
    printf("            ... Move the current pokemon to the next one\n");
    next_pokemon(foundPokemons);
    printf("                ==> Checking the current pokemon\n");
    assert(is_copied_pokemon(get_current_pokemon(foundPokemons), hfksfuqkcVowad));
    green();
    printf("            Hooray! The found pokedex has the ids in correct order!\n");
    normalColor();
    printf("        ... Destroy the lately created pokedex\n");
    destroy_pokedex(foundPokemons);

    printf("    ... Destroy the lately created pokedex\n");
    destroy_pokedex(flying);

    printf("    ... Destroying the main Pokedex\n");
    destroy_pokedex(pokedex);
    printf("\n");
    green();
    printf("Passed Test2");
    printf("\n");
    normalColor();
    printf("\n");

}


// Helper function to create HfksfuqkcVowad for testing purposes.
static Pokemon create_hfksfuqkcVowad(void) {
    Pokemon pokemon = new_pokemon(
            HFKSFUQKcVOWAD_ID, 
            HFKSFUQKcVOWAD_NAME,
            HFKSFUQKcVOWAD_HEIGHT, 
            HFKSFUQKcVOWAD_WEIGHT,
            HFKSFUQKcVOWAD_FIRST_TYPE,
            HFKSFUQKcVOWAD_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Csyrgt for testing purposes.
static Pokemon create_csyrgt(void) {
    Pokemon pokemon = new_pokemon(
            CSYRGT_ID, 
            CSYRGT_NAME,
            CSYRGT_HEIGHT, 
            CSYRGT_WEIGHT,
            CSYRGT_FIRST_TYPE,
            CSYRGT_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Lvsqgklwgmvoyhx for testing purposes.
static Pokemon create_lvsqgklwgmvoyhx(void) {
    Pokemon pokemon = new_pokemon(
            LVSQGKLWGMVOYHX_ID, 
            LVSQGKLWGMVOYHX_NAME,
            LVSQGKLWGMVOYHX_HEIGHT, 
            LVSQGKLWGMVOYHX_WEIGHT,
            LVSQGKLWGMVOYHX_FIRST_TYPE,
            LVSQGKLWGMVOYHX_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Arwauegdkkvacvhief for testing purposes.
static Pokemon create_arwauegdkkvacvhief(void) {
    Pokemon pokemon = new_pokemon(
            ARWAUEGDKKVACVHIEF_ID, 
            ARWAUEGDKKVACVHIEF_NAME,
            ARWAUEGDKKVACVHIEF_HEIGHT, 
            ARWAUEGDKKVACVHIEF_WEIGHT,
            ARWAUEGDKKVACVHIEF_FIRST_TYPE,
            ARWAUEGDKKVACVHIEF_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Smyirdnpry for testing purposes.
static Pokemon create_smyirdnpry(void) {
    Pokemon pokemon = new_pokemon(
            SMYIRDNPRY_ID, 
            SMYIRDNPRY_NAME,
            SMYIRDNPRY_HEIGHT, 
            SMYIRDNPRY_WEIGHT,
            SMYIRDNPRY_FIRST_TYPE,
            SMYIRDNPRY_SECOND_TYPE
    );
    return pokemon;
}


////////////////////////////////////////////////////////////////////////
//                     Pokedex Test Functions                         //
////////////////////////////////////////////////////////////////////////


//=======================================================================
// Some of the ways that you could extend these tests would include:
//   = calling the get_found_pokemon function on an empty Pokedex,
//   = calling the get_found_pokemon function on a Pokedex where none of
//     the Pokemon have been found,
//   = checking that the Pokemon in the new Pokedex are in ascending
//     order of pokemon_id (regardless of the order that they appeared
//     in the original Pokedex),
//   = checking that the currently selected Pokemon in the returned
//     Pokedex has been set correctly,
//   = checking that the original Pokedex has not been modified,
//   = ... and more!
//=======================================================================




////////////////////////////////////////////////////////////////////////
//                     Helper Functions                               //
////////////////////////////////////////////////////////////////////////


// Helper function to compare whether two Pokemon are the same.
// This checks that the two pointers contain the same address, i.e.
// they are both pointing to the same pokemon struct in memory.
//
// Pokemon ivysaur = new_pokemon(0, 'ivysaur', 1.0, 13.0, GRASS_TYPE, POISON_TYPE)
// Pokemon also_ivysaur = ivysaur
// is_same_pokemon(ivysaur, also_ivysaur) == TRUE
static int is_same_pokemon(Pokemon first, Pokemon second) {
    return first == second;
}

// Helper function to compare whether one Pokemon is a *copy* of
// another, based on whether their attributes match (e.g. pokemon_id,
// height, weight, etc).
// 
// It also checks that the pointers do *not* match == i.e. that the
// pointers aren't both pointing to the same pokemon struct in memory.
// If the pointers both contain the same address, then the second
// Pokemon is not a *copy* of the first Pokemon.
// 
// This function doesn't (yet) check that the Pokemon's names match
// (but perhaps you could add that check yourself...).
static int is_copied_pokemon(Pokemon first, Pokemon second) {
    return (pokemon_id(first) == pokemon_id(second))
    &&  (first != second)
    &&  (pokemon_height(first) == pokemon_height(second))
    &&  (pokemon_weight(first) == pokemon_weight(second))
    &&  (pokemon_first_type(first) == pokemon_first_type(second))
    &&  (pokemon_second_type(first) == pokemon_second_type(second));
}
// ==============   HELPER FUNCTIONS    ================== //

// Print passed sign in color
static void printPassed() {
    green();
    printf("passed\n");
    normalColor();
}

// Print failed sign in color
static void printFailed() {
    red();
    printf("failed\n");
    normalColor();
}

// Change output color to green
static void green() {
    printf("\033[1;32m");
}
// Change output color to red
static void red() {
    printf("\033[1;31m");
}
// Change output color to yellow
static void yellow() {
    printf("\033[1;33m");
}
// Change output color to normal
static void normalColor() {
    printf("\033[0m");
}
// Return if a character is alphabetical
static int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        || (ch >= 'A' && ch <= 'Z')) {
        return 1;
    } else {
        return 0;
    }
}
// Convert a character to lowercase
static int lowerChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'A' && ch <= 'Z') {
            newCh = ch + 32;
        }
    } 
    return newCh;
}
// Convert a character to uppercase
static int upperChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'a' && ch <= 'z') {
            newCh = ch = 32;
        }
    } 
    return newCh;
}
// Return a random Uppercase characters
static int randUppercase() {
    return 'A' + rand() % ('Z'-'A' + 1);
}
// Return a random lowercase characters
static int randLowercase() {
    return 'a' + rand() % ('z' - 'a' + 1);
}
// Create a radom String with a specific number or characters
/*static char *randString(int N_chars) {
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *strincV[MAX_CHAR_IN_STR];
    strincV[0] = "";
    char str[MAX_CHAR_IN_STR + 1];
    str[0] = (char) randUppercase();
    strincV[0] = (const char) randUppercase();
    char randChar = (char) randUppercase();
    memcpy(strincV, randChar, 1);
    //strcpy(strincV, (const char*) randUppercase());
    //strincV[0] = &charset[25 + rand() % 26];
    strcpy(strincV, charset[25 + rand() % 26]);
    int i = 1;
    while (i < N_chars) {
        randChar = (char) randLowercase();
        memcpy(strincV, randChar, 1);
        //strcpy(strincV, (const char*) randLowercase());
        //str[i] = (char) randLowercase();
        strincV[i] = charset[rand() % 26];
        i++;
    }
    strincV[i] = '\0';
    return strincV;
}*/
/*static char *rand_string(char *str, int size) {
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (size) {
        --size;
        for (int  n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}*/
static void rand_str(char *str, int length) {
    char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int firstChar = 1;
    while (length-- > 0) {
        int index;
        if (firstChar) {
            index = rand() % N_APHABET + N_APHABET - 1;
            firstChar = 0;
        } else {
            index = rand() % N_APHABET;
        }
        *str++ = charset[index];
    }
    *str = '\0';
}

// Print a string in uppercase
static void upperStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(toupper(str[i]));
        i++;
    }
}
// Print a string in lowercase
static void lowerStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(tolower(str[i]));
        i++;
    }
}
// Print a line with indentation
static void indPrintf(const char *str) {
    printf("    %s", str);
}
// Print a line with second indentation
static void ind2Printf(const char *str) {
    printf("        %s", str);
}
///////////////////////////////////////////////////////////////
//               PRODUCE TESTING CODE FUNCTION               //
///////////////////////////////////////////////////////////////

// Produce testing code for this file
// Copy the code below the line '========' into this file
static void produceTestingCode() {
    printf("\n\n==========Produce C hard-code of Pokedex Tests =======================\n");
    int N_tests, N_pokednodes;
    printf("How many tests that you want to hard-C-code? ");
    scanf("%d", &N_tests);
    printf("How many pokemons that you want to have in the test?"
           "\n(real max is 32766 but recommended max 300 pokemons for good randomnization) \n");
    scanf("%d", &N_pokednodes);
    red();
    printf("FYI: ");
    normalColor();
    printf("For each test, the program will try to call and test all functions in pokedex.c \n");
    printf("The more pokemons and tests you have, the more thorought the test is\n\n");
    yellow();
    printf("Start copying the code below to test_pokdex.c \n");
    normalColor();
    //red();
    //printf("Warning: ");
    //normalColor();
    //printf("Be careful with sign '[MAIN FUNCTION START HERE]' in between may occur. \n");
    //printf("This is where we need to break our code put this piece inside main function\n\n");
    printf("=================================================================\n\n\n");

    int i, j;
    int pokemonId[MAX_POKEMON] = {0};
    char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR];
    double pokemonWeight[MAX_POKEMON] = {0};
    double pokemonHeight[MAX_POKEMON] = {0};
    int pokemonFirstType[MAX_POKEMON];
    int pokemonSecType[MAX_POKEMON];

    // Generate #define code
    for (j = 0; j < N_pokednodes; j++) {
        //pokemonName[j] = randString(rand() % MAX_CHAR_IN_STR + 1);
        //pokemonName[j] = randString(rand() % (MAX_CHAR_IN_STR - 1) + 1);
        srand(rand());
        rand_str(pokemonName[j], rand() % (MAX_CHAR_IN_STR - 5) + 5);
        pokemonId[j] = rand() % MAX_POKEMON;
        pokemonHeight[j] = (double) rand() / (double) rand();
        pokemonWeight[j] = (double) rand() / (double) rand();
        pokemonFirstType[j] = 1 + rand() % N_POKEMON_TYPE;
        pokemonSecType[j] = rand() % (N_POKEMON_TYPE - 1);
        printf("\n//Sample data on %s, the Pokemon with pokemon_id %d\n", pokemonName[j], pokemonId[j]);
        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_ID %d \n", pokemonId[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_NAME \"%s\"\n", pokemonName[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_HEIGHT %.1lf\n", pokemonHeight[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_WEIGHT %.1lf\n", pokemonWeight[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_FIRST_TYPE ");
        upperStr(pokemon_type_to_string(pokemonFirstType[j]));
        printf("_TYPE\n");

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_SECOND_TYPE ");
        upperStr(pokemon_type_to_string(pokemonSecType[j]));
        printf("_TYPE\n");

        printf("\n");
    }
    // Generate the code of prototyping function here
    printf("// Prototypes of creating nodes here\n");
    for (j = 0; j < N_pokednodes; j++) {
        printf("static Pokemon create_");
        lowerStr(pokemonName[j]);
        printf("(void);\n");
    }
    printf("\n");
    // Prototypes of collective  testing functions here
    printf("static void testing%d(void);\n", N_tests);

    // Prototypes of each test functions
    printf("// Prototypes of testing functions here\n");
    for (i = 1; i <= N_tests; i++) {
        printf("static void test%d(void);\n", i);
    }
    printf("\n");
    // Main function is here
    /*red();
    printf("\n[MAIN FUNCTION STARTS HERE]\n");
    normalColor();*/
    printf("int main(int argc, char *argv[]) {\n");
    indPrintf("testing");
    printf("%d();\n", N_tests);
    printf("}\n\n");
    /*red();
    printf("\n[MAIN FUNCTION ENDS HERE]\n");
    normalColor();*/
    // Collective of testing functions here
    printf("static void testing%d() {\n", N_tests);
    for (i = 1; i <= N_tests; i++) {
        indPrintf("test");
        printf("%d();\n", i);
    }
    printf("}\n\n");
    // Generate the testing functions code
    for (i = 1; i <= N_tests; i++) {
        printf("static void test%d() {\n", i);
        indPrintf("printf(\"=====================================\\n\");\n");
        indPrintf("yellow();\n");
        indPrintf("printf(\"Test");
        printf("%d\");\n", i); 
        indPrintf("printf(\"\\n\");\n");
        indPrintf("normalColor();\n");
        testing(pokemonName, pokemonId, N_pokednodes);
        indPrintf("green();\n");
        indPrintf("printf(\"Passed Test");
        printf("%d\");\n", i); 
        indPrintf("printf(\"\\n\");\n");
        indPrintf("normalColor();\n");
        indPrintf("printf(\"\\n\");\n");
        printf("\n}\n");
        printf("\n");
    }


    // Generate the helper function code
    
    // Generating testing function here

    // Generate creat_pokenode code here
    create_pokenodes(pokemonName, N_pokednodes);
}
static void create_pokenodes(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], int N_pokednodes) {
    int j;
    for (j = 0; j < N_pokednodes; j++) {
        printf("\n");
        char *name = pokemonName[j];
        printf("// Helper function to create %s for testing purposes.\n", name);
        printf("static Pokemon create_");
        lowerStr(name);
        printf("(void) {\n");
        printf("    Pokemon pokemon = new_pokemon(\n");
        printf("            ");
        upperStr(name);
        printf("_ID, \n");
        printf("            ");
        upperStr(name);
        printf("_NAME,\n");
        printf("            ");
        upperStr(name);
        printf("_HEIGHT, \n");
        printf("            ");
        upperStr(name);
        printf("_WEIGHT,\n");
        printf("            ");
        upperStr(name);
        printf("_FIRST_TYPE,\n");
        printf("            ");
        upperStr(name);
        printf("_SECOND_TYPE\n");
        printf("    );\n");
        printf("    return pokemon;\n");
        printf("}\n");
    }
}

static void testing(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], 
                    int pokemonId[MAX_POKEMON],
                    int N_pokednodes) {
    indPrintf("printf(\"    ... Creating a new Pokedex\\n\");\n");
    indPrintf("Pokedex pokedex = new_pokedex();\n");
    /*indPrintf("printf(\"    ... Checking initialized fields of the pokedex\\n\");\n");
    indPrintf("assert(pokedex->head == NULL);\n");
    indPrintf("assert(pokedex->last == NULL);\n");
    indPrintf("assert(pokedex->current == NULL);\n\n");*/
    int j;
    int N_nodes_in_pokedex = 0;
    // Creating the nodes
    for (j = 0; j < N_pokednodes; j++) {
        indPrintf("printf(\"    ... Creating pokenode ");
        printf("%s\\n\");\n", pokemonName[j]);
        indPrintf("Pokemon ");
        lowerStr(pokemonName[j]);
        printf(" = create_");
        lowerStr(pokemonName[j]);
        printf("();\n");
    }
    indPrintf("printf(\"\\n\");\n");

    // Add the nodes to the pokedex and check the current selected node 
    // of the pokedex
    int found_count = 0;
    int removed[MAX_POKEMON] = {0};
    int found[MAX_POKEMON] = {0};
    int currentPokemon = 0;
    for (j = 0; j < N_pokednodes; j++) {
        indPrintf("printf(\"    ... Adding ");
        printf("%s ", pokemonName[j]);
        printf("to the pokedex\\n\");\n");
        indPrintf("add_pokemon(pokedex, ");
        lowerStr(pokemonName[j]);
        printf(");\n");
        indPrintf("printf(\"       ==> Checking that the current Pokemon is ");
        printf("%s", pokemonName[currentPokemon]);
        printf("\\n\");");
        indPrintf("assert(get_current_pokemon(pokedex) == ");
        lowerStr(pokemonName[currentPokemon]);
        printf(");\n");
        printf("\n");
        N_nodes_in_pokedex++;

        // Randomly set this pokemon is found
        if (rand() % SET_FOUND == 0) {
            found_count++;
            found[j] = 1;
            indPrintf("printf(\"    ... Setting ");
            printf("%s ", pokemonName[j]);
            printf("to be found\\n\");\n");
            indPrintf("find_current_pokemon(pokedex);\n");
            indPrintf("printf(\"       ==> Checking the correct number of Pokemons found \\n\");\n");
            indPrintf("assert(count_found_pokemon(pokedex) == ");
            printf("%d);\n", found_count);
            printf("\n");
        }
        

        // Randomly remove the pokemon
        if (rand() % REMOVE_POKEMON == 0) {
            N_nodes_in_pokedex--;
            removed[j] = 1;
            if (found[j] == 1) {
                found[j] = 0;
                found_count--;
            }
            indPrintf("printf(\"    ... Removing the current pokemon ");
            printf("%s from the pokedex\\n\");\n", pokemonName[currentPokemon]);
            indPrintf("remove_pokemon(pokedex);\n");

            indPrintf("printf(\"       ==> Checking the total number of Pokemons in the pokedex \\n\");\n");
            indPrintf("assert(count_total_pokemon(pokedex) == ");
            printf("%d);\n", N_nodes_in_pokedex);
            indPrintf("printf(\"       ==> Checking the corresponding current Pokemon is \\n\");\n");
            indPrintf("assert(get_current_pokemon(pokedex) == ");
            int tempIndex2 = currentPokemon;
            while (removed[tempIndex2]) {
                tempIndex2++;
            }
            if (tempIndex2 >= N_pokednodes) {
                tempIndex2 = currentPokemon;
                while (removed[tempIndex2]) {
                    tempIndex2--;
                }
            }
            if (tempIndex2 == -1) {
                printf("NULL");
                currentPokemon = 0;
            } else {
                currentPokemon = tempIndex2;
                lowerStr(pokemonName[currentPokemon]);
            }
            printf(");\n");
            printf("\n");
        }
        
    }
    j--;
    // Randomly execute function untile the last node is removed
    while (N_nodes_in_pokedex > 0) {
        // Randomly change the current selected pokenode
        if (rand() % CHANGE_CURR_POKEMON == 0) {
            int tempIndex = rand() % N_pokednodes;
            int randomId = pokemonId[tempIndex];
            while (removed[tempIndex] || pokemonId[tempIndex] == pokemonId[currentPokemon]) {
                tempIndex = rand() % N_pokednodes;
                randomId = pokemonId[tempIndex];
            }
            currentPokemon = tempIndex;
            indPrintf("printf(\"    ... Changing the current node to the pokemon");
            printf(" %s with id %d \\n\");\n", pokemonName[tempIndex], pokemonId[tempIndex]);
            indPrintf("change_current_pokemon(pokedex, ");
            printf("%d);\n", randomId);
            indPrintf("printf(\"       ==> Checking if the current pokemon is ");
            printf("%s \\n\");\n", pokemonName[currentPokemon]);
            indPrintf("assert(get_current_pokemon(pokedex) == ");
            lowerStr(pokemonName[currentPokemon]);
            printf(");\n");
        }
        // Randomly remove the pokemon
        if (rand() % REMOVE_POKEMON == 0) {
            N_nodes_in_pokedex--;
            removed[j] = 1;
            if (found[j] == 1) {
                found[j] = 0;
                found_count--;
            }
            indPrintf("printf(\"    ... Removing the current pokemon ");
            printf("%s from the pokedex\\n\");\n", pokemonName[currentPokemon]);
            indPrintf("remove_pokemon(pokedex);\n");

            indPrintf("printf(\"       ==> Checking the total number of Pokemons in the pokedex \\n\");\n");
            indPrintf("assert(count_total_pokemon(pokedex) ==");
            printf(" %d);\n", N_nodes_in_pokedex);
            indPrintf("printf(\"       ==> Checking if  the corresponding current Pokemon is "); 
            int tempIndex2 = currentPokemon;
            while (removed[tempIndex2]) {
                tempIndex2++;
            }
            if (tempIndex2 >= N_pokednodes) {
                tempIndex2 = currentPokemon;
                while (removed[tempIndex2]) {
                    tempIndex2--;
                }
            }
            if (tempIndex2 == -1) {
                printf("NULL");
                currentPokemon = 0;
            } else {
                currentPokemon = tempIndex2;
                lowerStr(pokemonName[currentPokemon]);
            }
            printf("\");\n");
            indPrintf("assert(get_current_pokemon(pokedex) == ");
            if (tempIndex2 == -1) {
                printf("NULL");
                currentPokemon = 0;
            } else {
                currentPokemon = tempIndex2;
                lowerStr(pokemonName[currentPokemon]);
            }
            printf(");\n");
            indPrintf("\n");
        }
    }

    indPrintf("printf(\"    ... Destroying the main Pokedex\\n\");\n");
    indPrintf("destroy_pokedex(pokedex);\n");
    indPrintf("printf(\"\\n\");\n");
    

    /*printf(\"    ... Getting all found Pokemon\\n\");
    Pokedex found_pokedex = get_found_pokemon(pokedex);

    printf(\"       ==> Checking the correct Pokemon were copied and returned\\n\");
    assert(count_total_pokemon(found_pokedex) == 1);
    assert(count_found_pokemon(found_pokedex) == 1);
    assert(is_copied_pokemon(get_current_pokemon(found_pokedex), bulbasaur));

    printf(\"    ... Destroying both Pokedexes\\n\");
    destroy_pokedex(pokedex);
    destroy_pokedex(found_pokedex);

    green();
    printf(\">> Passed get_found_pokemon tests!\\n\");
    normalColor();*/
}

