// Assignment 2 19T1 COMP1511: Pokedex
// test_pokedex.c
//
// This program was written by  TUAN HO (z5261243)
// on 27/4/19
//
// Version 1.0.0: Assignment released.
// Version 1.0.1: Added pointer check for the provided tests.

// SKETCH IDEA OF THIS TESTING FILE:
// The idea 

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "pokedex.h"


#define MAX_CHAR_IN_STR         20
#define MAX_POKEMON          32766
#define MIN_STR                  5
#define N_POKEMON_TYPE          18
#define N_APHABET               26

// Sample data on Bulbasaur, the Pokemon with pokemon_id 1.
#define BULBASAUR_ID 1
#define BULBASAUR_NAME "Bulbasaur"
#define BULBASAUR_HEIGHT 0.7
#define BULBASAUR_WEIGHT 6.9
#define BULBASAUR_FIRST_TYPE GRASS_TYPE
#define BULBASAUR_SECOND_TYPE POISON_TYPE

// Sample data on Ivysaur, the Pokemon with pokemon_id 2.
#define IVYSAUR_ID 2
#define IVYSAUR_NAME "Ivysaur"
#define IVYSAUR_HEIGHT 1.0
#define IVYSAUR_WEIGHT 13.0
#define IVYSAUR_FIRST_TYPE GRASS_TYPE
#define IVYSAUR_SECOND_TYPE POISON_TYPE

//Sample data on Vlrbbmqbhcdarzowkk, the Pokemon with pokemon_id 24256
#define VLRBBMQBHCDARZOWKK_ID 24256 
#define VLRBBMQBHCDARZOWKK_NAME "Vlrbbmqbhcdarzowkk"
#define VLRBBMQBHCDARZOWKK_HEIGHT 0.1
#define VLRBBMQBHCDARZOWKK_WEIGHT 0.2
#define VLRBBMQBHCDARZOWKK_FIRST_TYPE GROUND_TYPE
#define VLRBBMQBHCDARZOWKK_SECOND_TYPE FLYING_TYPE


//Sample data on Cxrjmow, the Pokemon with pokemon_id 5759
#define CXRJMOW_ID 5759 
#define CXRJMOW_NAME "Cxrjmow"
#define CXRJMOW_HEIGHT 1.1
#define CXRJMOW_WEIGHT 3.3
#define CXRJMOW_FIRST_TYPE STEEL_TYPE
#define CXRJMOW_SECOND_TYPE FIGHTING_TYPE


//Sample data on Cbefsarc, the Pokemon with pokemon_id 24095
#define CBEFSARC_ID 24095 
#define CBEFSARC_NAME "Cbefsarc"
#define CBEFSARC_HEIGHT 0.4
#define CBEFSARC_WEIGHT 0.7
#define CBEFSARC_FIRST_TYPE PSYCHIC_TYPE
#define CBEFSARC_SECOND_TYPE NONE_TYPE


//Sample data on Fxxpklorel, the Pokemon with pokemon_id 18607
#define FXXPKLOREL_ID 18607 
#define FXXPKLOREL_NAME "Fxxpklorel"
#define FXXPKLOREL_HEIGHT 1.3
#define FXXPKLOREL_WEIGHT 0.1
#define FXXPKLOREL_FIRST_TYPE PSYCHIC_TYPE
#define FXXPKLOREL_SECOND_TYPE FIGHTING_TYPE


//Sample data on Vkhopkmcoqhnwnkue, the Pokemon with pokemon_id 21186
#define VKHOPKMCOQHNWNKUE_ID 21186 
#define VKHOPKMCOQHNWNKUE_NAME "Vkhopkmcoqhnwnkue"
#define VKHOPKMCOQHNWNKUE_HEIGHT 0.5
#define VKHOPKMCOQHNWNKUE_WEIGHT 0.5
#define VKHOPKMCOQHNWNKUE_FIRST_TYPE BUG_TYPE
#define VKHOPKMCOQHNWNKUE_SECOND_TYPE DRAGON_TYPE
// Each called function corresponds with a #define number below
// Whenever rand() % the function code == 0, the function is called
#define SET_FOUND                   4
#define CHANGE_CURR_POKEMON         6
#define REMOVE_POKEMON              7


// Add your own prototypes here.
static void green();
static void red();
static void yellow();
static void normalColor();
static void printPassed();
static void printFailed();
static int isAlphabet(int ch);
static int lowerChar(int ch);
static int upperChar(int ch);
static int randLowercase();
static int randUppercase();
static char *randString(int N_chars);
static void rand_str(char *dest, int length);
static void upperStr(const char *str);
static void lowerStr(const char *str);

// Tests for Pokedex functions from pokedex.c.
static void test_new_pokedex(void);
static void test_add_pokemon(void);
static void test_get_found_pokemon(void);
static void test_next_pokemon(void);

// Helper functions for creating/comparing Pokemon.
static Pokemon create_bulbasaur(void);
static Pokemon create_ivysaur(void);
static int is_same_pokemon(Pokemon first, Pokemon second);
static int is_copied_pokemon(Pokemon first, Pokemon second);
static void create_pokenodes(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], int N_pokednodes);
static void testing(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], 
                    int pokemonId[MAX_POKEMON],
                    int N_pokednodes);
// Helper function to create Vlrbbmqbhcdarzowkk for testing purposes.
static Pokemon create_vlrbbmqbhcdarzowkk(void) {
    Pokemon pokemon = new_pokemon(
            VLRBBMQBHCDARZOWKK_ID, 
            VLRBBMQBHCDARZOWKK_NAME,
            VLRBBMQBHCDARZOWKK_HEIGHT, 
            VLRBBMQBHCDARZOWKK_WEIGHT,
            VLRBBMQBHCDARZOWKK_FIRST_TYPE,
            VLRBBMQBHCDARZOWKK_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Cxrjmow for testing purposes.
static Pokemon create_cxrjmow(void) {
    Pokemon pokemon = new_pokemon(
            CXRJMOW_ID, 
            CXRJMOW_NAME,
            CXRJMOW_HEIGHT, 
            CXRJMOW_WEIGHT,
            CXRJMOW_FIRST_TYPE,
            CXRJMOW_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Cbefsarc for testing purposes.
static Pokemon create_cbefsarc(void) {
    Pokemon pokemon = new_pokemon(
            CBEFSARC_ID, 
            CBEFSARC_NAME,
            CBEFSARC_HEIGHT, 
            CBEFSARC_WEIGHT,
            CBEFSARC_FIRST_TYPE,
            CBEFSARC_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Fxxpklorel for testing purposes.
static Pokemon create_fxxpklorel(void) {
    Pokemon pokemon = new_pokemon(
            FXXPKLOREL_ID, 
            FXXPKLOREL_NAME,
            FXXPKLOREL_HEIGHT, 
            FXXPKLOREL_WEIGHT,
            FXXPKLOREL_FIRST_TYPE,
            FXXPKLOREL_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Vkhopkmcoqhnwnkue for testing purposes.
static Pokemon create_vkhopkmcoqhnwnkue(void) {
    Pokemon pokemon = new_pokemon(
            VKHOPKMCOQHNWNKUE_ID, 
            VKHOPKMCOQHNWNKUE_NAME,
            VKHOPKMCOQHNWNKUE_HEIGHT, 
            VKHOPKMCOQHNWNKUE_WEIGHT,
            VKHOPKMCOQHNWNKUE_FIRST_TYPE,
            VKHOPKMCOQHNWNKUE_SECOND_TYPE
    );
    return pokemon;
}

static void produceTestingCode();

static int main(int argc, char *argv[]) {
    printf("Welcome to the COMP1511 Pokedex Tests!\n");

    printf("\n==================== Pokedex Tests ====================\n");

yellow();
printf("Test 1\n");
normalColor();
printf("    ... Creating a new Pokedex\n");
Pokedex pokedex = new_pokedex();
printf("    ... Checking initialized fields of the pokedex\n");
assert(pokedex->head == NULL);
assert(pokedex->last == NULL);
assert(pokedex->current == NULL);

printf("    ... Creating pokenodeVlrbbmqbhcdarzowkk");
Pokemon vlrbbmqbhcdarzowkk = create_vlrbbmqbhcdarzowkk();
printf("    ... Creating pokenodeCxrjmow");
Pokemon cxrjmow = create_cxrjmow();
printf("    ... Creating pokenodeCbefsarc");
Pokemon cbefsarc = create_cbefsarc();
printf("    ... Creating pokenodeFxxpklorel");
Pokemon fxxpklorel = create_fxxpklorel();
printf("    ... Creating pokenodeVkhopkmcoqhnwnkue");
Pokemon vkhopkmcoqhnwnkue = create_vkhopkmcoqhnwnkue();
printf("\n");
printf("    ... Adding Vlrbbmqbhcdarzowkk to the pokedex");
add_pokemon(pokedex, vlrbbmqbhcdarzowkk);
printf("       ==> Checking that the current Pokemon is Vlrbbmqbhcdarzowkk\n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Removing the current pokemon Vlrbbmqbhcdarzowkk from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Adding Cxrjmow to the pokedex");
add_pokemon(pokedex, cxrjmow);
printf("       ==> Checking that the current Pokemon is Cxrjmow\n");
assert(get_current_pokemon(pokedex) == cxrjmow);

printf("    ... Removing the current pokemon Cxrjmow from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Adding Cbefsarc to the pokedex");
add_pokemon(pokedex, cbefsarc);
printf("       ==> Checking that the current Pokemon is Cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Adding Fxxpklorel to the pokedex");
add_pokemon(pokedex, fxxpklorel);
printf("       ==> Checking that the current Pokemon is Fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Adding Vkhopkmcoqhnwnkue to the pokedex");
add_pokemon(pokedex, vkhopkmcoqhnwnkue);
printf("       ==> Checking that the current Pokemon is Vkhopkmcoqhnwnkue\n");
assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 2);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is NULL\n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Destroying the main Pokedex\n");
destroy_pokedex(pokedex);
printf("\n");
green();
printf("Passed Test 1\n");
normalColor();
printf("\n");printf("\n");
printf("=================================");
yellow();
printf("Test 2\n");
normalColor();
printf("    ... Creating a new Pokedex\n");
Pokedex pokedex = new_pokedex();
printf("    ... Checking initialized fields of the pokedex\n");
assert(pokedex->head == NULL);
assert(pokedex->last == NULL);
assert(pokedex->current == NULL);

printf("    ... Creating pokenodeVlrbbmqbhcdarzowkk");
Pokemon vlrbbmqbhcdarzowkk = create_vlrbbmqbhcdarzowkk();
printf("    ... Creating pokenodeCxrjmow");
Pokemon cxrjmow = create_cxrjmow();
printf("    ... Creating pokenodeCbefsarc");
Pokemon cbefsarc = create_cbefsarc();
printf("    ... Creating pokenodeFxxpklorel");
Pokemon fxxpklorel = create_fxxpklorel();
printf("    ... Creating pokenodeVkhopkmcoqhnwnkue");
Pokemon vkhopkmcoqhnwnkue = create_vkhopkmcoqhnwnkue();
printf("\n");
printf("    ... Adding Vlrbbmqbhcdarzowkk to the pokedex");
add_pokemon(pokedex, vlrbbmqbhcdarzowkk);
printf("       ==> Checking that the current Pokemon is Vlrbbmqbhcdarzowkk\n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Setting Vlrbbmqbhcdarzowkk to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 1);

printf("    ... Adding Cxrjmow to the pokedex");
add_pokemon(pokedex, cxrjmow);
printf("       ==> Checking that the current Pokemon is Cxrjmow\n");
assert(get_current_pokemon(pokedex) == cxrjmow);

printf("    ... Setting Cxrjmow to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 2);

printf("    ... Adding Cbefsarc to the pokedex");
add_pokemon(pokedex, cbefsarc);
printf("       ==> Checking that the current Pokemon is Cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Adding Fxxpklorel to the pokedex");
add_pokemon(pokedex, fxxpklorel);
printf("       ==> Checking that the current Pokemon is Fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Setting Fxxpklorel to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 3);

printf("    ... Removing the current pokemon Fxxpklorel from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 3);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Adding Vkhopkmcoqhnwnkue to the pokedex");
add_pokemon(pokedex, vkhopkmcoqhnwnkue);
printf("       ==> Checking that the current Pokemon is Vkhopkmcoqhnwnkue\n");
assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 3);
printf("       ==> Checking that the corresponding current Pokemon is cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 2);
printf("       ==> Checking that the corresponding current Pokemon is cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is NULL\n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Destroying the main Pokedex\n");
destroy_pokedex(pokedex);
printf("\n");
green();
printf("Passed Test 2\n");
normalColor();
printf("\n");printf("\n");
printf("=================================");
yellow();
printf("Test 3\n");
normalColor();
printf("    ... Creating a new Pokedex\n");
Pokedex pokedex = new_pokedex();
printf("    ... Checking initialized fields of the pokedex\n");
assert(pokedex->head == NULL);
assert(pokedex->last == NULL);
assert(pokedex->current == NULL);

printf("    ... Creating pokenodeVlrbbmqbhcdarzowkk");
Pokemon vlrbbmqbhcdarzowkk = create_vlrbbmqbhcdarzowkk();
printf("    ... Creating pokenodeCxrjmow");
Pokemon cxrjmow = create_cxrjmow();
printf("    ... Creating pokenodeCbefsarc");
Pokemon cbefsarc = create_cbefsarc();
printf("    ... Creating pokenodeFxxpklorel");
Pokemon fxxpklorel = create_fxxpklorel();
printf("    ... Creating pokenodeVkhopkmcoqhnwnkue");
Pokemon vkhopkmcoqhnwnkue = create_vkhopkmcoqhnwnkue();
printf("\n");
printf("    ... Adding Vlrbbmqbhcdarzowkk to the pokedex");
add_pokemon(pokedex, vlrbbmqbhcdarzowkk);
printf("       ==> Checking that the current Pokemon is Vlrbbmqbhcdarzowkk\n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Setting Vlrbbmqbhcdarzowkk to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 1);

printf("    ... Adding Cxrjmow to the pokedex");
add_pokemon(pokedex, cxrjmow);
printf("       ==> Checking that the current Pokemon is Cxrjmow\n");
assert(get_current_pokemon(pokedex) == cxrjmow);

printf("    ... Removing the current pokemon Cxrjmow from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Adding Cbefsarc to the pokedex");
add_pokemon(pokedex, cbefsarc);
printf("       ==> Checking that the current Pokemon is Cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Setting Cbefsarc to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 2);

printf("    ... Adding Fxxpklorel to the pokedex");
add_pokemon(pokedex, fxxpklorel);
printf("       ==> Checking that the current Pokemon is Fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Adding Vkhopkmcoqhnwnkue to the pokedex");
add_pokemon(pokedex, vkhopkmcoqhnwnkue);
printf("       ==> Checking that the current Pokemon is Vkhopkmcoqhnwnkue\n");
assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);

printf("    ... Setting Vkhopkmcoqhnwnkue to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 3);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 3);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 2);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Vlrbbmqbhcdarzowkk with id 24256 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Vlrbbmqbhcdarzowkk \n");

assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is NULL\n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Destroying the main Pokedex\n");
destroy_pokedex(pokedex);
printf("\n");
green();
printf("Passed Test 3\n");
normalColor();
printf("\n");printf("\n");
printf("=================================");
yellow();
printf("Test 4\n");
normalColor();
printf("    ... Creating a new Pokedex\n");
Pokedex pokedex = new_pokedex();
printf("    ... Checking initialized fields of the pokedex\n");
assert(pokedex->head == NULL);
assert(pokedex->last == NULL);
assert(pokedex->current == NULL);

printf("    ... Creating pokenodeVlrbbmqbhcdarzowkk");
Pokemon vlrbbmqbhcdarzowkk = create_vlrbbmqbhcdarzowkk();
printf("    ... Creating pokenodeCxrjmow");
Pokemon cxrjmow = create_cxrjmow();
printf("    ... Creating pokenodeCbefsarc");
Pokemon cbefsarc = create_cbefsarc();
printf("    ... Creating pokenodeFxxpklorel");
Pokemon fxxpklorel = create_fxxpklorel();
printf("    ... Creating pokenodeVkhopkmcoqhnwnkue");
Pokemon vkhopkmcoqhnwnkue = create_vkhopkmcoqhnwnkue();
printf("\n");
printf("    ... Adding Vlrbbmqbhcdarzowkk to the pokedex");
add_pokemon(pokedex, vlrbbmqbhcdarzowkk);
printf("       ==> Checking that the current Pokemon is Vlrbbmqbhcdarzowkk\n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Setting Vlrbbmqbhcdarzowkk to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 1);

printf("    ... Adding Cxrjmow to the pokedex");
add_pokemon(pokedex, cxrjmow);
printf("       ==> Checking that the current Pokemon is Cxrjmow\n");
assert(get_current_pokemon(pokedex) == cxrjmow);

printf("    ... Setting Cxrjmow to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 2);

printf("    ... Removing the current pokemon Cxrjmow from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is \n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Adding Cbefsarc to the pokedex");
add_pokemon(pokedex, cbefsarc);
printf("       ==> Checking that the current Pokemon is Cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Adding Fxxpklorel to the pokedex");
add_pokemon(pokedex, fxxpklorel);
printf("       ==> Checking that the current Pokemon is Fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Setting Fxxpklorel to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 2);

printf("    ... Adding Vkhopkmcoqhnwnkue to the pokedex");
add_pokemon(pokedex, vkhopkmcoqhnwnkue);
printf("       ==> Checking that the current Pokemon is Vkhopkmcoqhnwnkue\n");
assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);

printf("    ... Changing the current node to the pokemon Vkhopkmcoqhnwnkue with id 21186 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Vkhopkmcoqhnwnkue \n");

assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);
printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Changing the current node to the pokemon Vkhopkmcoqhnwnkue with id 21186 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Vkhopkmcoqhnwnkue \n");

assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 3);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 2);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Vlrbbmqbhcdarzowkk with id 24256 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Vlrbbmqbhcdarzowkk \n");

assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);
printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is NULL\n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Destroying the main Pokedex\n");
destroy_pokedex(pokedex);
printf("\n");
green();
printf("Passed Test 4\n");
normalColor();
printf("\n");printf("\n");
printf("=================================");
yellow();
printf("Test 5\n");
normalColor();
printf("    ... Creating a new Pokedex\n");
Pokedex pokedex = new_pokedex();
printf("    ... Checking initialized fields of the pokedex\n");
assert(pokedex->head == NULL);
assert(pokedex->last == NULL);
assert(pokedex->current == NULL);

printf("    ... Creating pokenodeVlrbbmqbhcdarzowkk");
Pokemon vlrbbmqbhcdarzowkk = create_vlrbbmqbhcdarzowkk();
printf("    ... Creating pokenodeCxrjmow");
Pokemon cxrjmow = create_cxrjmow();
printf("    ... Creating pokenodeCbefsarc");
Pokemon cbefsarc = create_cbefsarc();
printf("    ... Creating pokenodeFxxpklorel");
Pokemon fxxpklorel = create_fxxpklorel();
printf("    ... Creating pokenodeVkhopkmcoqhnwnkue");
Pokemon vkhopkmcoqhnwnkue = create_vkhopkmcoqhnwnkue();
printf("\n");
printf("    ... Adding Vlrbbmqbhcdarzowkk to the pokedex");
add_pokemon(pokedex, vlrbbmqbhcdarzowkk);
printf("       ==> Checking that the current Pokemon is Vlrbbmqbhcdarzowkk\n");
assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);

printf("    ... Adding Cxrjmow to the pokedex");
add_pokemon(pokedex, cxrjmow);
printf("       ==> Checking that the current Pokemon is Cxrjmow\n");
assert(get_current_pokemon(pokedex) == cxrjmow);

printf("    ... Setting Cxrjmow to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 1);

printf("    ... Adding Cbefsarc to the pokedex");
add_pokemon(pokedex, cbefsarc);
printf("       ==> Checking that the current Pokemon is Cbefsarc\n");
assert(get_current_pokemon(pokedex) == cbefsarc);

printf("    ... Adding Fxxpklorel to the pokedex");
add_pokemon(pokedex, fxxpklorel);
printf("       ==> Checking that the current Pokemon is Fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Adding Vkhopkmcoqhnwnkue to the pokedex");
add_pokemon(pokedex, vkhopkmcoqhnwnkue);
printf("       ==> Checking that the current Pokemon is Vkhopkmcoqhnwnkue\n");
assert(get_current_pokemon(pokedex) == vkhopkmcoqhnwnkue);

printf("    ... Setting Vkhopkmcoqhnwnkue to be found\n");
find_current_pokemon(pokedex);
printf("       ==> Checking the correct number of Pokemons found \n");
assert(count_found_pokemon(pokedex) == 2);

printf("    ... Changing the current node to the pokemon Vlrbbmqbhcdarzowkk with id 24256 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Vlrbbmqbhcdarzowkk \n");

assert(get_current_pokemon(pokedex) == vlrbbmqbhcdarzowkk);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 4);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 3);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Changing the current node to the pokemon Fxxpklorel with id 18607 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Fxxpklorel \n");

assert(get_current_pokemon(pokedex) == fxxpklorel);
printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 2);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 1);
printf("       ==> Checking that the corresponding current Pokemon is fxxpklorel\n");
assert(get_current_pokemon(pokedex) == fxxpklorel);

printf("    ... Changing the current node to the pokemon Cbefsarc with id 24095 \n");
change_current_pokemon(pokedex, randomId);
printf("       ==> Checking if the current pokemon is Cbefsarc \n");

assert(get_current_pokemon(pokedex) == cbefsarc);
printf("    ... Removing the current pokemon Vkhopkmcoqhnwnkue from the pokedex\n");
remove_pokemon(pokedex);
printf("       ==> Checking the total number of Pokemons in the pokedex \n");
assert(count_total_pokemon(pokedex) == 0);
printf("       ==> Checking that the corresponding current Pokemon is NULL\n");
assert(get_current_pokemon(pokedex) == NULL);

printf("    ... Destroying the main Pokedex\n");
destroy_pokedex(pokedex);
printf("\n");
green();
printf("Passed Test 5\n");
normalColor();
printf("\n");printf("\n");
}


////////////////////////////////////////////////////////////////////////
//                     Pokedex Test Functions                         //
////////////////////////////////////////////////////////////////////////

// `test_new_pokedex` checks whether the new_pokedex and destroy_pokedex
// functions work correctly, to the extent that it can.
//
// It does this by creating a new Pokedex, checking that it's not NULL,
// then calling destroy_pokedex.
//
// Note that it isn't possible to check whether destroy_pokedex has
// successfully destroyed/freed the Pokedex, so the best we can do is to
// call the function and make sure that it doesn't crash..
static void test_new_pokedex(void) {
    printf("\n>> Testing new_pokedex\n");

    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();

    printf("       ==> Checking that the returned Pokedex is not NULL\n");
    assert(pokedex != NULL);

    printf("    ... Destroying the Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf(">> Passed new_pokedex tests!\n");
    normalColor();
}

// `test_add_pokemon` checks whether the add_pokemon function works
// correctly.
//
// It does this by creating the Pokemon Bulbasaur (using the helper
// functions in this file and the provided code in pokemon.c), and
// calling add_pokemon to add it to the Pokedex.
//
// Some of the ways that you could extend these test would include:
//   = adding additional Pokemon other than just Bulbasaur,
//   = checking whether the currently selected Pokemon is correctly set,
//   = checking that functions such as `count_total_pokemon` return the
//     correct result after more Pokemon are added,
//   = ... and more!
static void test_add_pokemon(void) {
    printf("\n>> Testing add_pokemon\n");

    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();

    printf("    ... Creating Bulbasaur\n");
    Pokemon bulbasaur = create_bulbasaur();

    printf("    ... Adding Bulbasaur to the Pokedex\n");
    add_pokemon(pokedex, bulbasaur);

    printf("    ... Destroying the Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf(">> Passed add_pokemon tests!\n");
    normalColor();
}

// `test_next_pokemon` checks whether the next_pokemon function works
// correctly.
//
// It does this by creating two Pokemon: Bulbasaur and Ivysaur (using
// the helper functions in this file and the provided code in pokemon.c).
//
// It then adds these to the Pokedex, then checks that calling the
// next_pokemon function changes the currently selected Pokemon from
// Bulbasaur to Ivysaur.
//
// Some of the ways that you could extend these tests would include:
//   = adding even more Pokemon to the Pokedex,
//   = calling the next_pokemon function when there is no "next" Pokemon,
//   = calling the next_pokemon function when there are no Pokemon in
//     the Pokedex,
//   = ... and more!
static void test_next_pokemon(void) {
    printf("\n>> Testing next_pokemon\n");

    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();

    printf("    ... Creating Bulbasaur and Ivysaur\n");
    Pokemon bulbasaur = create_bulbasaur();
    Pokemon ivysaur = create_ivysaur();

    printf("    ... Adding Bulbasaur and Ivysaur to the Pokedex\n");
    add_pokemon(pokedex, bulbasaur);
    add_pokemon(pokedex, ivysaur);

    printf("       ==> Checking that the current Pokemon is Bulbasaur\n");
    assert(is_same_pokemon(get_current_pokemon(pokedex), bulbasaur));

    printf("    ... Moving to the next pokemon\n");
    next_pokemon(pokedex);

    printf("       ==> Checking that the current Pokemon is Ivysaur\n");
    assert(is_same_pokemon(get_current_pokemon(pokedex), ivysaur));

    printf("    ... Destroying the Pokedex\n");
    destroy_pokedex(pokedex);

    green();
    printf(">> Passed next_pokemon tests!\n");
    normalColor();
}

// `test_get_found_pokemon` checks whether the get_found_pokemon
// function works correctly.
//
// It does this by creating two Pokemon: Bulbasaur and Ivysaur (using
// the helper functions in this file and the provided code in pokemon.c).
//
// It then adds these to the Pokedex, sets Bulbasaur to be found, and
// then calls the get_found_pokemon function to get all of the Pokemon
// which have been found (which should be just the one, Bulbasaur).
//=======================================================================
// Some of the ways that you could extend these tests would include:
//   = calling the get_found_pokemon function on an empty Pokedex,
//   = calling the get_found_pokemon function on a Pokedex where none of
//     the Pokemon have been found,
//   = checking that the Pokemon in the new Pokedex are in ascending
//     order of pokemon_id (regardless of the order that they appeared
//     in the original Pokedex),
//   = checking that the currently selected Pokemon in the returned
//     Pokedex has been set correctly,
//   = checking that the original Pokedex has not been modified,
//   = ... and more!
//=======================================================================
static void test_get_found_pokemon(void) {
    printf("\n>> Testing get_found_pokemon\n");

    printf("    ... Creating a new Pokedex\n");
    Pokedex pokedex = new_pokedex();

    printf("    ... Creating Bulbasaur and Ivysaur\n");
    Pokemon bulbasaur = create_bulbasaur();
    Pokemon ivysaur = create_ivysaur();

    printf("    ... Adding Bulbasaur and Ivysaur to the Pokedex\n");
    add_pokemon(pokedex, bulbasaur);
    add_pokemon(pokedex, ivysaur);

    printf("       ==> Checking that the current Pokemon is Bulbasaur\n");
    assert(get_current_pokemon(pokedex) == bulbasaur);
    
    printf("    ... Setting Bulbasaur to be found\n");
    find_current_pokemon(pokedex);

    printf("    ... Getting all found Pokemon\n");
    Pokedex found_pokedex = get_found_pokemon(pokedex);

    printf("       ==> Checking the correct Pokemon were copied and returned\n");
    assert(count_total_pokemon(found_pokedex) == 1);
    assert(count_found_pokemon(found_pokedex) == 1);
    assert(is_copied_pokemon(get_current_pokemon(found_pokedex), bulbasaur));

    printf("    ... Destroying both Pokedexes\n");
    destroy_pokedex(pokedex);
    destroy_pokedex(found_pokedex);

    green();
    printf(">> Passed get_found_pokemon tests!\n");
    normalColor();
}



////////////////////////////////////////////////////////////////////////
//                     Helper Functions                               //
////////////////////////////////////////////////////////////////////////

// Helper function to create Bulbasaur for testing purposes.
static Pokemon create_bulbasaur(void) {
    Pokemon pokemon = new_pokemon(
            BULBASAUR_ID, BULBASAUR_NAME,
            BULBASAUR_HEIGHT, BULBASAUR_WEIGHT,
            BULBASAUR_FIRST_TYPE,
            BULBASAUR_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to create Ivysaur for testing purposes.
static Pokemon create_ivysaur(void) {
    Pokemon pokemon = new_pokemon(
            IVYSAUR_ID, IVYSAUR_NAME,
            IVYSAUR_HEIGHT, IVYSAUR_WEIGHT,
            IVYSAUR_FIRST_TYPE,
            IVYSAUR_SECOND_TYPE
    );
    return pokemon;
}

// Helper function to compare whether two Pokemon are the same.
// This checks that the two pointers contain the same address, i.e.
// they are both pointing to the same pokemon struct in memory.
//
// Pokemon ivysaur = new_pokemon(0, 'ivysaur', 1.0, 13.0, GRASS_TYPE, POISON_TYPE)
// Pokemon also_ivysaur = ivysaur
// is_same_pokemon(ivysaur, also_ivysaur) == TRUE
static int is_same_pokemon(Pokemon first, Pokemon second) {
    return first == second;
}

// Helper function to compare whether one Pokemon is a *copy* of
// another, based on whether their attributes match (e.g. pokemon_id,
// height, weight, etc).
// 
// It also checks that the pointers do *not* match == i.e. that the
// pointers aren't both pointing to the same pokemon struct in memory.
// If the pointers both contain the same address, then the second
// Pokemon is not a *copy* of the first Pokemon.
// 
// This function doesn't (yet) check that the Pokemon's names match
// (but perhaps you could add that check yourself...).
static int is_copied_pokemon(Pokemon first, Pokemon second) {
    return (pokemon_id(first) == pokemon_id(second))
    &&  (first != second)
    &&  (pokemon_height(first) == pokemon_height(second))
    &&  (pokemon_weight(first) == pokemon_weight(second))
    &&  (pokemon_first_type(first) == pokemon_first_type(second))
    &&  (pokemon_second_type(first) == pokemon_second_type(second));
}
// ==============   HELPER FUNCTIONS    ================== //

// Print passed sign in color
static void printPassed() {
    green();
    printf("passed\n");
    normalColor();
}

// Print failed sign in color
static void printFailed() {
    red();
    printf("failed\n");
    normalColor();
}

// Change output color to green
static void green() {
    printf("\033[1;32m");
}
// Change output color to red
static void red() {
    printf("\033[1;31m");
}
// Change output color to yellow
static void yellow() {
    printf("\033[1;33m");
}
// Change output color to normal
static void normalColor() {
    printf("\033[0m");
}
// Return if a character is alphabetical
static int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        || (ch >= 'A' && ch <= 'Z')) {
        return 1;
    } else {
        return 0;
    }
}
// Convert a character to lowercase
static int lowerChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'A' && ch <= 'Z') {
            newCh = ch + 32;
        }
    } 
    return newCh;
}
// Convert a character to uppercase
static int upperChar(int ch) {
    int newCh = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'a' && ch <= 'z') {
            newCh = ch = 32;
        }
    } 
    return newCh;
}
// Return a random Uppercase characters
static int randUppercase() {
    return 'A' + rand() % ('Z'-'A' + 1);
}
// Return a random lowercase characters
static int randLowercase() {
    return 'a' + rand() % ('z' - 'a' + 1);
}
// Create a radom String with a specific number or characters
/*static char *randString(int N_chars) {
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *stringg[MAX_CHAR_IN_STR];
    stringg[0] = "";
    char str[MAX_CHAR_IN_STR + 1];
    str[0] = (char) randUppercase();
    stringg[0] = (const char) randUppercase();
    char randChar = (char) randUppercase();
    memcpy(stringg, randChar, 1);
    //strcpy(stringg, (const char*) randUppercase());
    //stringg[0] = &charset[25 + rand() % 26];
    strcpy(stringg, charset[25 + rand() % 26]);
    int i = 1;
    while (i < N_chars) {
        randChar = (char) randLowercase();
        memcpy(stringg, randChar, 1);
        //strcpy(stringg, (const char*) randLowercase());
        //str[i] = (char) randLowercase();
        stringg[i] = charset[rand() % 26];
        i++;
    }
    stringg[i] = '\0';
    return stringg;
}*/
/*static char *rand_string(char *str, int size) {
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (size) {
        --size;
        for (int  n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}*/
static void rand_str(char *str, int length) {
    char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int firstChar = 1;
    while (length-- > 0) {
        int index;
        if (firstChar) {
            index = rand() % N_APHABET + N_APHABET - 1;
            firstChar = 0;
        } else {
            index = rand() % N_APHABET;
        }
        *str++ = charset[index];
    }
    *str = '\0';
}

// Print a string in uppercase
static void upperStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(toupper(str[i]));
        i++;
    }
}
// Print a string in lowercase
static void lowerStr(const char *str) {
    int i = 0;
    while (str[i]) {
        putchar(tolower(str[i]));
        i++;
    }
}
///////////////////////////////////////////////////////////////
//               PRODUCE TESTING CODE FUNCTION               //
///////////////////////////////////////////////////////////////

// Produce testing code for this file
// Copy the code below the line '========' into this file
static void produceTestingCode() {
    printf("\n\n==========Produce C code of Pokedex Tests =======================\n");
    int N_tests, N_pokednodes;
    printf("How many tests that you want to C code? ");
    scanf("%d", &N_tests);
    printf("How many pokemons that you want to have in the test? (real max is 32766 but recommended max 300 pokemons) \n");
    scanf("%d", &N_pokednodes);

    printf("Start copying the code below to test_pokdex.c \n");
    red();
    printf("Warning: ");
    normalColor();
    printf("Be careful with sign '[MAIN FUNCTION START HERE]' in between may occur. \n");
    printf("This is where we need to break our code put this piece inside main function\n\n");
    printf("=================================================================\n\n\n");

    int i, j;
    int pokemonId[MAX_POKEMON] = {0};
    char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR];
    double pokemonWeight[MAX_POKEMON] = {0};
    double pokemonHeight[MAX_POKEMON] = {0};
    int pokemonFirstType[MAX_POKEMON];
    int pokemonSecType[MAX_POKEMON];

    // Generate #define code
    for (j = 0; j < N_pokednodes; j++) {
        //pokemonName[j] = randString(rand() % MAX_CHAR_IN_STR + 1);
        //pokemonName[j] = randString(rand() % (MAX_CHAR_IN_STR - 1) + 1);
        rand_str(pokemonName[j], rand() % (MAX_CHAR_IN_STR - 5) + 5);
        pokemonId[j] = rand() % MAX_POKEMON;
        pokemonHeight[j] = (double) rand() / (double) rand();
        pokemonWeight[j] = (double) rand() / (double) rand();
        pokemonFirstType[j] = 1 + rand() % N_POKEMON_TYPE;
        pokemonSecType[j] = rand() % (N_POKEMON_TYPE - 1);
        printf("\n//Sample data on %s, the Pokemon with pokemon_id %d\n", pokemonName[j], pokemonId[j]);
        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_ID %d \n", pokemonId[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_NAME \"%s\"\n", pokemonName[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_HEIGHT %.1lf\n", pokemonHeight[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_WEIGHT %.1lf\n", pokemonWeight[j]);

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_FIRST_TYPE ");
        upperStr(pokemon_type_to_string(pokemonFirstType[j]));
        printf("_TYPE\n");

        printf("#define ");
        upperStr(pokemonName[j]);
        printf("_SECOND_TYPE ");
        upperStr(pokemon_type_to_string(pokemonSecType[j]));
        printf("_TYPE\n");

        printf("\n");
    }
    red();
    printf("\n[MAIN FUNCTION STARTS HERE]\n");
    normalColor();
    // Generate the testing functions code
    for (i = 1; i <= N_tests; i++) {
        printf("printf(\"=================================\");\n");
        printf("yellow();\n");
        printf("printf(\"Test %d\\n\");\n", i);
        printf("normalColor();\n");
        testing(pokemonName, pokemonId, N_pokednodes);
        printf("green();\n");
        printf("printf(\"Passed Test %d\\n\");\n", i);
        printf("normalColor();\n");
        printf("printf(\"\\n\");");
        printf("printf(\"\\n\");");
        printf("\n");
    }
    red();
    printf("\n[MAIN FUNCTION ENDS HERE]\n");
    normalColor();

    // Generate the helper function code
    
    // Generating testing function here

    // Generate creat_pokenode code here
    create_pokenodes(pokemonName, N_pokednodes);
}
static void create_pokenodes(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], int N_pokednodes) {
    int j;
    for (j = 0; j < N_pokednodes; j++) {
        printf("\n");
        char *name = pokemonName[j];
        printf("// Helper function to create %s for testing purposes.\n", name);
        printf("static Pokemon create_");
        lowerStr(name);
        printf("(void) {\n");
        printf("    Pokemon pokemon = new_pokemon(\n");
        printf("            ");
        upperStr(name);
        printf("_ID, \n");
        printf("            ");
        upperStr(name);
        printf("_NAME,\n");
        printf("            ");
        upperStr(name);
        printf("_HEIGHT, \n");
        printf("            ");
        upperStr(name);
        printf("_WEIGHT,\n");
        printf("            ");
        upperStr(name);
        printf("_FIRST_TYPE,\n");
        printf("            ");
        upperStr(name);
        printf("_SECOND_TYPE\n");
        printf("    );\n");
        printf("    return pokemon;\n");
        printf("}\n");
    }
}

static void testing(char pokemonName[MAX_POKEMON][MAX_CHAR_IN_STR], 
                    int pokemonId[MAX_POKEMON],
                    int N_pokednodes) {
    printf("printf(\"    ... Creating a new Pokedex\\n\");\n");
    printf("Pokedex pokedex = new_pokedex();\n");
    printf("printf(\"    ... Checking initialized fields of the pokedex\\n\");\n");
    printf("assert(pokedex->head == NULL);\n");
    printf("assert(pokedex->last == NULL);\n");
    printf("assert(pokedex->current == NULL);\n\n");
    int j;
    int N_nodes_in_pokedex = 0;
    // Creating the nodes
    for (j = 0; j < N_pokednodes; j++) {
        printf("printf(\"    ... Creating pokenode%s\");\n", pokemonName[j]);
        printf("Pokemon ");
        lowerStr(pokemonName[j]);
        printf(" = create_");
        lowerStr(pokemonName[j]);
        printf("();\n");
    }
    printf("printf(\"\\n\");\n");

    // Add the nodes to the pokedex and check the current selected node 
    // of the pokedex
    int found_count = 0;
    int removed[MAX_POKEMON] = {0};
    int found[MAX_POKEMON] = {0};
    for (j = 0; j < N_pokednodes; j++) {
        printf("printf(\"    ... Adding %s to the pokedex\");\n", pokemonName[j]);
        printf("add_pokemon(pokedex, ");
        lowerStr(pokemonName[j]);
        printf(");\n");
        printf("printf(\"       ==> Checking that the current Pokemon is %s\\n\");", pokemonName[j]);
        printf("\nassert(get_current_pokemon(pokedex) == ");
        lowerStr(pokemonName[j]);
        printf(");\n");
        printf("\n");
        N_nodes_in_pokedex++;

        // Randomly set this pokemon is found
        if (rand() % SET_FOUND == 0) {
            found_count++;
            found[j] = 1;
            printf("printf(\"    ... Setting %s to be found\\n\");\n", pokemonName[j]);
            printf("find_current_pokemon(pokedex);\n");
            printf("printf(\"       ==> Checking the correct number of Pokemons found \\n\");\n");
            printf("assert(count_found_pokemon(pokedex) == %d);\n", found_count);
            printf("\n");
        }
        

        // Randomly remove the pokemon
        if (rand() % REMOVE_POKEMON == 0) {
            N_nodes_in_pokedex--;
            removed[j] = 1;
            if (found[j] == 1) {
                found[j] = 0;
                found_count--;
            }
            printf("printf(\"    ... Removing the current pokemon %s from the pokedex\\n\");\n", pokemonName[j]);
            printf("remove_pokemon(pokedex);\n");

            printf("printf(\"       ==> Checking the total number of Pokemons in the pokedex \\n\");\n");
            printf("assert(count_total_pokemon(pokedex) == %d);\n", N_nodes_in_pokedex);
            printf("printf(\"       ==> Checking that the corresponding current Pokemon is \\n\");");
            printf("\nassert(get_current_pokemon(pokedex) == ");
            int counter = j;
            while (removed[counter] && counter > 0) {
                counter--;
            }
            if (counter >= 0 && !removed[counter]) {
                lowerStr(pokemonName[counter]);
            } else {
                printf("NULL");
            }

            printf(");\n");
            printf("\n");
        }
        
    }
    j--;
    // Randomly execute function untile the last node is removed
    while (N_nodes_in_pokedex > 0) {
        // Randomly change the current selected pokenode
        if (rand() % CHANGE_CURR_POKEMON == 0) {
            int tempIndex = rand() % N_pokednodes;
            int randomId = pokemonId[tempIndex];
            while (removed[tempIndex]) {
                tempIndex = rand() % N_pokednodes;
                randomId = pokemonId[tempIndex];
            }
            printf("printf(\"    ... Changing the current node to the pokemon"
                    " %s with id %d \\n\");\n", pokemonName[tempIndex], pokemonId[tempIndex]);
            printf("change_current_pokemon(pokedex, randomId);\n");
            printf("printf(\"       ==> Checking if the current pokemon is %s \\n\");\n", pokemonName[tempIndex]);
            printf("\nassert(get_current_pokemon(pokedex) == ");
            lowerStr(pokemonName[tempIndex]);
            printf(");\n");
        }
        // Randomly remove the pokemon
        if (rand() % REMOVE_POKEMON == 0) {
            N_nodes_in_pokedex--;
            removed[j] = 1;
            if (found[j] == 1) {
                found[j] = 0;
                found_count--;
            }
            printf("printf(\"    ... Removing the current pokemon %s from the pokedex\\n\");\n", pokemonName[j]);
            printf("remove_pokemon(pokedex);\n");

            printf("printf(\"       ==> Checking the total number of Pokemons in the pokedex \\n\");\n");
            printf("assert(count_total_pokemon(pokedex) == %d);\n", N_nodes_in_pokedex);
            printf("printf(\"       ==> Checking that the corresponding current Pokemon is "); 
            int counter = j;
            while (removed[counter] && counter > 0) {
                counter--;
            }
            if (counter >= 0 && !removed[counter] && N_nodes_in_pokedex > 0) {
                lowerStr(pokemonName[counter]);
            } else {
                printf("NULL");
            }
            printf("\\n\");");
            printf("\nassert(get_current_pokemon(pokedex) == ");
            while (removed[counter] && counter > 0) {
                counter--;
            }
            if (counter >= 0 && !removed[counter] && N_nodes_in_pokedex > 0) {
                lowerStr(pokemonName[counter]);
            } else {
                printf("NULL");
            }

            printf(");\n");
            printf("\n");
        }
    }

    printf("printf(\"    ... Destroying the main Pokedex\\n\");\n");
    printf("destroy_pokedex(pokedex);\n");
    printf("printf(\"\\n\");\n");
    
    


    /*printf(\"    ... Getting all found Pokemon\\n\");
    Pokedex found_pokedex = get_found_pokemon(pokedex);

    printf(\"       ==> Checking the correct Pokemon were copied and returned\\n\");
    assert(count_total_pokemon(found_pokedex) == 1);
    assert(count_found_pokemon(found_pokedex) == 1);
    assert(is_copied_pokemon(get_current_pokemon(found_pokedex), bulbasaur));

    printf(\"    ... Destroying both Pokedexes\\n\");
    destroy_pokedex(pokedex);
    destroy_pokedex(found_pokedex);

    green();
    printf(\">> Passed get_found_pokemon tests!\\n\");
    normalColor();*/
}

