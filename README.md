
# COMP_1511: Programming Fundamentals
Lectured by Dr.Andrew Taylor in 19T1

### Introduction
An introduction to problem-solving via programming, which aims to have students develop proficiency in using a high level programming language. Topics: algorithms, program structures (statements, sequence, selection, iteration, functions), data types (numeric, character), data structures (arrays, tuples, pointers, lists), storage structures (memory, addresses), introduction to analysis of algorithms, testing, code quality, teamwork, and reflective practice. The course includes extensive practical work in labs and programming projects. (copied from [UNSW handbook](https://www.handbook.unsw.edu.au/undergraduate/courses/2019/COMP1511/))

### Resourses
* [Course Outline](https://github.com/ttuanho/COMP_1511/blob/master/Course%20Outline.pdf)
* [Tutorial Code](https://github.com/ttuanho/COMP_1511/tree/master/Tutorial%20Code%20Examples) (written by my tutor Dylan Bletcher)
* Textbook (optional) : C programming by BRIAN W KERNIGHAN & DENNIS M. RITCHIE([source](https://www.dipmat.univpm.it/~demeio/public/the_c_programming_language_2.pdf))

### Assignment 1 : Arrays
For this assignment, you will be writing a program to play the made-up card game named Coco (see [assignment specs](https://github.com/ttuanho/COMP_1511/blob/master/pdfs/Assignment%201%20-%20Coco.pdf))



### Assignment 2 : Linked Lists
For this assignment, we are asking you to implement a mini Pokédex in C. The task is split into 5 sections; each section is not weighted the same. Pokémon are fictional creatures from the Pokémon franchise, most famously from the Pokémon games. The game revolves around the (questionably ethical) capturing of these creatures. Within the fiction of this universe, a device called the Pokédex is used to catalogue all the creatures a player finds and captures. (see the [assignment specs](https://github.com/ttuanho/COMP_1511/blob/master/pdfs/Assignment%202%20-%20Poke%CC%81dex.pdf))


***Note***: The remarkle thing in this assignment is that I have written the C code that procudes another C-harcode that then tests the [pokedex.c](https://github.com/ttuanho/COMP_1511/blob/master/ass2/pokedex.c) with very good style. Even though this is idea was unsuccessfull, I view this idea as something very substaintial : the concept of **a machine that can test another machine**. Feel free to develop this function [produceTestingCode()](https://github.com/ttuanho/COMP_1511/blob/master/ass2/test_pokedex_beta.c) on a new branch.