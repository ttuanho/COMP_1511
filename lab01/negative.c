//
//  negative.c
//  
//
//  Created by Tuan Ho on 26/2/19.
//


#include<stdio.h>

int main(void){
    
    double x;
    
    scanf("%lf",&x);
    if (x<0){
        printf("Don't be so negative!\n");
    }
    else  if (x>0){
        
        printf("You have entered a positive number.\n");
    }
    else{
        
        printf("You have entered zero.\n");
    }
    
    return 0;
}
