#include<stdio.h>

int main(void){
    printf("Please enter an integer: ");
    int x;
    scanf("%d",&x);
    if (x<1){
        printf("You entered a number less than one.");
    }else if (x==1){
        printf("You entered one.");
    }else if (x==2){
        printf("You entered two.");
    }
    else if (x==3){
        printf("You entered three.");
    }else if (x==4){
        printf("You entered four.");
    }else if (x==5){
        printf("You entered five.");
    }
    else {
        printf("You entered a number greater than five.");
    }
    
    printf("\n");
    return 0;
}
