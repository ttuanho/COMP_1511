#include<stdio.h>
#include<string.h>
int main(void){
    printf("Enter Year: ");
    int year;
    scanf("%d",&year);
    
    int a,b,c,d,e,f,g,h,i,k,l,m,Easter_Month,p,Easter_Date;
    a=year%19;
    b=year/100;
    c=year%100;
    d=b/4;
    e=b%4;
    f=(b+8)/25;
    g=(b-f+1)/3;
    h=(19*a+b-d-g+15)%30;
    i=c/4;
    k=c%4;
    l=(32+2*e+2*i-h-k)%7;
    m=(a+11*h+22*l)/451;
    Easter_Month =(h+l-7*m+114)/31;
    p=(h+l-7*m+114)%31;
    Easter_Date=p+1; 
    
    printf("Easter is ");
    char month[80];
    if (Easter_Month==1){
        printf("January");
    }
    else if (Easter_Month==2){
        printf("February");
    }
    else if (Easter_Month==3){
        printf("March");
    }
    else if (Easter_Month==4){
        printf("April");
    }
    else if (Easter_Month==5){
        printf("May");
    }
    else if (Easter_Month==6){
        printf("June");
    }
    else if (Easter_Month==7){
        printf("July");
    }
    else if (Easter_Month==8){
        printf("August");
    }
    else if (Easter_Month==9){
        printf("September");
    }
    else if (Easter_Month==10){
        printf("October");
    }
    else if (Easter_Month==11){
        printf("November");
    }
    else if (Easter_Month==12){
        printf("December");
    }
    printf(" %d in %d.\n",Easter_Date,year);
    
    return 0;
}
