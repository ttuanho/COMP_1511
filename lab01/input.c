#include <stdio.h>

int main(void) {

    int array[2000];
    int ch;
    int i = 0;
    while(scanf("%d", &array[i]) == 1) {
        i++;
        if ((ch = getchar()) == 10) {
            break;
        }
    }
    for (int t = 0; t < i; t ++) {
        printf("%d ", array[t]);
    }
    return 0;
}