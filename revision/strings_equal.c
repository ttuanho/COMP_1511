// String Equality
// Created by
//  ... (z0000000)
//  ... (z0000000)
// Created on 2017-08-??
// Tutor's name (dayHH-lab)

#include <stdio.h>
#include <assert.h>
int conver_to_lowercase(int ch);
int isAlphabet(int ch);
int strings_equal(char *string1, char *string2);
int string_length(char *string);

int main(int argc, char *argv[]) {

    // Some simple assert-based tests.
    // You probably want to write some more.
    assert(strings_equal("", "") == 1);
    assert(strings_equal(" ", "") == 0);
    assert(strings_equal("", " ") == 0);
    assert(strings_equal(" ", " ") == 1);
    assert(strings_equal("\n", "\n") == 1);
    assert(strings_equal("This is 17 bytes.", "") == 0);
    assert(strings_equal("", "This is 17 bytes.") == 0);
    assert(strings_equal("This is 17 bytes.", "This is 17 bytes.") == 1);
    assert(strings_equal("Here are 18 bytes!", "This is 17 bytes.") == 0);
    assert(strings_equal("THiS is 17 bytes.", "This is 17 bytes.") == 1);

    printf("All tests passed.  You are awesome!\n");
    char *string1 = "THiS dis 17 bytes.";
    char *string2 = "This is 17 bytes.";
    printf("%d", strings_equal(string1, string2));
    return 0;
}
int isAlphabet(int ch) {
    if ((ch >= 'a' && ch <= 'z')
        || (ch >= 'A' && ch <= 'Z')) {
        return 1;
    } else {
        return 0;
    }
}
int string_length(char *string) {
    int counter = 0;
    while (string[counter] != '\0') {
        counter++;
    }
    return counter;
}

int conver_to_lowercase(int ch) {
    int nch = ch;
    if (isAlphabet(ch)) {
        if (ch >= 'A' && ch <= 'Z') {
            nch = ch + 32;
        }
    } 
    return nch;
}
// Takes two strings, and if they are the same,
// returns 1, or 0 otherwise.
int strings_equal(char *string1, char *string2) {
    int is_it = 1;
    int i;
    if (string_length(string1) == string_length(string2)) {
        for (i = 0; i < string_length(string1); i++) {
            if (conver_to_lowercase(string1[i])!= conver_to_lowercase(string2[i])) {
                is_it = 0;
                break;
            }
        }
    } else {
        is_it = 0;
    }
    return is_it;
}
