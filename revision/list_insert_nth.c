#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct node {
    struct node *next;
    int          data;
};

struct node *insert_nth(int n, int value, struct node *head);
struct node *strings_to_list(int len, char *strings[]);
void print_list(struct node *head);

// DO NOT CHANGE THIS MAIN FUNCTION

int main(int argc, char *argv[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s n value list-elements\n", argv[0]);
        return 1;
    }
    int n = atoi(argv[1]);
    int value = atoi(argv[2]);
    // create linked list from command line arguments
    struct node *head = strings_to_list(argc - 3, &argv[3]);

    struct node *new_head = insert_nth(n, value, head);
    print_list(new_head);

    return 0;
}


// Insert a new node containing value at position n of the linked list.
// if n == 0, node is inserted at start of list
// if n >= length of list, node is appended at end of list
// The head of the new list is returned.
struct node *insert_nth(int n, int value, struct node *head) {
    struct node *newNode = malloc(sizeof(struct node));
    newNode->data = value;
    struct node *p;
    int length = 0;
    for (p = head; p != NULL; p = p->next) {
        length++;    
    }
    if (head == NULL) {

        head = newNode;
        newNode->next = NULL;
    } else {
        length++;
        //printf("len = %d\n", length);
        if (n == 0 && head != NULL) {
            newNode->next = head;
            head = newNode;
        } else if (n >= length && length > 1) {
            for (p = head; p->next != NULL; p = p->next) {
                //printf(" %d ", p->data);
            }
            p->next = newNode;
            newNode->next = NULL;
        } else {
            int count = 0;
            for (p = head; p != NULL; p = p->next) {
                count++;
                if (count == n) {
                    //struct node *currNode = p;
                    struct node *nextNode = p->next;
                    newNode->next = nextNode;
                    p->next = newNode;
                    break;
                }
            }
        }
    }
    
    
    return head;
}


// DO NOT CHANGE THIS FUNCTION
// create linked list from array of strings
struct node *strings_to_list(int len, char *strings[]) {
    struct node *head = NULL;
    for (int i = len - 1; i >= 0; i = i - 1) {
        struct node *n = malloc(sizeof (struct node));
        assert(n != NULL);
        n->next = head;
        n->data = atoi(strings[i]);
        head = n;
    }
    return head;
}

// DO NOT CHANGE THIS FUNCTION
// print linked list
void print_list(struct node *head) {
    printf("[");

    for (struct node *n = head; n != NULL; n = n->next) {
        // If you're getting an error here,
        // you have returned an invalid list
        printf("%d", n->data);
        if (n->next != NULL) {
            printf(", ");
        }
    }
    printf("]\n");
}

